-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Хост: 10.0.0.119:3307
-- Время создания: Июн 24 2019 г., 18:23
-- Версия сервера: 10.1.32-MariaDB
-- Версия PHP: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `anteyy_kip`
--

-- --------------------------------------------------------

--
-- Структура таблицы `basis`
--

CREATE TABLE IF NOT EXISTS `basis` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `name` text COLLATE utf8_unicode_ci,
  `name_translit` text COLLATE utf8_unicode_ci,
  `type` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `content` mediumtext COLLATE utf8_unicode_ci,
  `title` tinytext COLLATE utf8_unicode_ci,
  `description` tinytext COLLATE utf8_unicode_ci,
  `keywords` tinytext COLLATE utf8_unicode_ci,
  `lang` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ru',
  `order_num` smallint(5) unsigned NOT NULL DEFAULT '1',
  `img` tinytext COLLATE utf8_unicode_ci,
  `target` tinytext COLLATE utf8_unicode_ci,
  `popular` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `mime_type` tinytext COLLATE utf8_unicode_ci,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closed` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type_lang_closed` (`type`(255),`lang`,`closed`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=152 ;

--
-- Дамп данных таблицы `basis`
--

INSERT INTO `basis` (`id`, `id_parent`, `name`, `name_translit`, `type`, `content`, `title`, `description`, `keywords`, `lang`, `order_num`, `img`, `target`, `popular`, `mime_type`, `user_id`, `ip`, `created`, `closed`) VALUES
(1, 0, 'Теплоучет', 'teplouchet', 'category', NULL, 'Теплоучет - Теплосчетчик, Тепловычислители, Узлы учета тепловой энергии (УУТЭ)', 'Узлы учета тепловой энергии (УУТЭ): теплосчетчики, тепловычислители. Купить оборудование для теплоучета (счетчики тепла и вычислители тепл', 'Устройство сбора и передачи данных УСПД', 'ru', 1, 'teplouchet.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/teplouchet/', 0, NULL, 0, '127.0.0.1', '2019-06-24 10:16:10', NULL),
(2, 1, 'Квартирные теплосчетчики Ду-15,20,25', 'kvartirnye-teploschetchiki-du-152025', 'category', NULL, 'Квартирные теплосчетчики (индивидуальные счетчики тепла, тепловой энергии) Ду15, Ду20', 'Квартирные теплосчетчики (счетчики тепла, тепловой энергии) Ду10-Ду25 ультразвуковые и тахометрические. Как купить индивидуальный КТС по це?', 'СТЭ-31-БЕРИЛЛ Ду15/20 электронный счетчик тепловой энергии', 'ru', 1, 'kvartirnye-teploschetchiki-du-152025.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kvartirnye-teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:10', NULL),
(3, 11, 'Ультразвуковые счетчики тепла', 'ultrazvukovye-schetchiki-tepla', 'category', NULL, 'Индивидуальные (квартирные) ультразвуковые счетчики тепла (теплосчетчики Ду15, Ду20)', 'Индивидуальные (квартирные) ультразвуковые счетчики тепла (теплосчетчики Ду15, Ду20)', 'теплосчетчик SANEXT MONO RM, счетчик тепла Санекст', 'ru', 1, 'ultrazvukovye-schetchiki-tepla.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ultrazvukovye-schetchiki-tepla/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(4, 11, 'Тахометрические счетчики тепла', 'tahometricheskie-schetchiki-tepla', 'category', NULL, 'Индивидуальные (квартирные) тахометрические счетчики тепла (теплосчетчики Ду15, Ду20)', 'Индивидуальные (квартирные) тахометрические (крыльчатые) счетчики тепла (теплосчетчики Ду15, Ду20)', 'СТЭ-31-БЕРИЛЛ Ду15/20 электронный счетчик тепловой энергии', 'ru', 1, 'tahometricheskie-schetchiki-tepla.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/tahometricheskie-schetchiki-tepla/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(5, 11, 'Доп', 'dop', 'category', NULL, 'Дополнительное оборудование и арматура для монтажа квартирного теплосчетчика', 'Дополнительное оборудование и арматура для монтажа квартирного теплосчетчика', 'Индикаторы коррозии тепловых сетей ИКТ-40, ИКТ-50', 'ru', 1, 'dop.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/dop-oborudovanie-kts/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(6, 1, 'Теплосчетчики общедомовые и промышленные', 'teploschetchiki-obshchedomovye-i-promyshlennye', 'category', NULL, 'Теплосчетчики - счетчики тепла (тепловой энергии) общедомовые и промышленные', 'Теплосчетчики - счетчик тепла (тепловой энергии) Ду-15-300 одно- и многоканальные домовые и промышленные для закрытых и открытых систем теплос', 'Ключевые слова', 'ru', 1, 'teploschetchiki-obshchedomovye-i-promyshlennye.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:10', NULL),
(7, 12, 'Электромагнитные теплосчетчики', 'elektromagnitnye-teploschetchiki', 'category', NULL, 'Электромагнитные теплосчетчики', 'Электромагнитные теплосчетчики- счетчики тепла в основе которых лежат электромагнитные расходомеры', 'КСТ-22 общедомовой теплосчётчик', 'ru', 1, 'elektromagnitnye-teploschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/elektromagnitnye-teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(8, 12, 'Ультразвуковые теплосчетчики', 'ultrazvukovye-teploschetchiki', 'category', NULL, 'Ультразвуковые теплосчетчики', 'Ультразвуковые теплосчетчики - счетчики тепла работающие на принципе изменения времени прохождения ультразвукового сигнала от источника', 'MULTICAL-603, Мультикал-603', 'ru', 1, 'ultrazvukovye-teploschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ultrazvukovye-teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(9, 12, 'Тахометрические теплосчетчики', 'tahometricheskie-teploschetchiki', 'category', NULL, 'Тахометрические теплосчетчики', 'Тахометрические теплосчетчики - счетчики тепла, принцип действия которых основан на преобразовании поступательного движения потока жидк?', 'Ключевые слова', 'ru', 1, 'tahometricheskie-teploschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/tahometricheskie-teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(10, 12, 'Вихревые теплосчетчики', 'vihrevye-teploschetchiki', 'category', NULL, 'Вихревые теплосчетчики', 'Вихревые теплосчетчики - счетчики тепла, работающие на принципе широко известного природного явления - образование вихрей за препятствием', 'ПРАМЕР-ТС-100 теплосчетчики', 'ru', 1, 'vihrevye-teploschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/vihrevye-teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(11, 12, 'Погружные теплосчетчики', 'pogrujnye-teploschetchiki', 'category', NULL, 'Погружные теплосчетчики', 'Теплосчетчики погружного типа - счетчики тепла принцип действия, которых основан на на явлении электромагнитной индукции', 'КМ-5-Б1, КМ-5-Б3, КМ-5-Б, вставной теплосчетчик погружного типа. Купить КМ-5Б по цене производителя со склада в Москве.', 'ru', 1, 'pogrujnye-teploschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/pogruzhnye-teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(12, 12, 'Универсальные теплосчетчики', 'universalnye-teploschetchiki', 'category', NULL, 'Универсальные теплосчетчики (счетчики тепловой энергии/тепла многоканальные/многосистемные)', 'Универсальные многоканальные теплосчетчики - это счетчики тепловой энергии/тепла, предназначенные для работы с различными типами преобра', 'Теплосчетчик ТС-ТВК универсальный комбинированный', 'ru', 1, 'universalnye-teploschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/universalnye-teploschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(13, 1, 'Тепловычислители', 'teplovychisliteli', 'category', NULL, 'Тепловычислитель - вычислитель количества тепловой энергии (тепла).', 'Тепловычислители - вычислители количества тепловой энергии (тепла). Купить ВКТ, ВТЭ, СПТ по цене производителя, в наличии и под заказ со скла', 'Тепловычислители ТВК-01, ТВК-02', 'ru', 1, 'teplovychisliteli.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/teplovychisliteli/', 0, NULL, NULL, '', '2019-06-24 10:16:10', NULL),
(14, 13, 'Многоканальные тепловычислители', 'mnogokanalnye-teplovychisliteli', 'category', NULL, 'Тепловычислитель многоканальный - многосистемный, многотрубный вычислитель тепла', 'Тепловычислитель многоканальный (многосистемный, многотрубный) - электронный прибор (вычислитель) для измерения и регистрации параметров ', 'Тепловычислители ТВК-01, ТВК-02', 'ru', 1, 'mnogokanalnye-teplovychisliteli.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/mnogokanalnye-teplovychisliteli/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(15, 13, 'Одноканальные тепловычислители', 'odnokanalnye-teplovychisliteli', 'category', NULL, 'Тепловычислитель одноканальный (односистемный, однотрубный) - вычислитель тепла', 'Тепловычислитель одноканальный (односистемный, однотрубный) - вычислитель для измерения и регистрации параметров (температуры, расхода и ?', 'СПТ 940 тепловычислитель', 'ru', 1, 'odnokanalnye-teplovychisliteli.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/odnokanalnye-teplovychisliteli/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(16, 1, 'Коммуникационное и сервисное оборудование', 'kommunikacionnoe-i-servisnoe-oborudovanie', 'category', NULL, 'Коммуникационное и сервисное оборудование', 'Коммуникационное и сервисное оборудование, периферийные устройства для систем сбора и обработки данных.', 'АДИ преобразователь измерительный для расходомера Питерфлоу РС', 'ru', 1, 'kommunikacionnoe-i-servisnoe-oborudovanie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kommunikacionnoe-servisnoe-oborudovanie/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(17, 14, 'Адаптеры, преобразователи и коннекторы', 'adaptery-preobrazovateli-i-konnektory', 'category', NULL, 'Адаптеры, коннекторы и преобразователи интерфейсов', 'Адаптеры, коннекторы и преобразователи интерфейсов', 'АДИ преобразователь измерительный для расходомера Питерфлоу РС', 'ru', 1, 'adaptery-preobrazovateli-i-konnektory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/adapter-preobrazovatel-konnektor/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(18, 14, 'Накопители, регистраторы, считыватели данных', 'nakopiteli-registratory-schityvateli-dannyh', 'category', NULL, 'Накопители, регистраторы, считыватели данных', 'Накопители, регистраторы, считыватели данных', 'УС-Н2 считывающее устройство переносное', 'ru', 1, 'nakopiteli-registratory-schityvateli-dannyh.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/nakopitel-registrator-schityvatel-dannyh/', 0, NULL, NULL, '', '2019-06-24 10:16:13', NULL),
(19, 14, 'Модемы и модули передачи данных', 'modemy-i-moduli-peredachi-dannyh', 'category', NULL, 'GSM/GPRS, радио-модемы и модули передачи данных', 'GSM/GPRS, радио-модемы и модули передачи данных', 'GSM/GPRS модем Теплоком RS232/RS485', 'ru', 1, 'modemy-i-moduli-peredachi-dannyh.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gsm-gprs-radio-modemy-i-moduli-peredachi-dannyh/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(20, 1, 'Диспетчеризация', 'dispetcherizaciya', 'category', NULL, 'Диспетчеризация теплосчетчиков и УУТЭ', 'Диспетчеризация теплосчетчиков-ТС и УУТЭ (узлов учета тепловой энергии)', 'Устройство сбора и передачи данных УСПД', 'ru', 1, 'dispetcherizaciya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/dispetcherizatsiya/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(21, 0, 'Температура', 'temperatura', 'category', NULL, 'Приборы и оборудование для измерения, контроля и регулирования температуры', 'Приборы и оборудование для измерения, контроля и регулирования температуры', 'МПР51-Щ4 регулятор температуры и влажности', 'ru', 1, 'temperatura.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/temperatura/', 0, NULL, 0, '127.0.0.1', '2019-06-24 10:16:10', NULL),
(22, 2, 'Датчики температуры (термопреобразователи)', 'datchiki-temperatury-termopreobrazovateli', 'category', NULL, 'Датчики температуры (термопреобразователи): термометры сопротивления -ТС, термоэлектрические преобразователи - ТП (термопары).', 'Датчики температуры (термопреобразователи): термометры сопротивления -ТС, термоэлектрические преобразователи - ТП (термопары).', 'МПР51-Щ4 регулятор температуры и влажности', 'ru', 1, 'datchiki-temperatury-termopreobrazovateli.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchiki-temperatury-termopreobrazovateli/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(23, 21, 'Термометры сопротивления (термосопротивления)', 'termometry-soprotivleniya-termosoprotivleniya', 'category', NULL, 'Термопреобразователи/термометры сопротивления ТС: медные ТСМ, платиновые ТСП', 'Термопреобразователи (термометры) сопротивления ТС: медные-ТСМ, платиновые-ТСП. Купить датчики дТС термосопротивления по цене производите', 'ТСПТ 105, 106, 206 термометры сопротивления платиновые', 'ru', 1, 'termometry-soprotivleniya-termosoprotivleniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/termometry-soprotivleniya-termosoprotivleniya/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(24, 21, 'Термоэлектрические преобразователи (термопары)', 'termoelektricheskie-preobrazovateli-termopary', 'category', NULL, 'Преобразователи термоэлектрические ТП - термопреобразователи (термопары)', 'Преобразователи термоэлектрические ТП. Купить термопреобразователи (термопары) ТПК-ХА, ТПL-ХК, ТПП-S, ТПР-B по цене производителя, в наличии и', 'Термопары ТППТ, ТПРТ, ТПВР (датчики температуры)', 'ru', 1, 'termoelektricheskie-preobrazovateli-termopary.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/termoelektricheskie-preobrazovateli-termopary/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(25, 21, 'Комплекты термометров сопротивления', 'komplekty-termometrov-soprotivleniya', 'category', NULL, 'Комплект термометров сопротивления (термопреобразователей) платиновых', 'Комплект термометров сопротивления платиновых КТПТР, КТС-Б, КТСП-Н/Р/005, КТСПТТВХ. Термопреобразователи по цене производителя в наличии и п?', 'Комплекты термометров сопротивления КТСМ/КТСП-0196-02, -02Б, -03, -03Б', 'ru', 1, 'komplekty-termometrov-soprotivleniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/komplekty-termometrov-soprotivleniya/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(26, 21, 'Термопреобразователи с унифицированным выходным сигналом', 'termopreobrazovateli-s-unificirovannym-vyhodnym-signalom', 'category', NULL, 'ТСМУ, ТСПУ, ТХАУ - термопреобразователи (датчики температуры) с выходом 0-5мА/4-20мА по цене производителя в наличии.', 'ТСМУ, ТСПУ, ТХАУ - термопреобразователи (датчики температуры) с унифицированным выходным токовым сигналом 0-5мА/4-20мА по цене производителя в', 'Термопары ТППТ, ТПРТ, ТПВР (датчики температуры)', 'ru', 1, 'termopreobrazovateli-s-unificirovannym-vyhodnym-signalom.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/termopreobrazovateli-s-unifitsirovannym-vyhodnym-signalom/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(27, 21, 'Датчики влажности и температуры', 'datchiki-vlajnosti-i-temperatury', 'category', NULL, 'Преобразователи (датчики) температуры и влажности ДВТ, Ивит, РОСА, ИПТВ, КИП-20, ПВТ.', 'Преобразователи (датчики) температуры и влажности ДВТ, Ивит, РОСА, ИПТВ, КИП-20, ПВТ.', 'МПР51-Щ4 регулятор температуры и влажности', 'ru', 1, 'datchiki-vlajnosti-i-temperatury.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchiki-vlazhnosti-i-temperatury/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(28, 21, 'Нормирующие преобразователи', 'normiruyushchie-preobrazovateli', 'category', NULL, 'Нормирующий преобразователь НП температуры НПТ, усилитель НУ, НПУ', 'Нормирующие преобразователи НП температуры НПТ, усилители НУ преобразуют НСХ термопреобразователей ТС и ТП с унифицированный выходной си', 'Модульные преобразователи ИПМ-0399/М,-0499/М2-Н', 'ru', 1, 'normiruyushchie-preobrazovateli.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/normiruyushhie-preobrazovateli/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(29, 21, 'Вспомогательное оборудование и арматура', 'vspomogatelnoe-oborudovanie-i-armatura', 'category', NULL, 'Защитно-монтажная арматура термопреоразователей: гильзы защитные ГЗ, бобышки БП/БС, штуцера ШП. Кабель термоэлектродный.', 'Вспомогательное оборудование для термопреобразователей ТС и ТП: защитно-монтажная арматура (гильзы защитные ГЗ, бобышки БП/БС, штуцера пер', 'Монтажный комплект для взрывозащищенных термопреобразователей  ТХА/ТХК-0595, ТСМ/ТСП-0595 ', 'ru', 1, 'vspomogatelnoe-oborudovanie-i-armatura.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/vspomogatelnoe-oborudovanie-i-armatura/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(30, 21, 'Эталонные термометры и термопреобразователи', 'etalonnye-termometry-i-termopreobrazovateli', 'category', NULL, 'Эталонные термометры сопротивления и термопреобразователи', 'Эталонные термометры сопротивления и термопреобразователи. Купить по цене производителя со склада в Москве.', 'термометры сопротивления ЭТС-50, этс50', 'ru', 1, 'etalonnye-termometry-i-termopreobrazovateli.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/etalonnye-termometry-i-termopreobrazovateli/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(31, 2, 'Датчики-реле температуры', 'datchiki-rele-temperatury', 'category', NULL, 'Датчики-реле температуры, температурные реле, термореле, термостаты', 'Датчики-реле температуры, температурные реле, термореле, термостаты по цене производителя из наличия и под заказ со склада в Москве', 'ТДМВ-102 датчик-реле температуры манометрический взрывозащищенный', 'ru', 1, 'datchiki-rele-temperatury.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchiki-rele-temperatury/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(32, 22, 'Манометрические датчики-реле температуры', 'manometricheskie-datchiki-rele-temperatury', 'category', NULL, 'Манометрические датчики-реле температуры', 'Манометрические датчики-реле температуры ТАМ, ТЧМ. Купить термореле, термостаты по цене производителя из наличия и под заказ со склада в Мо', 'ТДМВ-102 датчик-реле температуры манометрический взрывозащищенный', 'ru', 1, 'manometricheskie-datchiki-rele-temperatury.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/manometricheskie-datchiki-rele-temperatury/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(33, 22, 'Биметаллические датчики-реле температуры', 'bimetallicheskie-datchiki-rele-temperatury', 'category', NULL, 'Биметаллические датчики-реле температуры ДТКБ, ТАБ', 'Биметаллические датчики-реле температуры ДТКБ, ТАБ', 'ДТКБ датчик-реле температуры камерный биметаллический', 'ru', 1, 'bimetallicheskie-datchiki-rele-temperatury.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/bimetallicheskie-datchiki-rele-temperatury/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(34, 22, 'Дилатометрические датчики-реле температуры', 'dilatometricheskie-datchiki-rele-temperatury', 'category', NULL, 'Дилатометрические датчики-реле температуры ТА-1038, ТАД-101, ТУДЭ, ТДЭ', 'Дилатометрические датчики-реле температуры ТА-1038, ТАД-101, ТУДЭ, ТДЭ. Купить термореле (термостат) по цене производителя из наличия со склада ', 'ТМ-100В, ТМ-111, ТМ-111-А, ТМ-112, ТМ-112-А, ТМ-113, ТМ-113-А', 'ru', 1, 'dilatometricheskie-datchiki-rele-temperatury.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/dilatometricheskie-datchiki-rele-temperatury/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(35, 22, 'Электронные датчики-реле температуры', 'elektronnye-datchiki-rele-temperatury', 'category', NULL, 'Электронные датчики-реле температуры Т419, РТ-015, Т422, Т425, ТРЭ-201', 'Электронные датчики-реле температуры Т419, Т422, Т425, ТРЭ-201. Купить термореле по цене производителя, в наличии и под заказ со склада в Москве.', 'датчики-реле температуры РТ-015, RT-015, ERT, RTE, датчики температуры рт-015, датчики рт-015', 'ru', 1, 'elektronnye-datchiki-rele-temperatury.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/elektronnye-datchiki-rele-temperatury/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(36, 2, 'Термометры', 'termometry', 'category', NULL, 'Термометры технические показывающие контактные', 'Термометры технические показывающие контактные: стеклянные, биметаллические, манометрические, электроконтактные, самопишущие, цифровые (', 'Термометры лабораторные ТЛ-2 ртутные стеклянные -30 до +350°С', 'ru', 1, 'termometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/termometry/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(37, 23, 'Биметаллические термометры (механические)', 'bimetallicheskie-termometry-mehanicheskie', 'category', NULL, 'Термометры биметаллические (механические) показывающие ТБ, БТ, ТБП', 'Термометры биметаллические (механические) показывающие  ТБ, БТ, ТБП', 'Термоманометр МПТ-d80-РШ/ОШ, Термоманометр МПТ-d100-РШ/ОШ', 'ru', 1, 'bimetallicheskie-termometry-mehanicheskie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/bimetallicheskie-termometry-mehanicheskie/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(38, 23, 'Манометрические термометры (газовые)', 'manometricheskie-termometry-gazovye', 'category', NULL, 'Манометрические термометры газовые - ТГП и конденсационные - ТКП', 'Манометрические термометры (газовые - ТГП и конденсационные - ТКП): показывающие и электроконтактные - ЭК (сигнализирующие - СГ), виброустойч', 'ТГП-Э, термометр ТГП-Э', 'ru', 1, 'manometricheskie-termometry-gazovye.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/manometricheskie-termometry-gazovye/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(39, 23, 'Цифровые (электронные) термометры', 'cifrovye-elektronnye-termometry', 'category', NULL, 'Цифровые (электронные) термометры контактные ТК-5-01...11 портативные (переносные)', 'Цифровые (электронные) термометры контактные ТК-5-01..11 портативные (переносные). Купить ТК5 по цене производителя в наличии.', 'IT-8 термометр цифровой портативный', 'ru', 1, 'cifrovye-elektronnye-termometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/zifrovye-elektron-termometr/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(40, 23, 'Жидкостные (стеклянные) термометры', 'jidkostnye-steklyannye-termometry', 'category', NULL, 'Термометры стеклянные (жидкостные и ртутные)', 'Термометры стеклянные (жидкостные и ртутные) прямые и угловые: ТТ (ТТП, ТТУ), ТТЖ-П(У), ТЖСТ, ТТК-П(У), СП-2П(2У), ТЛ, ТР, ТН, ТИН, ТС, СП-1, ТТ-В, оправы ', 'Термометры лабораторные ТЛ-2 ртутные стеклянные -30 до +350°С', 'ru', 1, 'jidkostnye-steklyannye-termometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/zhidkostnye-steklyannye-termometry/', 0, NULL, NULL, '', '2019-06-24 10:16:14', NULL),
(41, 23, 'Гигрометры психрометрические', 'gigrometry-psihrometricheskie', 'category', NULL, 'Гигрометры психрометрические ВИТ-1,-2,-3', 'Гигрометры психрометрические ВИТ-1,-2,-3', 'Гигрометр ВИТ-1 ВИТ-2 психрометры', 'ru', 1, 'gigrometry-psihrometricheskie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gigrometry-psihrometricheskie/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(42, 2, 'Пирометры и тепловизоры', 'pirometry-i-teplovizory', 'category', NULL, 'Пирометры и тепловизоры', 'Пирометры С20, С500 и тепловизоры купить по цене производителя в наличии и под заказ со склада в Москве.', 'комплекс пирометров АПИР-С', 'ru', 1, 'pirometry-i-teplovizory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/pirometry/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(43, 2, 'Терморегуляторы', 'termoregulyatory', 'category', NULL, 'Терморегулятор РТ-ДО(ДЗ) - регулятор температуры прямого действия', 'Терморегуляторы РТ-ДО(ДЗ) регулятор температуры прямого действия купить по цене производителя.', 'Регулятор температуры прямого действия РТПД', 'ru', 1, 'termoregulyatory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/termoregulyatory/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(44, 0, 'Давление', 'davlenie', 'category', NULL, 'Приборы контроля давления: преобразователи, датчика-реле, манометры/вакуумметры, напоромеры/тягомеры, дифманометры, микроманометры, задат', 'Приборы контроля давления: преобразователи, датчика-реле, манометры/вакуумметры, напоромеры/тягомеры, дифманометры, микроманометры, задат', 'ПД100И-Exi, ПД100-Exd датчик давления', 'ru', 1, 'davlenie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/davlenie/', 0, NULL, 0, '127.0.0.1', '2019-06-24 10:16:10', NULL),
(45, 3, 'Преобразователи давления', 'preobrazovateli-davleniya', 'category', NULL, 'Преобразователи давления избыточного, дифференциального, абсолютного', 'Датчики давления цена от 2200 руб.+ндс. Преобразователи избыточного, абсолютного, дифференциального (разницы перепада), вакуумметрического-р', 'ПД100И-Exi, ПД100-Exd датчик давления', 'ru', 1, 'preobrazovateli-davleniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/preobrazovateli-davleniya/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(46, 31, 'Преобразователи давления в токовый (мА) и цифровой сигнал (протокол)', 'preobrazovateli-davleniya-v-tokovyy-ma-i-cifrovoy-signal-protokol', 'category', NULL, 'Преобразователи/датчики давления в токовый сигнал 4-20мА и HART-протокол', 'Датчики давления от 2200р+ндс. Преобразователи избыточного, дифференциального/перепада, абсолютного в токовый сигнал 0-5мА/4-20мА и HART-протокол', 'ПД100И-Exi, ПД100-Exd датчик давления', 'ru', 1, 'preobrazovateli-davleniya-v-tokovyy-ma-i-cifrovoy-signal-protokol.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/preobrazovateli-davleniya-v-tokovyj-ma-i-tsifrovoj-signal-protokol/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(47, 31, 'Преобразователи давления в сигнал напряжения постоянного тока (В)', 'preobrazovateli-davleniya-v-signal-napryajeniya-postoyannogo-toka-v', 'category', NULL, 'Преобразователи давления в сигнал напряжения постоянного тока (В)', 'Преобразователи давления в сигнал напряжения постоянного тока (В)', 'ЗОНД-20-К, ЗОНД-20К', 'ru', 1, 'preobrazovateli-davleniya-v-signal-napryajeniya-postoyannogo-toka-v.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/preobrazovateli-davleniya-v-signal-napryazheniya-postoyannogo-toka-v/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(48, 31, 'Преобразователи давления в сигнал взаимной индуктивности 0-10мГн', 'preobrazovateli-davleniya-v-signal-vzaimnoy-induktivnosti-0-10mgn', 'category', NULL, 'Индуктивные преобразователи давления в сигнал взаимной индуктивности 0-10мГн', 'Индуктивные преобразователи давления и перепада в сигнал взаимной индуктивности 0-10мГн. Купить МЭД, ДМ3583М, ДД-41003/4 по цене производителя из ', 'ПДТ, ПД-Т', 'ru', 1, 'preobrazovateli-davleniya-v-signal-vzaimnoy-induktivnosti-0-10mgn.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/preobrazovateli-davleniya-v-signal-vzaimnoj-induktivnosti-0-10mgn/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(49, 31, 'Преобразователи давления в пневматический выходной сигнал 20-100кПа', 'preobrazovateli-davleniya-v-pnevmaticheskiy-vyhodnoy-signal-20-100kpa', 'category', NULL, 'Пневматические преобразователи (датчики), выход 20-100кПа', 'Пневматические преобразователи давления в выходной сигнал 20-100кПа. Купить пневмо-датчики по цене производителя.', 'РДФ-3-I, РДФ-3-II редукторы давления с фильтром', 'ru', 1, 'preobrazovateli-davleniya-v-pnevmaticheskiy-vyhodnoy-signal-20-100kpa.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/preobrazovateli-davleniya-v-pnevmaticheskij-vyhodnoj-signal-20-100kpa-0-2-1kgs-sm2/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(50, 31, 'Преобразователи гидростатического давления (датчики уровня)', 'preobrazovateli-gidrostaticheskogo-davleniya-datchiki-urovnya', 'category', NULL, 'Преобразователи (датчики) гидростатического давления погружные зонды и врезные уровнемеры купить по цене производителя в наличии и под за', 'Преобразователи (датчики) гидростатического давления погружные зонды и врезные уровнемеры можно купить по цене производителя в наличии и ', 'Погружной зонд, датчик уровня LMP 307I, ЛМП307и', 'ru', 1, 'preobrazovateli-gidrostaticheskogo-davleniya-datchiki-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/preobrazovateli-gidrostaticheskogo-davleniya-datchiki-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(51, 31, 'Нестандартные специальные преобразователи давления', 'nestandartnye-specialnye-preobrazovateli-davleniya', 'category', NULL, 'Нестандартные специальные преобразователи (датчики) давления', 'Нестандартные специальные преобразователи (датчики) давления: многократная перегрузка, нестандартный диапазон, демпфирование выходного ?', 'ММ111В, ММ111Д, ДЕ1050, ММ124Д, ММ125Д, ММ126, ММ128, ММ129, ММ106 автомобильные датчики давления', 'ru', 1, 'nestandartnye-specialnye-preobrazovateli-davleniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/nestandartnye-spetsialnye-pd/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(52, 3, 'Датчики-реле давления', 'datchiki-rele-davleniya', 'category', NULL, 'Реле давления РД, ДЕМ и перепада РДД (дифференциальные сигнализаторы разности)', 'Датчики-реле давления РД, ДЕМ и перепада РДД (дифференциальные сигнализаторы разности ДД ДРД РРД), напора ДН и тяги ДТ, купить по цене произв', 'Реле давления РД, РД-У, купить РД, РД-У', 'ru', 1, 'datchiki-rele-davleniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchiki-rele-davleniya/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(53, 32, 'Датчики-реле давления, напора, тяги', 'datchiki-rele-davleniya-napora-tyagi', 'category', NULL, 'Датчики-реле давления РД (сигнализаторы напора и тяги).', 'Датчики-реле давления (напора) и разряжения (тяги) РД. Купить сигнализаторы (прессостаты) по цене производителя из наличия и под заказ со скл', 'Реле давления РД, РД-У, купить РД, РД-У', 'ru', 1, 'datchiki-rele-davleniya-napora-tyagi.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchiki-rele-davleniya-napora-tyagi/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(54, 32, 'Датчики-реле разности давлений (дифференциальные)', 'datchiki-rele-raznosti-davleniy-differencialnye', 'category', NULL, 'Дифференциальные датчики-реле разности давления (сигнализаторы перепада)', 'Дифференциальные датчики-реле разности давления. Купить сигнализаторы перепада по цене производителя из наличия и под заказ со склада в М?', 'Датчики-реле давления ДЕМ-102...301', 'ru', 1, 'datchiki-rele-raznosti-davleniy-differencialnye.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchiki-rele-raznosti-davlenij-differentsialnye/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(55, 3, 'Манометры, Напоромеры, Дифманометры', 'manometry-naporomery-difmanometry', 'category', NULL, 'Манометры (вакуумметры), напоромеры (тягомеры), дифманометры', 'Манометры (вакуумметры), напоромеры (тягомеры), дифманометры показывающие купить по цене производителя в наличии и под заказ со склада в Мо?', 'САМТ-02, САМТ-03', 'ru', 1, 'manometry-naporomery-difmanometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/manometry-naporomery-difmanometry/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(56, 33, 'Дифманометры', 'difmanometry', 'category', NULL, 'Дифференциальный манометр (дифманометр) ДСП, ДСС, МДМ, МДП, МВ, ДМЦ, ЗОНД, ДНМ ДТМ ДТНМ', 'Дифференциальные манометры (дифманометры): ДСП, ДСС, МДМ, МДП, МВ, ДМЦ, ЗОНД, Testo, ДНМ ДТМ ДТНМ и др. Как купить по цене производителя в наличии ?', 'Напорная трубка Пито', 'ru', 1, 'difmanometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/difmanometry/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(57, 33, 'Напоромеры', 'naporomery', 'category', NULL, 'Напоромеры (тягомеры тягонапоромеры) показывающие сигнализирующие взрывозащищенные коррозионностойкие купить по цене производителя в н?', 'Напоромеры (тягомеры тягонапоромеры) показывающие сигнализирующие взрывозащищенные коррозионностойкие купить по цене производителя в н?', 'БАММ-1, БАМ-1', 'ru', 1, 'naporomery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/naporomery/', 0, NULL, NULL, '', '2019-06-24 10:16:15', NULL),
(58, 33, 'Манометры', 'manometry', 'category', NULL, 'Манометры МП ДМ ТМ МО МТИ', 'Манометры МП ДМ ТМ МО МТИ', 'САМТ-02, САМТ-03', 'ru', 1, 'manometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/manometry/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(59, 3, 'Метрологическое оборудование', 'metrologicheskoe-oborudovanie', 'category', NULL, 'Задатчики давления:  пресса, грузопоршневые манометры МП, калибраторы давления, микроманометры купить по цене производителя в наличии и п?', 'Задатчики давления:  пресса, грузопоршневые манометры МП, калибраторы давления, микроманометры купить по цене производителя в наличии и п?', 'ПОИСК-600 Многодиапазонный поверочный и измерительный комплекс Набор для поверки и измерения', 'ru', 1, 'metrologicheskoe-oborudovanie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/metrologicheskoe-oborudovanie/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(60, 34, 'Микроманометры', 'mikromanometry', 'category', NULL, 'Микроманометры образцовые МКВ-250, МКВК-250-0,02%, ПМКМ-1 и рабочие ММН-2400(240), по цене производителя в наличии и под заказ со склада в Москве.', 'Микроманометры образцовые МКВ-250, МКВК-250-0,02%, ПМКМ-1 и рабочие ММН-2400(240), по цене производителя в наличии и под заказ со склада в Москве.', 'мановакуумметр двухтрубный МВ, мановакууметр U-образный МВ, МВ2500 МВ3600 МВ5000 МВ6000 МВ10000 МВ20000', 'ru', 1, 'mikromanometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/mikromanometry/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(61, 34, 'Задатчики давления', 'zadatchiki-davleniya', 'category', NULL, 'Задатчики давления: пресс манометрический гидравлический переносной 2113/2113М, ПУМ МП', 'Задатчики давления: пресс манометрический гидравлический переносной 2113/2113М, ПУМ МП', 'ПОИСК-600 Многодиапазонный поверочный и измерительный комплекс Набор для поверки и измерения', 'ru', 1, 'zadatchiki-davleniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/zadatchiki-davleniya/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(62, 34, 'Грузопоршневые манометры', 'gruzoporshnevye-manometry', 'category', NULL, 'Грузопоршневой манометр МП: МП2,5 МП6 МП60 МП250 МП600 МП2500 МГП МПД МПП МГЦ МГЦ-100 ППКМ ППКМ МПМ МПК МВП МПАК', 'Грузопоршневые манометры МП: МП2,5 МП6 МП60 МП250 МП600 МП2500 МГП МПД МПП МГЦ МГЦ-100 ППКМ ППКМ МПМ МПК МВП МПАК купить по цене производителя из нали?', 'ПГК привод грузопоршневой колонки, ПГМ платформа для грузопоршевого манометра', 'ru', 1, 'gruzoporshnevye-manometry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gruzoporsh/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(63, 3, 'Дополнительное оборудование (давление)', 'dopolnitelnoe-oborudovanie-davlenie', 'category', NULL, 'Отборные устройства, импульсные трубки (линии), краны и клапаны манометровые, переходники, демпферы, охладители, разделители мембранные РМ', 'Дополнительное и вспомогательное оборудование для приборов контроля давления (манометров, напоромеров, датчиков-реле, преобразователей): ', 'VE-PACKO кран кнопочный для манометра купить наличие москва', 'ru', 1, 'dopolnitelnoe-oborudovanie-davlenie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/additional-equipment-pressure/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(64, 35, 'Защитно-разделительные устройства', 'zashchitno-razdelitelnye-ustroystva', 'category', NULL, 'Защитно-разделительные устройства', 'Защитно-разделительные устройства', 'Термочехол без/с подогревом для КИПиА', 'ru', 1, 'zashchitno-razdelitelnye-ustroystva.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/zashhitno-razdelitelnye-ustrojstva/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(65, 35, 'Монтажно-подводящая арматура', 'montajno-podvodyashchaya-armatura', 'category', NULL, 'Монтажно-подводящая арматура для манометров и преобразователей (датчиков) давления', 'Монтажно-подводящая арматура для манометров (вакуумметров и мановакуумметров) и простых преобразователей (датчиков) давления', 'VE-PACKO кран кнопочный для манометра купить наличие москва', 'ru', 1, 'montajno-podvodyashchaya-armatura.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/montazhno-podvodyashhaya-armatura/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(66, 0, 'Расход', 'rashod', 'category', NULL, 'Расходомеры-счетчики жидкости (воды), газа, пара. Ротаметры, регуляторы, реле.', 'Расходомеры-счетчики жидкости (воды), газа, пара. Купить ротаметры, регуляторы, реле по цене производителя под заказ и со склада в Москве.', 'счетчики ОСВХ НЕПТУН (Ду 15, 20, 25, 32, 40) метрологического класса С крыльчатые одноструйные холодной воды', 'ru', 1, 'rashod.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/rashod/', 0, NULL, 0, '127.0.0.1', '2019-06-24 10:16:10', NULL);
INSERT INTO `basis` (`id`, `id_parent`, `name`, `name_translit`, `type`, `content`, `title`, `description`, `keywords`, `lang`, `order_num`, `img`, `target`, `popular`, `mime_type`, `user_id`, `ip`, `created`, `closed`) VALUES
(67, 4, 'Расходомеры жидкости', 'rashodomery-jidkosti', 'category', NULL, 'Расходомеры жидкости', 'Расходомер жидкости - прибор, измеряющий объемный расход или массовый расход жидкости, т. е. количество вещества (объем, масса), проходящее ч', 'счетчики ОСВХ НЕПТУН (Ду 15, 20, 25, 32, 40) метрологического класса С крыльчатые одноструйные холодной воды', 'ru', 1, 'rashodomery-jidkosti.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/rashodomery-zhidkosti/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(68, 41, 'Квартирные счетчики воды (водосчетчики)', 'kvartirnye-schetchiki-vody-vodoschetchiki', 'category', NULL, 'Счетчики воды (водосчетчики) индивидуальные (квартирные, бытовые) Ду15', 'Счетчики воды (водосчетчики) индивидуальные (квартирные, бытовые). Купить счётчики холодной и горячей воды Ду15, Ду20 по цене производителя и?', 'счетчики ОСВХ НЕПТУН (Ду 15, 20, 25, 32, 40) метрологического класса С крыльчатые одноструйные холодной воды', 'ru', 1, 'kvartirnye-schetchiki-vody-vodoschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kvartirnye-schetchiki-vody/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(69, 41, 'Тахометрические расходомеры-счетчики', 'tahometricheskie-rashodomery-schetchiki', 'category', NULL, 'Тахометрические расходомеры-счетчики воды (водосчетчики турбинные)', 'Тахометрические турбинные расходомеры-счетчики жидкости (в т.ч. горячей и холодной воды). Купить водосчетчики Ду25 и более по цене производи', 'ВСХН-25,-32,-40, ВСГН-25,-32,40', 'ru', 1, 'tahometricheskie-rashodomery-schetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/tahometricheskie-rashodomery/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(70, 41, 'Электромагнитные расходомеры', 'elektromagnitnye-rashodomery', 'category', NULL, 'Электромагнитные расходомеры-счетчики (преобразователи расхода) воды', 'Электромагнитные расходомеры-счетчики (преобразователи расхода) воды полнопроходные и погружные. Купить водосчетчик по цене производите?', 'ВЭПС-Р преобразователь расхода вихревой электромагнитный', 'ru', 1, 'elektromagnitnye-rashodomery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/elektromagnitnye-rashodomery/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(71, 41, 'Ультразвуковые расходомеры', 'ultrazvukovye-rashodomery', 'category', NULL, 'Ультразвуковые счетчики-расходомеры (преобразователи расхода) жидкости (воды).', 'Ультразвуковые счетчики-расходомеры врезные и накладные (портативные). Купить акустические преобразователи расхода воды и других жидкост', 'преобразователь расхода SonoSensor 30 ультразвуковой расходомер купить производитель наличие заказ склад Москва', 'ru', 1, 'ultrazvukovye-rashodomery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ultrazvukovye-rashodomery/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(72, 41, 'Вихревые расходомеры', 'vihrevye-rashodomery', 'category', NULL, 'Вихревые расходомеры-счетчики (преобразователи расхода) воды.', 'Вихревые расходомеры-счетчики (преобразователи расхода) воды. Купить водосчетчик по цене производителя, в наличии и под заказ на складе в М', 'ВЭПС-Р преобразователь расхода вихревой электромагнитный', 'ru', 1, 'vihrevye-rashodomery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/vihrevye-rashodomery/', 0, NULL, NULL, '', '2019-06-24 10:16:16', NULL),
(73, 41, 'Счетчики нефтепродуктов', 'schetchiki-nefteproduktov', 'category', NULL, 'Счетчики жидкости и нефтепродуктов (бензина, дизельного топлива/солярки, керосина, мазута и пр.)', 'Специальные счетчики жидкости СЖ: нефтепродуктов (бензина, дизельного топлива/солярки, керосина, мазута и др.), а также других вязких и агре', 'датчик ДИ-О-5', 'ru', 1, 'schetchiki-nefteproduktov.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/schetchiki-nefteproduktov/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(74, 41, 'Массовые кориолисовые расходомеры', 'massovye-koriolisovye-rashodomery', 'category', NULL, 'Массовые кориолисовые расходомеры', 'Массовые кориолисовые расходомеры. Купить по цене производителя, в наличии и под заказ со склада в Москве.<br />', 'ЭМИС-МАСС 260 расходомер массовый кориолисовый', 'ru', 1, 'massovye-koriolisovye-rashodomery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/massovye-koriolisovye-rashodomery/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(75, 41, 'Комбинированные водосчетчики', 'kombinirovannye-vodoschetchiki', 'category', NULL, 'Комбинированные счетчики воды (водосчетчики сдвоенные)', 'Комбинированные счетчики воды (водосчетчики сдвоенные)', 'КВМ-50, КВМ-80 счетчики воды комбинированные (Ду 50, 80мм)', 'ru', 1, 'kombinirovannye-vodoschetchiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kombinirovannye-vodoschetchiki/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(76, 4, 'Расходомеры газа', 'rashodomery-gaza', 'category', NULL, 'Расходомеры-счетчики газа турбинные, ротационные, вихревые, мембранные, ультразвуковые', 'Расходомеры-счетчики газа турбинные, ротационные, вихревые, мембранные, ультразвуковые, купить по цене производителя под заказ и из наличи', 'Измеритель расхода и скорости дымовых газов ИС-14.М', 'ru', 1, 'rashodomery-gaza.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/rashodomery-gaza/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(77, 42, 'Бытовые счетчики газа', 'bytovye-schetchiki-gaza', 'category', NULL, 'Бытовой счетчик газа СГ-G1 G1,6 G2,5 G4 G6 м3/час)', 'Бытовой счетчик газа (далее СГ-G1 G1,6 G2,5 G4 G6 м3/час) купить по цене производителя из наличия и под заказ со скдада в Москве.', 'Г6 Берестье счетчик газа бытовой для измерения объемного расхода', 'ru', 1, 'bytovye-schetchiki-gaza.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/schetchiki-gaza-bytovye/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(78, 42, 'Промышленные счетчики газа', 'promyshlennye-schetchiki-gaza', 'category', NULL, 'Промышленные счетчики газа (турбинные, вихревые, ультразвуковые >G40)', 'Промышленные счетчики газа. Купить турбинные, вихревые, ультразвуковые расходомеры G40 и более, по цене производителя впод заказ и в наличии', 'СВГ.М, СВГ.МЗ, СВГ.МЗЛ счетчики газа вихревые', 'ru', 1, 'promyshlennye-schetchiki-gaza.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/promyshlennye-schetchiki-gaza/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(79, 42, 'Ультразвуковые счетчики газа', 'ultrazvukovye-schetchiki-gaza', 'category', NULL, 'Коммунальные ультразвуковые счетчики газа (G16; G25)', 'Коммунальные ультразвуковые счетчики газа (G16; G25)', 'ГиперФлоу-УС комплекс измерительный ультразвуковой', 'ru', 1, 'ultrazvukovye-schetchiki-gaza.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ultrazvukovye-schetchiki-gaza/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(80, 42, 'Корректоры объема газа', 'korrektory-obema-gaza', 'category', NULL, 'Корректоры объема газа', 'ГК Теплоприбор: приборы и автоматика/КИПиА для АСУТП: цена производителя в наличии и под заказ', 'ОКВГ-01', 'ru', 1, 'korrektory-obema-gaza.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/korrektory-obema-gaza/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(81, 42, 'Комплексы измерительные', 'kompleksy-izmeritelnye', 'category', NULL, 'Комплексы измерительные', 'ГК Теплоприбор: приборы и автоматика/КИПиА для АСУТП: цена производителя в наличии и под заказ', 'Измеритель расхода и скорости дымовых газов ИС-14.М', 'ru', 1, 'kompleksy-izmeritelnye.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kompleksy-izmeritelnye/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(82, 4, 'Расходомеры пара', 'rashodomery-para', 'category', NULL, 'Струйные и вихревые счетчики-расходомеры (первичные преобразователи расхода воды/жидкости, газа и пара).', 'Струйные и вихревые первичные преобразователи расхода - универсальные счетчики-расходомеры воды и других жидкостей, газов (воздуха и газо?', 'Расходомер ЭМИС-ВИХРЬ 200, ЭВ-200', 'ru', 1, 'rashodomery-para.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/rashodomery-para/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(83, 43, 'Струйные расходомеры', 'struynye-rashodomery', 'category', NULL, 'Струйные расходомеры', 'ГК Теплоприбор: приборы и автоматика/КИПиА для АСУТП: цена производителя в наличии и под заказ', 'РМ-5-ПГ расходомер пара и газа', 'ru', 1, 'struynye-rashodomery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/strujnye-rashodomery-rashodomery-para/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(84, 43, 'Вихревые расходомеры', 'vihrevye-rashodomery', 'category', NULL, 'Вихревые расходомеры', 'ГК Теплоприбор: приборы и автоматика/КИПиА для АСУТП: цена производителя в наличии и под заказ', 'Расходомер ЭМИС-ВИХРЬ 200, ЭВ-200', 'ru', 1, 'vihrevye-rashodomery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/vihrevye-rashodomery-rashodomery-para/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(85, 4, 'Регуляторы расхода, реле потока', 'regulyatory-rashoda-rele-potoka', 'category', NULL, 'Регуляторы расхода и реле потока/протока жидкости (воды), газа (воздуха), пара.', 'Регуляторы расхода и датчики-реле потока (протока, протекания) жидкости (воды), газа (воздуха), пара.', 'реле скорости воздуха DWYER-530, реле воздушного потока DWYER-530', 'ru', 1, 'regulyatory-rashoda-rele-potoka.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/regulator-rele/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(86, 44, 'Регуляторы расхода', 'regulyatory-rashoda', 'category', NULL, 'Регуляторы расхода жидкости (воды), газа (воздуха) и пара РР-НО.', 'Регуляторы расхода жидкости (воды), газа (воздуха) и пара. Купить регулятор перепада давления РПД-РР-НО по цене производителя, в наличии со с?', 'Регуляторы давления и перепада давлений универсальные УРРД', 'ru', 1, 'regulyatory-rashoda.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/regulyatory-rashoda/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(87, 44, 'Реле потока жидкости', 'rele-potoka-jidkosti', 'category', NULL, 'Датчики-реле потока  РПИ, FS-1/2, РПЖ-1М, ДР-П, ДРУ-ПП (протока расхода жидкости)', 'Датчики-реле потока  РПИ, FS-1/2, РПЖ-1М, ДР-П, ДРУ-ПП (протока расхода жидкости)', 'СТД-31 сигнализатор уровня и потока термодифференциальный', 'ru', 1, 'rele-potoka-jidkosti.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/rele-potoka-zhidkosti/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(88, 44, 'Датчики-реле потока воздуха', 'datchiki-rele-potoka-vozduha', 'category', NULL, 'Датчики-реле потока воздуха ДРПВ', 'Датчики-реле потока воздуха ДРПВ', 'реле скорости воздуха DWYER-530, реле воздушного потока DWYER-530', 'ru', 1, 'datchiki-rele-potoka-vozduha.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchik-rele-potoka-vozduha-drpv/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(89, 4, 'Ротаметры, диафрагмы и сопла', 'rotametry-diafragmy-i-sopla', 'category', NULL, 'Ротаметры, диафрагмы и сопла', 'Ротаметры, диафрагмы и сопла', 'Ротаметры РМ, РМ-А, РМФ: жидкостные-ЖУ3 и газовые-ГУ3', 'ru', 1, 'rotametry-diafragmy-i-sopla.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/rotametr-diafragma-soplo/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(90, 45, 'Ротаметры', 'rotametry', 'category', NULL, 'Ротаметр', 'Ротаметр', 'Ротаметры РМ, РМ-А, РМФ: жидкостные-ЖУ3 и газовые-ГУ3', 'ru', 1, 'rotametry.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/rotametr/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(91, 45, 'Диафрагмы и сопла', 'diafragmy-i-sopla', 'category', NULL, 'Диафрагмы и сопла', 'Диафрагмы и сопла', 'ДКС, ДБС, ДФК, ДВС, ДФС диафрагмы для расходомеров', 'ru', 1, 'diafragmy-i-sopla.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/diafragma-soplo/', 0, NULL, NULL, '', '2019-06-24 10:16:17', NULL),
(92, 4, 'Доп', 'dop', 'category', NULL, 'Доп. оборудование для счетчиков-расходомеров: КМЧ, арматура, краны, редуктора, фильтры жидкости(воды) и газа.', 'Доп. оборудование для счетчиков-расходомеров: КМЧ, арматура, краны, редуктора, фильтры жидкости(воды) и газа.', 'Пневмоэлектроклапан ПЭКДД', 'ru', 1, 'dop.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/dop-oborudovanie-armatura-rashod/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(93, 46, 'Исполнительные механизмы и приводы', 'ispolnitelnye-mehanizmy-i-privody', 'category', NULL, 'Электрические исполнительные механизмы и приводы', 'Электрические исполнительные механизмы МЭО и МЭОФ и приводы. Купить электроприводы по цене изготовителя под заказ и со склада в Москве.', 'Пневмоэлектроклапан ПЭКДД', 'ru', 1, 'ispolnitelnye-mehanizmy-i-privody.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ispolnitelnyj-mekhanizm-privod/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(94, 46, 'Клапаны регулирующие', 'klapany-reguliruyushchie', 'category', NULL, 'Клапаны запорно-регулирующие КЗР, КР', 'Клапаны запорно-регулирующие КЗР, КР', 'Антифриз - теплоноситель Dixis-65 (Диксис-65)', 'ru', 1, 'klapany-reguliruyushchie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/klapan-reguliruyushchij/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(95, 46, 'Клапаны электромагнитные', 'klapany-elektromagnitnye', 'category', NULL, 'Клапаны электромагнитные (соленоидные)', 'Клапаны электромагнитные (соленоидные)', 'ЭМКГ8 для пиролизных газов', 'ru', 1, 'klapany-elektromagnitnye.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/klapan-ehlektromagnitnyj/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(96, 0, 'Уровень', 'uroven', 'category', NULL, 'Уровнемеры: преобразователи, датчики-реле и сигнализаторы уровня жидкости и сыпучих продуктов.', 'Уровнемеры: преобразователи, датчики-реле и сигнализаторы уровня жидкости (воды) и сыпучих продуктов. Купить приборы и системы контроля ур?', 'СУР-15 сигнализатор уровня ультразвуковой', 'ru', 1, 'uroven.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/uroven/', 0, NULL, 0, '127.0.0.1', '2019-06-24 10:16:10', NULL),
(97, 5, 'Уровнемеры', 'urovnemery', 'category', NULL, 'Уровнемеры: поплавковые, ультразвуковые (акустические); радарные (радиоволновые), буйковые и гидростатические.', 'Уровнемеры: поплавковые, ультразвуковые (акустические); радарные (радиоволновые), буйковые и гидростатические.', 'SLL-440F SLL-440M StreamLux уровнемер ультразвуковой  купить цена производитель наличие заказ склад Москва', 'ru', 1, 'urovnemery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/urovnemery/', 0, NULL, NULL, '', '2019-06-24 10:16:11', NULL),
(98, 51, 'Ультразвуковые уровнемеры', 'ultrazvukovye-urovnemery', 'category', NULL, 'Уровнемер ультразвуковой (датчик уровня акустический)', 'Уровнемеры ультразвуковые - датчики уровня акустические и унифицированным выходным сигналом. Купить преобразователь уровня по цене произ', 'SLL-440F SLL-440M StreamLux уровнемер ультразвуковой  купить цена производитель наличие заказ склад Москва', 'ru', 1, 'ultrazvukovye-urovnemery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ultrazvukovye-urovnemery/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(99, 51, 'Радарные уровнемеры', 'radarnye-urovnemery', 'category', NULL, 'Уровнемеры бесконтактные: радарный, радиоволновый, микроволновый датчик-преобразователь уровня', 'Уровнемеры бесконтактные: радарный, радиоволновый, микроволновый датчик-преобразователь уровня', 'МПУ-Р5 уровнемер радарный датчик уровня', 'ru', 1, 'radarnye-urovnemery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/radarnye-urovnemery/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(100, 51, 'Поплавковые уровнемеры', 'poplavkovye-urovnemery', 'category', NULL, 'Поплавковый уровнемер (датчик-преобразователь уровня в выходной сигнал 0-5/4-20мА)', 'Поплавковый уровнемер. Купить датчик-преобразователь уровня в выходной сигнал 0-5/4-20мА по цене производителя в наличии и под заказ со склад?', 'УПП-11 поплавковый уровнемер', 'ru', 1, 'poplavkovye-urovnemery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/poplavkovye-urovnemery/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(101, 5, 'Преобразователи уровня', 'preobrazovateli-urovnya', 'category', NULL, 'Преобразователи уровня (буйковые, гидростатические) - датчики с выходным токовым сигналом 0-5мА, 4-20мА', 'Преобразователи уровня (буйковые, гидростатические) - датчики с выходным токовым сигналом 0-5мА, 4-20мА, купить по цене производителя из налич?', 'Погружной зонд, датчик уровня LMP 307I, ЛМП307и', 'ru', 1, 'preobrazovateli-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/preobrazovateli-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(102, 52, 'Буйковые преобразователи уровня', 'buykovye-preobrazovateli-urovnya', 'category', NULL, 'Буйковый уровнемер - преобразователь уровня', 'Буйковый уровнемер - преобразователь уровня, купить Сапфир-22-ДУ, ПИУП-М, УБ-ЭМ, СКБ, УРБ-П по цене производителя из наличия и под заказ со скла', 'УРБ-П, УРБ-ПМ уровнемеры буйковые', 'ru', 1, 'buykovye-preobrazovateli-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/bujkovye-preobrazovateli-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(103, 52, 'Радиолокационные преобразователи уровня', 'radiolokacionnye-preobrazovateli-urovnya', 'category', NULL, 'Радиолокационный датчик уровня РДУ-Х2,-Х8 (преобразователь)', 'Радиолокационные датчики уровня РДУ-Х2,-Х8 (преобразователи)', 'УР-203-Ex, УР-203Ex', 'ru', 1, 'radiolokacionnye-preobrazovateli-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/radiolokatsionnye-preobrazovateli-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(104, 52, 'Гидростатические датчики уровня', 'gidrostaticheskie-datchiki-urovnya', 'category', NULL, 'Гидростатические датчики уровня - уровнемеры', 'Гидростатические датчики уровня - уровнемеры врезного (штуцерного/фланцевого) или погружного (зондового) типа.', 'Погружной зонд, датчик уровня LMP 307I, ЛМП307и', 'ru', 1, 'gidrostaticheskie-datchiki-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gidrostaticheskie-datchiki-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(105, 5, 'Датчики-реле уровня', 'datchiki-rele-urovnya', 'category', NULL, 'Датчик-реле уровня емкостной, поплавковый, кондуктометрический', 'Датчики-реле (сигнализаторы) уровня емкостные, поплавковые, кондуктометрические, купить по цене производителя в наличии и под заказ со скл?', 'СПАС-24 система предупредительно-аварийной сигнализации обнаружения воды в трюмах подвалах зданий утечки протечки', 'ru', 1, 'datchiki-rele-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/datchiki-rele-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(106, 53, 'Поплавковые датчики-реле уровня', 'poplavkovye-datchiki-rele-urovnya', 'category', NULL, 'Поплавковые датчики-реле уровня', 'Поплавковые датчики-реле уровня (ПДУ, ДРУ, РОС, ПМП). Купить герконовые сигнализаторы по цене производителя в наличии и под заказ со склада ?', 'ДУЖП-200М', 'ru', 1, 'poplavkovye-datchiki-rele-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/poplavkovye-datchiki-rele-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(107, 53, 'Емкостные датчики-реле уровня', 'emkostnye-datchiki-rele-urovnya', 'category', NULL, 'Емкостные датчики-реле уровня', 'Емкостные датчики-реле уровня, купить РИС, РОС, ДУЕ, ИСУ, ПУМА по цене производителя в наличии и под заказ со склада в Москве.', 'ПУМА-100, ПУМА100', 'ru', 1, 'emkostnye-datchiki-rele-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/emkostnye-datchiki-rele-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:18', NULL),
(108, 53, 'Кондуктометрические датчики-реле уровня', 'konduktometricheskie-datchiki-rele-urovnya', 'category', NULL, 'Кондуктометрические датчики-реле уровня жидкости (регуляторы-сигнализаторы)', 'Кондуктометрические датчики-реле уровня РОС-301, ЭРСУ, САУ-М6, ESP-50, ДРУ, СКБ-301-DIN, БКК1 (регулятор-сигнализатор уровня жидкости) вертикального ?', 'СПАС-24 система предупредительно-аварийной сигнализации обнаружения воды в трюмах подвалах зданий утечки протечки', 'ru', 1, 'konduktometricheskie-datchiki-rele-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/konduktometricheskie-datchiki-rele-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(109, 5, 'Сигнализаторы уровня', 'signalizatory-urovnya', 'category', NULL, 'Сигнализаторы уровня ультразвуковые, вибрационные, емкостные, поплавковые', 'Сигнализаторы уровня ультразвуковые, вибрационные, емкостные, поплавковые. Купить датчики-реле (регуляторы) по цене производителя в налич?', 'СУР-15 сигнализатор уровня ультразвуковой', 'ru', 1, 'signalizatory-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/signalizatory-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(110, 54, 'Поплавковые сигнализаторы уровня', 'poplavkovye-signalizatory-urovnya', 'category', NULL, 'Поплавковые сигнализаторы уровня герконовые', 'Поплавковые сигнализаторы уровня (датчики-реле герконовые СУГ-М СУЖ-П ПМП-066...145) купить по цене производителя в наличие и под заказ со склад', 'СУЖ-П-И Сигнализатор уровня жидкости СУЖ-П-И поплавковый взрывозащищенное исполнение цена производителя в наличии сроки поставки аналог', 'ru', 1, 'poplavkovye-signalizatory-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/poplavkovye-signalizatory-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(111, 54, 'Емкостные сигнализаторы уровня', 'emkostnye-signalizatory-urovnya', 'category', NULL, 'Емкостной сигнализатор уровня (ёмкостной регулятор)', 'Емкостные сигнализаторы уровня СУ, СУС, СУЖ. Купить ёмкостной регулятор уровня по цене производителя, в наличии и под заказ со склада в Моск', 'ИСУ-2000И, ИСУ2000И', 'ru', 1, 'emkostnye-signalizatory-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/emkostnye-signalizatory-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(112, 54, 'Ультразвуковые сигнализаторы уровня', 'ultrazvukovye-signalizatory-urovnya', 'category', NULL, 'Ультразвуковой сигнализатор уровня (датчик-реле акустический).', 'Сигнализатор уровня ультразвуковой. Купить акустические датчики-реле уровня по цене производителя, в наличии и под заказ со склада в Москв', 'СУР-15 сигнализатор уровня ультразвуковой', 'ru', 1, 'ultrazvukovye-signalizatory-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ultrazvukovye-signalizatory-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(113, 54, 'Вибрационные сигнализаторы уровня', 'vibracionnye-signalizatory-urovnya', 'category', NULL, 'Вибрационный сигнализатор уровня (датчик-реле)', 'Вибрационные сигнализаторы уровня. Купить вибро-датчики-реле по цене производителя, в наличии и под заказ со склада в Москве.', 'МИКРОТРОН СЖУ-1-МВ рефлекс-радарный микроволновый', 'ru', 1, 'vibracionnye-signalizatory-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/vibratsionnye-signalizatory-urovnya/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(114, 54, 'Термодифференциальные сигнализаторы', 'termodifferencialnye-signalizatory', 'category', NULL, 'Термодифференциальные сигнализаторы уровня и расхода (датчики потока)', 'Термодифференциальные сигнализаторы уровня и расхода. Купить датчики потока по цене производителя в наличии и под заказ со склада в Москв?', 'сигнализаторы уровня осадка СО-2, СО-1П', 'ru', 1, 'termodifferencialnye-signalizatory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/termodifferencialnyj-signalizator-datchik/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(115, 54, 'Мембранные сигнализаторы уровня', 'membrannye-signalizatory-urovnya', 'category', NULL, 'Мембранные сигнализаторы уровня СУМ', 'Мембранные сигнализаторы уровня СУМ', 'NMF, KOBOLD', 'ru', 1, 'membrannye-signalizatory-urovnya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/signalizator-urovnya-membrannyj-sum/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(116, 0, 'Автоматика и вторичные приборы', 'avtomatika-i-vtorichnye-pribory', 'category', NULL, 'Автоматика и вторичные приборы (измерительные, регулирующие и регистрирующие)', 'Автоматика и вторичные приборы (измерительные, регулирующие и регистрирующие). Купить КИПиА для АСУТП по цене производителя, в наличии и по', 'Шкафы контроля горелок ШКГ-1; -2; -3; -4; -6; -8, ШКГ-1Х/24, ШКГ-1Х/220, ШКГ-8Х/24', 'ru', 1, 'avtomatika-i-vtorichnye-pribory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/avtomatika-i-vtorichnye-pribory/', 0, NULL, 0, '127.0.0.1', '2019-06-24 10:16:10', NULL),
(117, 6, 'Измерители-регуляторы', 'izmeriteli-regulyatory', 'category', NULL, 'Измерители-регуляторы технологические', 'Измерители-регуляторы технологические температуры, давления, расхода, уровня и пр. Купить ИРТ по цене производителя, в наличии и под заказ ?', 'САУ-МП прибор управления насосами, микропроцессорный контроллер САУ-МП, логический контроллер САУ-МП', 'ru', 1, 'izmeriteli-regulyatory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gauges-regulators/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(118, 61, 'Измерители без выхода', 'izmeriteli-bez-vyhoda', 'category', NULL, 'Измерители 2ТРМ0 ТРМ200 УКТ38 ИДЦ1 ИТП (цифровые индикаторы)', 'Измерители 2ТРМ0, ТРМ200, УКТ38, ИДЦ1, ИТП (цифровые индикаторы)', 'измеритель УКТ38, устройство укт38, укт38', 'ru', 1, 'izmeriteli-bez-vyhoda.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gauges-meters/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(119, 61, 'ПИД-регуляторы', 'pid-regulyatory', 'category', NULL, 'ПИД-регуляторы ТРМ одно- и многоканальные с интерфейсом RS485', 'ПИД-регуляторы ТРМ одно- и многоканальные с интерфейсом RS485', 'ТРМ251', 'ru', 1, 'pid-regulyatory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/pid-regulators/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(120, 61, 'Измерители-Регуляторы', 'izmeriteli-regulyatory', 'category', NULL, 'Измерители-регуляторы ТРМ', 'Измерители-регуляторы ТРМ', 'ТЭРМ регулятор трёхканальный электронный четырёхканальный ТЕРМ купить производитель наличие заказ склад Москва', 'ru', 1, 'izmeriteli-regulyatory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/regulators-vp/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(121, 61, 'Контроллеры для ГВС, отопления и вентиляции', 'kontrollery-dlya-gvs-otopleniya-i-ventilyacii', 'category', NULL, 'Контроллеры ТРМ и КТР для систем ГВС, отопления и вентиляции', 'Контроллеры ТРМ и КТР для систем ГВС, отопления и вентиляции. Купить приборы по цене производителя, в наличии и под заказ со склада в Москве.', 'Каскадные регуляторы котлов КТР-121.02; КТР-121.03, регуляторы КТР-121.02; КТР-121.03; КТР-121.02', 'ru', 1, 'kontrollery-dlya-gvs-otopleniya-i-ventilyacii.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kontrollery-dlya-gvs-otopleniya-i-ventilyatsii/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(122, 61, 'Приборы для управления насосами', 'pribory-dlya-upravleniya-nasosami', 'category', NULL, 'Приборы для управления насосами', 'Приборы для управления насосами. Купить регуляторы по цене производителя, в наличии и под заказ со склада в Москве.', 'САУ-МП прибор управления насосами, микропроцессорный контроллер САУ-МП, логический контроллер САУ-МП', 'ru', 1, 'pribory-dlya-upravleniya-nasosami.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/pribory-dlya-upravleniya-nasosami/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(123, 6, 'Регистраторы', 'registratory', 'category', NULL, 'Регистраторы-самописцы бумажные и безбумажные (электронные-цифровые, видеографические).', 'Регистраторы-самописцы бумажные и безбумажные (электронные-цифровые, видеографические). Купить регистрирующий (самопишущий) прибор по цен', 'МСД-200, мсд200', 'ru', 1, 'registratory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/registratory/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(124, 62, 'Бумажные регистраторы', 'bumajnye-registratory', 'category', NULL, 'Регистратор бумажный (прибор самопишущий регистрирующий): РМТ, Диск-250, самописцы КС', 'Регистраторы бумажные (приборы самопишущие регистрирующие): РМТ-39/49, Диск-250/250М, КС (КСМ КСП КСУ КСД), Альфалог-100, Технограф-160. Купить самопис', 'ФЩЛ-501, ФЩЛ-502, ФШЛ-501, ФШЛ-502', 'ru', 1, 'bumajnye-registratory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/bumazhnye-registratory/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(125, 62, 'Видеографические регистраторы', 'videograficheskie-registratory', 'category', NULL, 'Регистраторы видеографические (безбумажные многоканальные)', 'Регистраторы видеографические (безбумажные многоканальные) технологические - РМТ,   купить по цене производителя.', 'Ключевые слова', 'ru', 1, 'videograficheskie-registratory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/videograficheskie-registratory/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(126, 62, 'Технологические регистраторы', 'tehnologicheskie-registratory', 'category', NULL, 'Регистратор технологический КП', 'Регистратор технологический КП', 'МСД-200, мсд200', 'ru', 1, 'tehnologicheskie-registratory.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/tehnologicheskie-registratory/', 0, NULL, NULL, '', '2019-06-24 10:16:19', NULL),
(127, 62, 'Датчики, расходные материалы, ЗиП', 'datchiki-rashodnye-materialy-zip', 'category', NULL, 'Бумага диаграммная (диски, ленты, узлы пишущие специальные УПС, расходники, ЗиП для регистраторов-самописцев', 'Бумага диаграммная (диски, ленты, бланки), узлы пишущие специальные УПС, датчики, расходники, ЗиП для регистраторов-самописцев КС, Диск, РМТ ?', 'Датчик давления, сенсор давления', 'ru', 1, 'datchiki-rashodnye-materialy-zip.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/supplies-for-registrars/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(128, 6, 'Пневматические приборы и устройства', 'pnevmaticheskie-pribory-i-ustroystva', 'category', NULL, 'Пневматические приборы контроля и регулирующие устройства', 'Пневматические приборы контроля и регулирующие устройства', 'ПП позиционеры пневматические', 'ru', 1, 'pnevmaticheskie-pribory-i-ustroystva.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/pnevmaticheskie-pribory-i-ustrojstva/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(129, 63, 'Приборы контроля пневматические', 'pribory-kontrolya-pnevmaticheskie', 'category', NULL, 'пневматические приборы контроля', 'пневматические приборы контроля', 'ПП позиционеры пневматические', 'ru', 1, 'pribory-kontrolya-pnevmaticheskie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/pribory-kontrolya-pnevmaticheskie/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(130, 63, 'Устройства регулирующие', 'ustroystva-reguliruyushchie', 'category', NULL, 'Устройства регулирующие пневматические', 'Устройства регулирующие пневматические', 'ФР0098 регулятор пневматический пропорциональный (ПР2.8-М1)', 'ru', 1, 'ustroystva-reguliruyushchie.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ustrojstva-reguliruyushhie/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(131, 63, 'Подготовка воздуха', 'podgotovka-vozduha', 'category', NULL, 'Стабилизаторы и редукторы давления с фильтром', 'Стабилизаторы и редукторы давления с фильтром', 'фильтр стабилизатор давления воздуха ФСДВ ФСДВ-6 ФСДВ-10 купить по цене производителя наличии под заказ склада Москва', 'ru', 1, 'podgotovka-vozduha.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/podgotovka-vozduha/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(132, 6, 'Блоки питания и преобразования', 'bloki-pitaniya-i-preobrazovaniya', 'category', NULL, 'Блоки питания и преобразования сигналов', 'Блоки питания и преобразования сигналов', 'Барьер искрозащиты ИСКРА-АТ/ТП/ТС.02', 'ru', 1, 'bloki-pitaniya-i-preobrazovaniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/bloki-pitaniya-preobrazovaniya/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(133, 64, 'Блоки питания', 'bloki-pitaniya', 'category', NULL, 'Блок питания', 'Блок питания', 'Блок питания 2000ПМ-24-4/8, Блок питания 2000ПМ-36-4/8', 'ru', 1, 'bloki-pitaniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/blok-pitaniya/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(134, 64, 'Блоки преобразования сигналов и барьеры искрозащиты', 'bloki-preobrazovaniya-signalov-i-barery-iskrozashchity', 'category', NULL, 'Блоки преобразования сигналов и барьеры искрозащиты', 'Блоки преобразования сигналов и барьеры искрозащиты', 'Барьер искрозащиты ИСКРА-АТ/ТП/ТС.02', 'ru', 1, 'bloki-preobrazovaniya-signalov-i-barery-iskrozashchity.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/blok-preobrazovaniya-signala-barer-iskrozashchity/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(135, 6, 'Котельное оборудование и автоматика', 'kotelnoe-oborudovanie-i-avtomatika', 'category', NULL, 'Котельное оборудование и автоматика', 'Котельное оборудование, приборы и автоматика безопасности, купить по цене производителя из наличия и под заказ со склада в Москве.', 'Шкафы контроля горелок ШКГ-1; -2; -3; -4; -6; -8, ШКГ-1Х/24, ШКГ-1Х/220, ШКГ-8Х/24', 'ru', 1, 'kotelnoe-oborudovanie-i-avtomatika.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kotelnoe-oborudovanie-i-avtomatika/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(136, 65, 'Устройства для автоматизации тепловых систем', 'ustroystva-dlya-avtomatizacii-teplovyh-sistem', 'category', NULL, 'Устройства для автоматизации тепловых систем', 'Устройства и приборы для автоматизации систем теплоснабжения, отопления, горячего водоснабжения, вентиляции.', 'ИЗОДРОМ-1, ИЗОДРОМ-2', 'ru', 1, 'ustroystva-dlya-avtomatizacii-teplovyh-sistem.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ustrojstva-dlya-avtomatizatsii-teplovyh-sistem/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(137, 65, 'Устройства розжига и приборы контроля пламени', 'ustroystva-rozjiga-i-pribory-kontrolya-plameni', 'category', NULL, 'Устройства розжига и приборы контроля пламени', 'Устройства управления розжигом и приборы контроля наличия пламени, купить по цене производителя из наличия и под заказ со склада в Москве.', 'ПНП, ПНП-М, ПНПМ', 'ru', 1, 'ustroystva-rozjiga-i-pribory-kontrolya-plameni.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/ustrojstva-rozzhiga-i-pribory-kontrolya-plameni/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(138, 65, 'Пускорегулирующие устройства', 'puskoreguliruyushchie-ustroystva', 'category', NULL, 'Пускорегулирующие устройства ЭИМ', 'Пускорегулирующие устройства ЭИМ', 'регулятор электронных положений РЭП-1 РЭП1 РЕП1 купить производитель наличие заказ склад Москва', 'ru', 1, 'puskoreguliruyushchie-ustroystva.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/puskoreguliruyushhie-ustrojstva/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(139, 65, 'Горелки газовые и жидкотопливные', 'gorelki-gazovye-i-jidkotoplivnye', 'category', NULL, 'Горелки газовые и жидкотопливные, форсунки', 'Горелки газовые, жидкотопливные, комбинированные, форсунки. Купить по цене производителя (см. прайс-лист) в наличии и под заказ со склада пр?', 'Горелки для подогрева битума БИТ-АВТО', 'ru', 1, 'gorelki-gazovye-i-jidkotoplivnye.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gorelki-gazovye-i-zhidkotoplivnye/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(140, 65, 'Шкафы, щиты и комплекты автоматики', 'shkafy-shchity-i-komplekty-avtomatiki', 'category', NULL, 'Шкафы, щиты и комплекты автоматики', 'Шкафы и щиты управления, комплекты автоматики', 'Шкафы контроля горелок ШКГ-1; -2; -3; -4; -6; -8, ШКГ-1Х/24, ШКГ-1Х/220, ШКГ-8Х/24', 'ru', 1, 'shkafy-shchity-i-komplekty-avtomatiki.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/shkaf-shchit-komplekt-avtomatiki/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL);
INSERT INTO `basis` (`id`, `id_parent`, `name`, `name_translit`, `type`, `content`, `title`, `description`, `keywords`, `lang`, `order_num`, `img`, `target`, `popular`, `mime_type`, `user_id`, `ip`, `created`, `closed`) VALUES
(141, 0, 'Аналитика', 'analitika', 'category', NULL, 'Аналитические приборы и оборудование', 'Аналитические приборы и оборудование', 'УКК узел контроля коррозии', 'ru', 1, 'analitika.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/analytics/', 0, NULL, 0, '127.0.0.1', '2019-06-24 10:16:10', NULL),
(142, 7, 'Калибраторы-измерители сигналов', 'kalibratory-izmeriteli-signalov', 'category', NULL, 'Калибраторы-измерители стандартных сигналов КИСС, ИКСУ', 'Калибраторы-измерители стандартных сигналов КИСС, ИКСУ', 'ИКСУ-2000 калибраторы-измерители унифицированных сигналов эталонные одноканальные цена производителя в наличии сроки поставки аналог', 'ru', 1, 'kalibratory-izmeriteli-signalov.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/calibrator-meter-signals/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(143, 7, 'Таймеры и счётчики импульсов', 'taymery-i-schetchiki-impulsov', 'category', NULL, 'Таймеры и счётчики импульсов', 'Таймеры и счётчики импульсов', 'ЭТ-99, таймер электронный ЭТ-99', 'ru', 1, 'taymery-i-schetchiki-impulsov.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/tajmer-schyotchik-impulsov/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(144, 72, 'Таймеры', 'taymery', 'category', NULL, 'Таймеры', 'Таймеры', 'ЭТ-99, таймер электронный ЭТ-99', 'ru', 1, 'taymery.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/tajmer/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(145, 72, 'Счетчики импульсов', 'schetchiki-impulsov', 'category', NULL, 'Счетчики импульсов СИ', 'Счетчики импульсов СИ', 'Счетчики  СИ-206-Д1, СИ-206-Д2, СИ-206-М1', 'ru', 1, 'schetchiki-impulsov.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/schetchik-impulsov-si/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(146, 7, 'Состав и свойства жидкостей и твердых тел', 'sostav-i-svoystva-jidkostey-i-tverdyh-tel', 'category', NULL, 'Состав и свойства жидкостей и твердых тел', 'Состав и свойства жидкостей и твердых тел', 'УКК узел контроля коррозии', 'ru', 1, 'sostav-i-svoystva-jidkostey-i-tverdyh-tel.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/sostav-i-svojstva-zhidkostej-i-tverdyh-tel/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(147, 73, 'Контроль плотности', 'kontrol-plotnosti', 'category', NULL, 'Измерение и контроль плотности', 'Измерение и контроль плотности', 'Плотномер Плот-3М', 'ru', 1, 'kontrol-plotnosti.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/izmerenie-i-kontrol-plotnosti/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(148, 73, 'Мониторинг коррозии и солеотложения', 'monitoring-korrozii-i-soleotlojeniya', 'category', NULL, 'Оборудование для коррозионного мониторинга «Коррсенс+» и контроля солеотложения', 'Оборудование для коррозионного мониторинга «Коррсенс+» и контроля солеотложения', 'УКК узел контроля коррозии', 'ru', 1, 'monitoring-korrozii-i-soleotlojeniya.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/monitoring-of-corrosion-and-scale-inhibitors/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(149, 73, 'Концентрация, проводимость, pH', 'koncentraciya-provodimost-pH', 'category', NULL, 'Концентрация, проводимость, pH-метр', 'Концентрация, проводимость, pH-метр', 'АЖК-3130 кондуктометр-трансмиттер', 'ru', 1, 'koncentraciya-provodimost-pH.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/kontsentratsiya-provodimost-ph/', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL),
(150, 7, 'Газовый анализ', 'gazovyy-analiz', 'category', NULL, 'Газовый анализ: газоанализаторы, газосигнализаторы', 'приборы, системы и оборудование для анализа газов и газовых смесей: газоанализаторы, газосигнализаторы.', 'течеискатель Тesto-316-4, детектор testo 316-4, testo-316, Тesto-316-4', 'ru', 1, 'gazovyy-analiz.png', 'http://xn--90ahjlpcccjdm.xn--p1ai/produkcija/gazovyj-analiz-gazoanalizator-gazosignalizator/', 0, NULL, NULL, '', '2019-06-24 10:16:12', NULL),
(151, 1, 'еу1', 'eu1', 'category', NULL, 'цуа4', 'ауц5', 'ацу6', 'ru', 1, 'eu1.png', '', 0, NULL, NULL, '', '2019-06-24 10:16:20', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `basis_meta`
--

CREATE TABLE IF NOT EXISTS `basis_meta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `basis_id` bigint(20) unsigned NOT NULL,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `autoload` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `basis_id_name` (`basis_id`,`name`(255)),
  KEY `basis_id` (`basis_id`),
  KEY `basis_id_autoload` (`basis_id`,`autoload`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lang` (`lang`,`name`(32))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `autoload` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`name`(25))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `name`, `value`, `autoload`) VALUES
(1, 'site_url', 'nord-e.ru', 1),
(3, 'site_name', 'NORD-E', 1),
(6, 'email_admin', 'admin@', 1),
(10, 'smtp_host', 'smtp.jino.ru', 1),
(11, 'smtp_login', 'robot@', 1),
(12, 'smtp_password', 'da3eptel8', 1),
(13, 'smtp_port', '587', 1),
(14, 'notify_email_charset', 'utf-8', 1),
(15, 'notify_name_from', '', NULL),
(16, 'notify_email_from', 'robot@', NULL),
(17, 'notify_email_subject', 'оповещение', NULL),
(18, 'feedback_send_to_email', 'admin@', NULL),
(20, 'articles_per_page', '20', 1),
(21, 'comments_per_page', '200', 1),
(22, 'comments_order', '200', 1),
(23, 'mysql_date_article_nice', '%d&nbsp;%M %Y', 1),
(24, 'mysql_date_comment_nice', '%H:%i %d&nbsp;%M %Y', 1),
(26, 'comment_moderation', '1', 1),
(27, 'moderation_notify', '1', 1),
(28, 'comment_registration', '1', 1),
(29, 'articles_per_page_main', '12', 1),
(30, 'yandex_metrika_id', '50683198', 1),
(40, 'file_upload_max_size', '52428800', 1),
(41, 'banner_upload_allowed', 'image/jpeg, image/png, image/gif', 1),
(42, 'product_upload_allowed', 'image/jpeg, image/png, image/gif, application/pdf, application/msword, application/zip, application/mspowerpoint, application/vnd.ms-powerpoint', 1),
(43, 'poll_upload_allowed', 'image/jpeg, image/png, image/gif, application/pdf, application/msword, application/zip, application/mspowerpoint, application/vnd.ms-powerpoint', 1),
(58, 'img_thumbnail_size_w', '150', 1),
(59, 'img_thumbnail_size_h', '150', 1),
(61, 'img_medium_size_w', '300', 1),
(62, 'img_medium_size_h', '300', 1),
(63, 'img_big_size_w', '1920', 1),
(64, 'img_big_size_h', '1080', 1),
(70, 'cache_web', '0', 1),
(71, 'cache_web_compress', '0', 1),
(100, 'widgets', 'a:0:{}', 1),
(104, 'lang', 'a:2:{s:7:"default";s:2:"ru";s:4:"list";a:3:{s:2:"ru";a:9:{i:0;s:2:"ru";i:1;s:2:"be";i:2;s:2:"uk";i:3;s:2:"ky";i:4;s:2:"ab";i:5;s:2:"mo";i:6;s:2:"et";i:7;s:2:"lv";i:8;s:2:"az";}s:2:"en";s:2:"en";s:2:"de";s:2:"de";}}', 1),
(105, 'translate_yandex_api_key', 'trnsl.1.1.20190608T100842Z.3e48c847758375b0.c019200f8a9cef70792c7c8b12c1337ff0a60097', 1),
(106, 'translate_yandex_api_url', 'https://translate.yandex.net/api/v1.5/tr.json/translate', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `token` char(32) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expired` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name_translit` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group_id` tinyint(4) NOT NULL DEFAULT '1',
  `password` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `email` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` tinytext,
  `card_number` char(19) DEFAULT NULL,
  `card_holder` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `qiwi` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `actkey` char(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user_action`
--

CREATE TABLE IF NOT EXISTS `user_action` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `user_action`
--

INSERT INTO `user_action` (`id`, `name`) VALUES
(1, 'Юзер. Регистрация'),
(2, 'Юзер. Активация'),
(3, 'Юзер. Вход'),
(4, 'Юзер. Выход'),
(5, 'Юзер. Регистрация ulogin'),
(6, 'Юзер. Вход ulogin'),
(7, 'Юзер. Смена данных'),
(8, 'Юзер. Смена пароля'),
(9, 'Юзер. Удаление'),
(10, 'Базис. Создал'),
(11, 'Базис. Изменил'),
(12, 'Базис. Удалил'),
(13, 'Мета. Создал'),
(19, 'Рейтинг. Поставил'),
(23, 'Рейтинг. Удалил');

-- --------------------------------------------------------

--
-- Структура таблицы `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `user_group`
--

INSERT INTO `user_group` (`id`, `name`) VALUES
(1, 'Гости'),
(2, 'Зарегистрированные'),
(8, 'Модераторы'),
(9, 'Администраторы');

-- --------------------------------------------------------

--
-- Структура таблицы `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `action_id` tinyint(3) unsigned NOT NULL,
  `target_id` int(10) unsigned DEFAULT NULL,
  `comment` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `ip` tinytext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `user_log`
--

INSERT INTO `user_log` (`id`, `user_id`, `action_id`, `target_id`, `comment`, `ip`, `time`) VALUES
(1, 0, 11, 44, '', '127.0.0.1', '2019-06-24 13:21:14'),
(2, 0, 11, 44, '', '127.0.0.1', '2019-06-24 13:22:10'),
(3, 0, 11, 44, '', '127.0.0.1', '2019-06-24 13:22:17'),
(4, 0, 11, 141, '', '127.0.0.1', '2019-06-24 13:25:25'),
(5, 0, 11, 116, '', '127.0.0.1', '2019-06-24 13:29:03'),
(6, 0, 11, 96, '', '127.0.0.1', '2019-06-24 13:29:47'),
(7, 0, 11, 66, '', '127.0.0.1', '2019-06-24 13:30:12'),
(8, 0, 11, 1, '', '127.0.0.1', '2019-06-24 13:31:36'),
(9, 0, 11, 44, '', '127.0.0.1', '2019-06-24 13:40:06'),
(10, 0, 11, 116, '', '127.0.0.1', '2019-06-24 13:40:17'),
(11, 0, 11, 141, '', '127.0.0.1', '2019-06-24 13:40:32'),
(12, 0, 11, 96, '', '127.0.0.1', '2019-06-24 13:40:33'),
(13, 0, 11, 66, '', '127.0.0.1', '2019-06-24 13:41:04'),
(14, 0, 11, 21, '', '127.0.0.1', '2019-06-24 15:07:19'),
(15, 0, 11, 21, '', '127.0.0.1', '2019-06-24 15:07:20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
