$(document).ready(function(){

	$('.glide').click(function(){
		var el = $(this).attr('href');
		var position=$(el).offset().top
		console.log(el)
		console.log(position)
		$('body,html').animate({ scrollTop: position }, 300);
		return false
	});
	
});


function language_change(e, lang){
	e.preventDefault()
	setCookie('lang', lang, 8760)
	//document.location.reload(true)
	window.location.href = '/';
}

function box_toggle(hide, show, disable, active){
	$(hide).hide()
	$(show).fadeIn()
	$(disable).removeClass('menu_tab_active')
	$(active).addClass('menu_tab_active')
}


function feedback_submit(e, form_id, target){
	e.preventDefault

	var data = {
		content: '',
		target: target
	};
	var error=false

	$(form_id).find ('.feedback_data').each(function() {
		switch(this.name){
			case 'agree':
				if(!this.checked) error=true
				return
			break
			case 'комментарий':
			case 'Имя':
				if($(this).val()=='') {
					error=true
					return
				}
			break
			default:
		}
		data["content"] += this.name+': '+$(this).val()+' | ';
	});
	$(form_id).find ('.feedback_data').each(function() {
		$(this).val('')
	});
	console.log(data)

	if(error) return
	else data["type"]='comment'

	answer = basis_create(data);

	modal_close('top')

	if(answer["id"]) alert('OK!')
	return true;
}


function basis_create(params){
	wait_start()
	var answer = jsonrpc_request("basis.create", params)
	wait_finish()

	if(typeof(answer["error"])=="undefined"){
		return answer["result"]
	}else{
		return answer["error"]
	}
}
function basis_read(params){
	wait_start()
	var answer = jsonrpc_request("basis.read", params)
	wait_finish()

	if(typeof(answer["error"])=="undefined"){
		return answer["result"]
	}else{
		return answer["error"]
	}
}
function basis_update(params){

	wait_start()
	var answer = jsonrpc_request("basis.update", params)
	wait_finish()

	if(typeof(answer["error"])=="undefined"){
		return answer["result"]
	}else{
		return answer["error"]
	}

}
function basis_delete(basis_id, basis_wipe){

	wait_start()
	var answer = jsonrpc_request("basis.delete", {id: basis_id, wipe: basis_wipe})
	wait_finish()

	if(typeof(answer["error"])=="undefined"){
		return answer["result"]
	}else{
		return answer["error"]
	}

}

function wait_start(){$('#wait_notify').fadeIn();}
function wait_finish(result, desc){result = result === undefined ? 1 : result;desc = desc === undefined ? '' : desc;$('#wait_notify').fadeOut();}
function modal_clear(position){$('#'+position+'_modal_header').empty();$('#'+position+'_modal_body').empty();$('#'+position+'_modal_footer').empty();}
function modal_show(position, width){width = width === undefined ? '60rem' : width;$('.modal_box').css('max-width', width);$('#'+position+'_modal').modal();}
function modal_close(position){$('#'+position+'_modal').modal('hide');}
function setCookie(n,v,h) {var d = new Date();d.setTime(d.getTime() + (h*60*60*1000));var expires="expires="+ d.toUTCString();document.cookie=n+"="+v+";"+expires+";path=/";}
function getCookie(n) {var n=n+"=";var decodedCookie=decodeURIComponent(document.cookie);var ca=decodedCookie.split(';');for(var i=0;i<ca.length;i++) {var c=ca[i];while (c.charAt(0)==' '){c=c.substring(1);}if (c.indexOf(n)==0) {return c.substring(n.length,c.length);}}return "";}