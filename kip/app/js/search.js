var suggest_show_quantity=7;
//var search_quantity=7;
var suggest_count=0;
var suggest_selected=0;
//var basis_ids=new Array();
var search_query='';
var search_params={};

$(window).on('load', function(){

//TOP MAIN SEARCH
	$('.advice_variant').on('click',function(){// обработка клика по подсказке
		$('#search_box').val($(this).text());
		$('#search_list').fadeOut(350).html('');
	});

	$('html').click(function(){
		$('#search_list').hide();
	});

	$('#search_box').click(function(event){// если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
		//alert(suggest_count);
		if(suggest_count) $('#search_list').show();
		event.stopPropagation();
	});



});

function search_top_main(keyCode){
	//alert(keyCode);return;
		switch(keyCode) {
			case 27:  // escape
				$('#search_list').hide();
				$('#search_box').val('');
				document.getElementById('search_list').innerHTML='';
				document.getElementById('search_count_box').innerHTML='';
				$('#search_box').css({'color': '#777', 'background': 'rgba(255, 255, 255, 0.9)'});
				return false;
				break;
/*
			case 38:  // стрелка вверх
			case 40:  // стрелка вниз
				//I.preventDefault();
				if(suggest_count){
					search_key_activate( keyCode-39 );//выделение пунктов в слое, переход по стрелкам
				}
				break;
			case 13:  // enter
				if(suggest_count>0){
					search_draw($('#search_box').val());
				}
				break;
*/
			default:
				
				if($('#search_box').val().length>2){
					suggest_selected=0;

					search_query=$('#search_box').val();
					search_params={
						search_str: search_query,
						type: 'product',
						response: {"limit":5}
					}

					jsonrpc_request("basis.read", search_params, 1, true, function(answer){
						suggest_count = answer["result"]["total_db"];
						$('#search_count_box').text(suggest_count);
						$('#search_count_box').fadeIn()
						document.getElementById('search_list').innerHTML='';
						if(suggest_count > 0){
							$('#search_box').css({'color': '#111', 'background': 'rgba(123, 255, 123, 0.2)'});
							var suggest_basis=answer["result"]["list"];
							$('#search_list').show();
							$.each(suggest_basis, function(i, data){
								if(this["img"]!='') var img_src='/files/image/'+this["img"];
								else var img_src='/images/no-img.png';
								$('#search_list').append('<tr class="advice_variant small text-left" ><td class="search_image"><a href="/'+this["name_translit_parent"]+'/'+this["name_translit"]+'"><img src="'+img_src+'" width="60" /></a></td><td class="search_name"><a href="/'+this["name_translit_parent"]+'/'+this["name_translit"]+'">'+this["name"]+'</a></td></tr>');
								if(i>=(suggest_show_quantity-1)) return false;
							});
						}else{
							$('#search_box').css({'color': '#444', 'background': 'rgba(255, 123, 123, 0.2)'});
						}
					});
				}else{
					document.getElementById('search_list').innerHTML='';
					document.getElementById('search_count_box').innerHTML='';
					$('#search_box').css({'color': '#777', 'background': 'rgba(255, 255, 255, 0.9)'});
				}
			break;
		}
}

function search_key_activate(n){
	$('#search_list tr').eq(suggest_selected-1).removeClass('active');

	if(n == 1 && suggest_selected < suggest_count){
		suggest_selected++;
	}else if(n == -1 && suggest_selected > 0){
		suggest_selected--;
	}

	if(suggest_selected>suggest_show_quantity) suggest_selected=suggest_show_quantity;

	if( suggest_selected > 0){
		$('#search_list tr').eq(suggest_selected-1).addClass('active');
		$("#search_box").val( $('#search_list .search_name').eq(suggest_selected-1).text() );
	} else {
		$("#search_box").val(search_query);
	}
}
