<?php if (!defined("INBOX")) die('separate call');?>
<!DOCTYPE html>
<html lang="<?=Lang::current_lang()?>">
<head>
<meta charset="utf-8">
<meta name="author" content="umdi.ru">

<?php //Open Graph
isset($var["page"]["title"])? '<meta property="og:title" content="'.Lang::str($var["page"]["title"]).'" />' : '';
isset($var["page"]["description"])? '<meta property="og:description" content="'.Lang::str($var["page"]["description"]).'" />' : '';
(isset($var["page"]["image"]) AND $var["page"]["image"]!='')? '<meta property="og:image" content="'.$var["page"]["img"].'" />' : '<meta property="og:image" content="/app/img/logo_big.png" />';
isset($var["page"]["type"])? '<meta property="og:type" content="'.$var["page"]["type"].'" />' : '';
isset($var["page"]["url"])? '<meta property="og:url" content="'.$var["page"]["url"].'" />' : '';
?>

<!--favicon-->
<link href="/app/img/favicon/apple-touch-icon.png" rel="apple-touch-icon" sizes="180x180">
<link href="/app/img/favicon/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png">
<link href="/app/img/favicon/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png">
<link href="/app/img/favicon/site.webmanifest" rel="manifest">
<link href="/app/img/favicon/safari-pinned-tab.svg" rel="mask-icon" color="#2b2b2b">
<link href="/app/img/favicon/favicon.ico" rel="shortcut icon">
<meta name="msapplication-TileColor" content="#2b2b2b">
<meta name="msapplication-config" content="/app/img/favicon/browserconfig.xml">
<meta name="theme-color" content="#2b2b2b">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="<?=Lang::str($var["page"]["keywords"])?>" />
<meta name="description" content="<?=Lang::str($var["page"]["description"])?>" />
<title><?=Lang::str($var["page"]["title"])?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="/app/plugin/jquery-3.4.1.min.js" type="text/javascript"></script>

<link href="/app/plugin/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">
<script src="/app/plugin/bootstrap/bootstrap.min.js" type="text/javascript"></script>

<link href="/app/plugin/bxslider/jquery.bxslider.min.css" rel="stylesheet" type="text/css">
<script src="/app/plugin/bxslider/jquery.bxslider.min.js" type="text/javascript"></script>

<link href="/app/plugin/fotorama/fotorama.css" rel="stylesheet" type="text/css">
<script src="/app/plugin/fotorama/fotorama.js" type="text/javascript"></script>

<script src="/app/plugin/jsonrpc.js" type="text/javascript"></script>

<link href="/app/css/style.css" rel="stylesheet" type="text/css">
<link href="/app/css/responsive.css" rel="stylesheet" type="text/css">
<script src="/app/js/core.js" type="text/javascript"></script>
<script src="/app/js/search.js" type="text/javascript"></script>
</head>
<body>
	<!--MODAL-->
	<div class="modal fade" id="top_modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal_box" role="document">
			<div class="modal-content" id="top_modal_box">
				<button class="btn btn-sm btn-secondary m-1 py-0" style="position:absolute;right:0;" data-dismiss="modal" aria-label="Close">x</button>
				<div class="modal-header pr-5" id="top_modal_header"></div>
				<div class="modal-body" id="top_modal_body"></div>
				<div class="modal-footer" id="top_modal_footer"></div>
			</div>
		</div>
	</div>
	<!--wait_notify-->
	<div id="wait_notify" style="display:none;position:fixed;top:90%;left:90%;z-index:9999;opacity:.5;"><i class="fas fa-cog fa-5x fa-spin"></i></div>