<div class="row justify-content-around no-gutters">
	<div class="col-sm-10">
<?php include 'app/view/block_breadcrumb.php';?>
	</div>
	<div class="col-sm-2 text-center">
		<a id="share_tab" href="#">
			<img src="/app/img/ico_share.png" alt="" height="15" width="15" />
			<?=Lang::str('Поделиться')?>
		</a>
	</div>
	<div class="col-12 text-center" id="share_box">
		<div class="addthis_inline_share_toolbox"></div>
	</div>
</div>

<script type="text/javascript">
	$('#share_tab').click(function(){
		$('#share_box').toggle('display');
		$(this).toggleClass('share_tab_active', 200);
		return false;
	});
</script>
