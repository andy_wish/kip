<div class="navbar_top_lang">
<?php
$lang=[
	"current"=>Lang::current_lang(),
	"list"=>Lang::listing()
];
foreach($lang["list"] as $l=>$vars){
	if($l==$lang["current"]){
?>
		<span class="navbar_top_lang_letters_active">&emsp;<?=$l?></span>
<?php
	}else{
?>
	<a href="#" class="navbar_top_lang_listing" onClick="language_change(event, '<?=$l?>')">
		<span class="navbar_top_lang_letters">&emsp;<?=$l?></span>
	</a>
<?php
	}
}
?>
</div>