<script type="text/javascript">
function feedback_draw(e){
	e.preventDefault()

	modal_clear('top');

	$('#top_modal_body').append(
		$('<form>').append(
			$('<div class="row no-gutters">').append(
				$('<div class="col-md-5 col-xl-4">').append(
					$('<input type="text" class="form-control navbar_top_feedback_input" id="feedback_name_product" placeholder="<?=Lang::str('Ваше имя')?>"><br />'),
					$('<input type="email" class="form-control navbar_top_feedback_input" id="feedback_email_product" placeholder="<?=Lang::str('Ваш адрес электронной почты')?>"><br />'),
					$('<input type="text" class="form-control navbar_top_feedback_input" id="feedback_phone_product" placeholder="<?=Lang::str('Ваш номер телефона')?>"><br />'),
					$('<button type="submit" class="navbar_top_feedback_btn_submit" onClick="feedback_submit(\'product\', <?=$var["product"]["id"]?>)"><?=Lang::str('ОТПРАВИТЬ')?></button>'),
				),
				$('<div class="col-md-7 col-xl-7 pl-md-4 ml-md-2 mb-3">').append(
					$('<textarea class="form-control navbar_top_feedback_textarea" id="feedback_text_product" rows="6" placeholder="<?=Lang::str('Ваш вопрос или комментарий')?>"></textarea>'),
					$('<label class="checkbox_container">').append(
						$('<?=Lang::str('Согласен на обработку персональных данных')?>'),
						$('<input type="checkbox" checked="">'),
						$('<span class="checkbox_checkmark"></span>'),
					),
					$('<div id="feedback_checkbox_desc">').append(
						$('<span><?=Lang::str('Нажимая кнопку "Отправить", я даю согласие на обработку всех моих персональных данных, указанных в форме обращения, а также получения информации в соответствии с Федеральным законом "О персональных данных" от 27.07.2006 N 152-ФЗ.')?></span>'),
					),
				),
			),
		),
	);
	modal_show('top', '50rem');
	wait_finish();
}
</script>