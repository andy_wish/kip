<h1 class="page_title"><?=$var["page"]["h1"]?></h1>

<?php
if (isset($var["breadcrumb"])){
	include 'app/view/block_breadcrumb.php';
}
?>



<?php
if ((isset($var["products"]) AND $var["products"]["total"]>0)){
?>



<div class="row no-gutters">
<?php
foreach($var["products"]["list"] as $row){
?>

<article itemscope itemtype="http://schema.org/Product" class="justify-content-center product_preview_block">
<?php
		if($row["img"]!='') $img_src='/files/image/'.$row["img"];
		else $img_src='/app/img/no-img.png';
		//<img itemprop="thumbnailUrl"
?>
		<a href="/<?=$var["category"]["one"]["list"][0]["name_translit"]?>/<?=$row["name_translit"]?>"><img itemprop="image" src="<?=$img_src?>" alt="<?=$row["name"]?>" width="300" height="200" /></a>

		<header class="product_preview_header">
			<a href="/<?=$var["category"]["one"]["list"][0]["name_translit"]?>/<?=$row["name_translit"]?>">
				<h4 itemprop="headline name" class="product_preview_name"><?=$row["name"]?></h4>
			</a>
		</header>

		<link itemprop="url" content="<?=$_SERVER['SERVER_NAME']?>/<?=$var["category"]["one"]["list"][0]["name_translit"]?>/<?=$row["name_translit"]?>" href="<?=$_SERVER['SERVER_NAME']?>/<?=$var["category"]["one"]["list"][0]["name_translit"]?>/<?=$row["name_translit"]?>" property="alternate" />

		<a class="product_preview_link" href="/<?=$var["category"]["one"]["list"][0]["name_translit"]?>/<?=$row["name_translit"]?>"><?=Lang::str('Подробнее')?></a>

</article>
<?php
	}
?>

</div>
<?php



}else{
?>
	<br />
	<h6 class="alert alert-danger mt-4"><?=Lang::str('Не найдено')?></h6>
	<a href="/"><?=Lang::str('На главную')?></a>
<?php
}
?>