<div class="row">
	<div class="error_box col-auto mx-auto">
		<h1 class="mb-4 text-center"><?=$var["page"]["h1"]?></h1>

<?php
if(file_exists(ROOT_DIR.'/app/view/block_error_'.$var["error_num"].'.php')) include ROOT_DIR.'/app/view/block_error_'.$var["error_num"].'.php';

if(DEBUG AND isset($var["error_desc"])){
?>
		<p><?=$var["error_desc"]?></p>
<?php
}
?>

		<p>Можно вернуться <a href="/">на главную</a></p>
	</div>
</div>