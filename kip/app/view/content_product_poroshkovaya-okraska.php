<?php // echo '<pre>';var_dump($var["product"]["infoboxes"]);?>
<link rel="stylesheet" type="text/css" href="/app/css/palette.css">

<article itemscope itemtype="http://schema.org/Product">

	<header class="product_header">
		<link itemprop="url" content="http://<?=$_SERVER['SERVER_NAME']?>/<?=$var["page"]["url"]?>" href="http://<?=$_SERVER['SERVER_NAME']?>/<?=$var["page"]["url"]?>" property="alternate" />
		<meta itemprop="alternativeHeadline" content="<?=$var["page"]["title"]?>" />
		<meta itemprop="image" content="<?=$var["page"]["img"]?>" />
<?php
	if($var["page"]["keywords"]!=''){
?>
		<meta itemprop="keywords" content="<?=$var["page"]["keywords"]?>"/>
<?php
	}
	if($var["page"]["description"]!=''){
?>
		<meta itemprop="description" content="<?=$var["page"]["description"]?>"/>
<?php
	}
?>
		<h1 class="page_title" itemprop="headline name"><?=$var["page"]["h1"]?></h1>
	</header>
<?php include 'app/view/block_breadcrumb_share.php';?>

	<!--<img itemprop="image" src="/product/<?=$var["page"]["img"]?>" alt="<?=$var["product"]["name"]?>" />-->

	<div class="row menu_tabs justify-content-around no-gutters">
<?php
for($i=0;$i<=3;$i++){
?>
		<a class="menu_tab col text-center glide" href="#infobox_<?=$var["product"]["infoboxes"]["$i"]["id"]?>"><?=$var["product"]["infoboxes"]["$i"]["name"]?></a>
<?php
}
if($var["product"]["files"]["total"]>0){
?>
		<a class="menu_tab col text-center menu_tab_last glide" href="#infobox_media"><?=Lang::str('Фото')?>/<?=Lang::str('видео')?></a>
<?php
}
?>
	</div>

<?php
for($i=0;$i<=1;$i++){
?>
	<div id="infobox_<?=$var["product"]["infoboxes"]["$i"]["id"]?>" class="container-fluid">
		<div class="wrapper_product">
			<?=$var["product"]["infoboxes"]["$i"]["content"]?>
		</div>
	</div>
<?php
}
?>

<!--Прайс-лист-->
	<div class="container-fluid" id="infobox_<?=$var["product"]["infoboxes"]["2"]["id"]?>">
		<div class="wrapper_product no_border_bottom">
			<?=$var["product"]["infoboxes"][2]["content"]?>
		</div>
	</div>

	<div class="row menu_tabs justify-content-around no-gutters">
		<a id="menu_tab_<?=$var["product"]["infoboxes"]["4"]["id"]?>" class="menu_tab menu_tab_active col text-center menu_tabs_toggle" onCLick="box_toggle('.boxes_toggle', '#infobox_<?=$var["product"]["infoboxes"]["4"]["id"]?>', '.menu_tabs_toggle', '#menu_tab_<?=$var["product"]["infoboxes"]["4"]["id"]?>')"><?=$var["product"]["infoboxes"]["4"]["name"]?></a>

		<a id="menu_tab_<?=$var["product"]["infoboxes"]["5"]["id"]?>" class="menu_tab col text-center menu_tabs_toggle" onCLick="box_toggle('.boxes_toggle', '#infobox_<?=$var["product"]["infoboxes"]["5"]["id"]?>', '.menu_tabs_toggle', '#menu_tab_<?=$var["product"]["infoboxes"]["5"]["id"]?>')"><?=$var["product"]["infoboxes"]["5"]["name"]?></a>

		<a id="menu_tab_<?=$var["product"]["infoboxes"]["6"]["id"]?>" class="menu_tab col text-center menu_tabs_toggle menu_tab_last" onCLick="box_toggle('.boxes_toggle', '#infobox_<?=$var["product"]["infoboxes"]["6"]["id"]?>', '.menu_tabs_toggle', '#menu_tab_<?=$var["product"]["infoboxes"]["6"]["id"]?>')"><?=$var["product"]["infoboxes"]["6"]["name"]?></a>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="boxes_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"]["4"]["id"]?>">
				<div class="wrapper_product">
					<?=$var["product"]["infoboxes"][4]["content"]?>
				</div>
			</div>
			<div class="boxes_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"]["5"]["id"]?>" style="display:none">
				<div class="wrapper_product">
					<?=$var["product"]["infoboxes"][5]["content"]?>
				</div>
			</div>
			<div class="boxes_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"]["6"]["id"]?>" style="display:none">
				<div class="wrapper_product">
					<?=$var["product"]["infoboxes"][6]["content"]?>
					
				</div>
			</div>
		</div>

	</div>


	<!--Палитра цветов-->
	<div class="container-fluid" id="infobox_<?=$var["product"]["infoboxes"]["3"]["id"]?>">

		<div class="wrapper_product" id="colors">
			<?=$var["product"]["infoboxes"][3]["content"]?>


			<div class="palitra_tabs">
				<ul class="nav nav-tabs flex_ul">
					<li><a href="#zheltyj" data-toggle="tab" aria-expanded="true"><?=Lang::str('Желтые</br>тона')?></a></li>
					<li><a href="#oranzhevyj" data-toggle="tab"><?=Lang::str('Оранжевые</br>тона')?></a></li>
					<li><a href="#krasnyj" data-toggle="tab"><?=Lang::str('Красные</br>тона')?></a></li>
					<li><a href="#fioletovyj" data-toggle="tab"><?=Lang::str('Фиолетовые</br>тона')?></a></li>
					<li><a href="#sinij" data-toggle="tab"><?=Lang::str('Синие</br>тона')?></a></li>
					<li><a href="#zelenyj" data-toggle="tab"><?=Lang::str('Зеленые</br>тона')?></a></li>
					<li><a href="#seryj" data-toggle="tab"><?=Lang::str('Серые</br>тона')?></a></li>
					<li><a href="#korichnevyj" data-toggle="tab"><?=Lang::str('Коричневые</br>тона')?></a></li>
					<li><a href="#chernyjbelyj" data-toggle="tab"><?=Lang::str('Черные и</br>белые тона')?></a></li>
				</ul>
			</div>

			<div class="tab-content">
				<div class="tab-pane active" id="zheltyj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1000">RAL1000<br /><?=Lang::str('Зелёно-Бежевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1001">RAL1001<br /><?=Lang::str('Бежевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1002">RAL1002<br /><?=Lang::str('Песочно-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1003">RAL1003<br /><?=Lang::str('Сигнальный жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1004">RAL1004<br /><?=Lang::str('Жёлто-золотой')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1005">RAL1005<br /><?=Lang::str('Медово-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1006">RAL1006<br /><?=Lang::str('Кукурузно-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1007">RAL1007<br /><?=Lang::str('Нарциссово-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1011">RAL1011<br /><?=Lang::str('Коричнево-бежевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1012">RAL1012<br /><?=Lang::str('Лимонно-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1013">RAL1013<br /><?=Lang::str('Жемчужно-белый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1014">RAL1014<br /><?=Lang::str('Слоновая кость')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1015">RAL1015<br /><?=Lang::str('Светлая слоновая кость')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1016">RAL1016<br /><?=Lang::str('Жёлтая сера')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1017">RAL1017<br /><?=Lang::str('Шафраново-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1018">RAL1018<br /><?=Lang::str('Цинково-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1019">RAL1019<br /><?=Lang::str('Серо-бежевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1020">RAL1020<br /><?=Lang::str('Оливково-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1021">RAL1021<br /><?=Lang::str('Рапсово-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1023">RAL1023<br /><?=Lang::str('Транспортно-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1024">RAL1024<br /><?=Lang::str('Охра жёлтая')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1026">RAL1026<br /><?=Lang::str('Люминесцент. жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1027">RAL1027<br /><?=Lang::str('Карри жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1028">RAL1028<br /><?=Lang::str('Дынно-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1032">RAL1032<br /><?=Lang::str('Жёлтый ракитник')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1033">RAL1033<br /><?=Lang::str('Георгиново-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1034">RAL1034<br /><?=Lang::str('Пастельно-жёлтый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1035">RAL1035<br /><?=Lang::str('Перламутрово-бежевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1036">RAL1036<br /><?=Lang::str('Перламутрово-золотой')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL1037">RAL1037<br /><?=Lang::str('Солнечно-жёлтый')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="oranzhevyj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2000">RAL2000<br /><?=Lang::str('Жёлто-оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2001">RAL2001<br /><?=Lang::str('Красно-оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2002">RAL2002<br /><?=Lang::str('Алый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2003">RAL2003<br /><?=Lang::str('Пастельно-оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2004">RAL2004<br /><?=Lang::str('Оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2005">RAL2005<br /><?=Lang::str('Люминесцентный оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2007">RAL2007<br /><?=Lang::str('Люминесцентный ярко-оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2008">RAL2008<br /><?=Lang::str('Ярко-красно-оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2009">RAL2009<br /><?=Lang::str('Транспортный оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2010">RAL2010<br /><?=Lang::str('Сигнальный оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2011">RAL2011<br /><?=Lang::str('Насыщенный оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2012">RAL2012<br /><?=Lang::str('Лососёво-оранжевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL2013">RAL2013<br /><?=Lang::str('Перламутрово-оранжевый')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="krasnyj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3000">RAL3000<br /><?=Lang::str('Огненно-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3001">RAL3001<br /><?=Lang::str('Сигнальный красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3002">RAL3002<br /><?=Lang::str('Карминно-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3003">RAL3003<br /><?=Lang::str('Рубиново-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3004 w">RAL3004<br /><?=Lang::str('Пурпурно-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3005 w">RAL3005<br /><?=Lang::str('Винно-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3007 w">RAL3007<br /><?=Lang::str('Чёрно-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3009">RAL3009<br /><?=Lang::str('Оксид красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3011">RAL3011<br /><?=Lang::str('Коричнево-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3012">RAL3012<br /><?=Lang::str('Бежево-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3013">RAL3013<br /><?=Lang::str('Томатно-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3014">RAL3014<br /><?=Lang::str('Розовый антик')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3015">RAL3015<br /><?=Lang::str('Светло-розовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3016">RAL3016<br /><?=Lang::str('Кораллово-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3017">RAL3017<br /><?=Lang::str('Розовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3018">RAL3018<br /><?=Lang::str('Клубнично-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3020">RAL3020<br /><?=Lang::str('Транспортный красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3022">RAL3022<br /><?=Lang::str('Лососёво-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3024">RAL3024<br /><?=Lang::str('Люминесцентный красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3026">RAL3026<br /><?=Lang::str('Люминесцентный ярко-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3027">RAL3027<br /><?=Lang::str('Малиново-красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3028">RAL3028<br /><?=Lang::str('Красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3031">RAL3031<br /><?=Lang::str('Ориент красный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3032 w">RAL3032<br /><?=Lang::str('Перламутрово-рубиновый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL3033">RAL3033<br /><?=Lang::str('Перламутрово-розовый')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="fioletovyj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4001">RAL4001<br /><?=Lang::str('Красно-сиреневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4002">RAL4002<br /><?=Lang::str('Красно-фиолетовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4003">RAL4003<br /><?=Lang::str('Вересково-фиолетовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4004 w">RAL4004<br /><?=Lang::str('Бордово-фиолетовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4005">RAL4005<br /><?=Lang::str('Сине-сиреневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4006">RAL4006<br /><?=Lang::str('Транспортный пурпурный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4007 w">RAL4007<br /><?=Lang::str('Пурпурно-фиолетовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4008">RAL4008<br /><?=Lang::str('Сигнальный фиолетовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4009">RAL4009<br /><?=Lang::str('Пастельно-фиолетовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4010">RAL4010<br /><?=Lang::str('Телемагента')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4011">RAL4011<br /><?=Lang::str('Перламутрово-фиолетовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL4012">RAL4012<br /><?=Lang::str('Перламутрово-ежевичный')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="sinij">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5000">RAL5000<br /><?=Lang::str('Фиолетово-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5001">RAL5001<br /><?=Lang::str('Зелёно-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5002 w">RAL5002<br /><?=Lang::str('Ультрамариново-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5003 w">RAL5003<br /><?=Lang::str('Сапфирово-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5004 w">RAL5004<br /><?=Lang::str('Чёрно-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5005">RAL5005<br /><?=Lang::str('Сигнальный синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5007">RAL5007<br /><?=Lang::str('Бриллиантово-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5008">RAL5008<br /><?=Lang::str('Серо-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5009">RAL5009<br /><?=Lang::str('Лазурно-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5010">RAL5010<br /><?=Lang::str('Горечавково-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5011 w">RAL5011<br /><?=Lang::str('Стально-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5012">RAL5012<br /><?=Lang::str('Голубой')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5013 w">RAL5013<br /><?=Lang::str('Кобальтово-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5014">RAL5014<br /><?=Lang::str('Голубино-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5015">RAL5015<br /><?=Lang::str('Небесно-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5017">RAL5017<br /><?=Lang::str('Транспортный синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5018">RAL5018<br /><?=Lang::str('Бирюзово-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5019">RAL5019<br /><?=Lang::str('Капри синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5020">RAL5020<br /><?=Lang::str('Океанская синь')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5021">RAL5021<br /><?=Lang::str('Водная синь')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5022 w">RAL5022<br /><?=Lang::str('Ночной синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5023">RAL5023<br /><?=Lang::str('Отдалённо-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5024">RAL5024<br /><?=Lang::str('Пастельно-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5025">RAL5025<br /><?=Lang::str('Перламутровый горечавково-синий')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL5026 w">RAL5026<br /><?=Lang::str('Перламутровый ночной синий')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="zelenyj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6000">RAL6000<br /><?=Lang::str('Патиново-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6001">RAL6001<br /><?=Lang::str('Изумрудно-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6002">RAL6002<br /><?=Lang::str('Лиственно-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6003">RAL6003<br /><?=Lang::str('Оливково-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6004 w">RAL6004<br /><?=Lang::str('Сине-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6005 w">RAL6005<br /><?=Lang::str('Зелёный мох')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6006">RAL6006<br /><?=Lang::str('Серо-оливковый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6007 w">RAL6007<br /><?=Lang::str('Бутылочно-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6008 w">RAL6008<br /><?=Lang::str('Коричнево-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6009 w">RAL6009<br /><?=Lang::str('Пихтовый зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6010">RAL6010<br /><?=Lang::str('Травяной зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6011">RAL6011<br /><?=Lang::str('Резедово-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6012 w">RAL6012<br /><?=Lang::str('Чёрно-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6013">RAL6013<br /><?=Lang::str('Тростниково-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6014 w">RAL6014<br /><?=Lang::str('Жёлто-оливковый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6015 w">RAL6015<br /><?=Lang::str('Чёрно-оливковый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6016">RAL6016<br /><?=Lang::str('Бирюзово-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6017">RAL6017<br /><?=Lang::str('Майский зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6018">RAL6018<br /><?=Lang::str('Желто-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6019">RAL6019<br /><?=Lang::str('Бело-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6020">RAL6020<br /><?=Lang::str('Хромовый зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6021">RAL6021<br /><?=Lang::str('Бледно-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6022 w">RAL6022<br /><?=Lang::str('Коричнево-оливковый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6024">RAL6024<br /><?=Lang::str('Транспортный зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6025">RAL6025<br /><?=Lang::str('Папоротниково-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6026">RAL6026<br /><?=Lang::str('Опаловый зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6027">RAL6027<br /><?=Lang::str('Светло-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6028">RAL6028<br /><?=Lang::str('Сосновый зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6029">RAL6029<br /><?=Lang::str('Мятно-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6032">RAL6032<br /><?=Lang::str('Сигнальный зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6033">RAL6033<br /><?=Lang::str('Мятно-бирюзовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6034">RAL6034<br /><?=Lang::str('Пастельно-бирюзовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6035">RAL6035<br /><?=Lang::str('Перламутрово-зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6036">RAL6036<br /><?=Lang::str('Перламутровый опаловый зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6037">RAL6037<br /><?=Lang::str('Зелёный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL6038">RAL6038<br /><?=Lang::str('Люминесцентный зелёный')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="seryj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7000">RAL7000<br /><?=Lang::str('Серая белка')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7001">RAL7001<br /><?=Lang::str('Серебристо-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7002">RAL7002<br /><?=Lang::str('Оливково-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7003">RAL7003<br /><?=Lang::str('Серый мох')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7004">RAL7004<br /><?=Lang::str('Сигнальный серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7005">RAL7005<br /><?=Lang::str('Мышино-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7006">RAL7006<br /><?=Lang::str('Бежево-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7008">RAL7008<br /><?=Lang::str('Серое хаки')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7009">RAL7009<br /><?=Lang::str('Зелёно-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7010">RAL7010<br /><?=Lang::str('Брезентово-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7011">RAL7011<br /><?=Lang::str('Железно-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7012">RAL7012<br /><?=Lang::str('Базальтово-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7013">RAL7013<br /><?=Lang::str('Коричнево-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7015">RAL7015<br /><?=Lang::str('Сланцево-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7016 w">RAL7016<br /><?=Lang::str('Антрацитово-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7021 w">RAL7021<br /><?=Lang::str('Чёрно-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7022">RAL7022<br /><?=Lang::str('Серая умбра')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7023">RAL7023<br /><?=Lang::str('Серый бетон')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7024">RAL7024<br /><?=Lang::str('Графитовый серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7026">RAL7026<br /><?=Lang::str('Гранитовый серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7030">RAL7030<br /><?=Lang::str('Каменно-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7031">RAL7031<br /><?=Lang::str('Сине-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7032">RAL7032<br /><?=Lang::str('Галечный серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7033">RAL7033<br /><?=Lang::str('Цементно-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7034">RAL7034<br /><?=Lang::str('Жёлто-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7035">RAL7035<br /><?=Lang::str('Светло-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7036">RAL7036<br /><?=Lang::str('Платиново-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7037">RAL7037<br /><?=Lang::str('Пыльно-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7038">RAL7038<br /><?=Lang::str('Агатовый серый§')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7039">RAL7039<br /><?=Lang::str('Кварцевый серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7040">RAL7040<br /><?=Lang::str('Серое окно')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7042">RAL7042<br /><?=Lang::str('Транспортный серый A')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7043">RAL7043<br /><?=Lang::str('Транспортный серый B')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7044">RAL7044<br /><?=Lang::str('Серый шёлк')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7045">RAL7045<br /><?=Lang::str('Телегрей 1')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7046">RAL7046<br /><?=Lang::str('Телегрей 2')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7047">RAL7047<br /><?=Lang::str('Телегрей 4')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL7048">RAL7048<br /><?=Lang::str('Перламутровый мышино-серый')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="korichnevyj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8000">RAL8000<br /><?=Lang::str('Зелёно-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8001">RAL8001<br /><?=Lang::str('Охра коричневая')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8002">RAL8002<br /><?=Lang::str('Сигнальный коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8003">RAL8003<br /><?=Lang::str('Глиняный коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8004">RAL8004<br /><?=Lang::str('Медно-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8007">RAL8007<br /><?=Lang::str('Олень коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8008">RAL8008<br /><?=Lang::str('Оливково-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8011">RAL8011<br /><?=Lang::str('Орехово-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8012">RAL8012<br /><?=Lang::str('Красно-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8014">RAL8014<br /><?=Lang::str('Сепия коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8015">RAL8015<br /><?=Lang::str('Каштаново-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8016 w">RAL8016<br /><?=Lang::str('Махагон коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8017 w">RAL8017<br /><?=Lang::str('Шоколадно-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8019 w">RAL8019<br /><?=Lang::str('Серо-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8022 w">RAL8022<br /><?=Lang::str('Чёрно-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8023">RAL8023<br /><?=Lang::str('Оранжево-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8024">RAL8024<br /><?=Lang::str('Бежево-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8025">RAL8025<br /><?=Lang::str('Бледно-коричневый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8028 w">RAL8028<br /><?=Lang::str('Терракотовый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL8029">RAL8029<br /><?=Lang::str('Перламутровый медный')?></p>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="chernyjbelyj">
					<div class="row ral-colors">
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9001">RAL9001<br /><?=Lang::str('Кремово-белый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9002">RAL9002<br /><?=Lang::str('Светло-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9003">RAL9003<br /><?=Lang::str('Сигнальный белый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9004 w">RAL9004<br /><?=Lang::str('Сигнальный чёрный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9005 w">RAL9005<br /><?=Lang::str('Чёрный янтарь')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9006">RAL9006<br /><?=Lang::str('Бело-алюминиевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9007">RAL9007<br /><?=Lang::str('Тёмно-алюминиевый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9010">RAL9010<br /><?=Lang::str('Белый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9011 w">RAL9011<br /><?=Lang::str('Графитно-чёрный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9016">RAL9016<br /><?=Lang::str('Транспортный белый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9017 w">RAL9017<br /><?=Lang::str('Транспортный чёрный')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9018">RAL9018<br /><?=Lang::str('Папирусно-белый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9022">RAL9022<br /><?=Lang::str('Перламутровый светло-серый')?></p>
						</div>
						<div class="col-sm-6 col-md-4 col-lg-3 col-xl-2">
							<p class="RAL9023">RAL9023<br /><?=Lang::str('Перламутровый тёмно-серый')?></p>
						</div>
					</div>
				</div>
			</div>
		
		</div>

	</div>


		<div class="container-fluid">
			<div class="wrapper_product no_border_bottom">
				<?php if($var["product"]["files"]["total"]>0) require 'block_media.php';?>
			</div>
		</div>

</article>