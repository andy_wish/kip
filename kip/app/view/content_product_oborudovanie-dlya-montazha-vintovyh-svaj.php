<?php //echo '<pre>';var_dump($var["product"]["infoboxes"]);exit;?>

<article itemscope itemtype="http://schema.org/Product">

	<header class="product_header">
		<link itemprop="url" content="http://<?=$_SERVER['SERVER_NAME']?>/<?=$var["page"]["url"]?>" href="http://<?=$_SERVER['SERVER_NAME']?>/<?=$var["page"]["url"]?>" property="alternate" />
		<meta itemprop="alternativeHeadline" content="<?=$var["page"]["title"]?>" />
		<meta itemprop="image" content="<?=$var["page"]["img"]?>" />
		<?php
			if($var["page"]["keywords"]!=''){
		?>
		<meta itemprop="keywords" content="<?=$var["page"]["keywords"]?>"/>
		<?php
			}
			if($var["page"]["description"]!=''){
		?>
		<meta itemprop="description" content="<?=$var["page"]["description"]?>"/>
		<?php
			}
		?>
		<h1 class="page_title" itemprop="headline name"><?=$var["page"]["h1"]?></h1>
	</header>

	<?php include 'app/view/block_breadcrumb_share.php';?>

	<!--<img itemprop="image" src="/product/<?=$var["page"]["img"]?>" alt="<?=$var["product"]["name"]?>" />-->

	<!--MENU-->
	<div class="row menu_tabs justify-content-around no-gutters">
		<a id="menu_tab_main_desc" class="menu_tabs_main_toggle menu_tab col text-center menu_tab_active" 
			onCLick="box_toggle('.boxes_main_toggle', '#infobox_main_desc', '.menu_tabs_main_toggle', '#menu_tab_main_desc')"><?=$var["product"]["infoboxes"][0]["name"]?></a>
		<a id="menu_tab_main_kompakt" class="menu_tabs_main_toggle menu_tab col text-center" 
			onCLick="box_toggle('.boxes_main_toggle', '#infobox_main_kompakt', '.menu_tabs_main_toggle', '#menu_tab_main_kompakt')"><?=$var["product"]["infoboxes"][1]["name"]?></a>
		<a id="menu_tab_main_standart" class="menu_tabs_main_toggle menu_tab col text-center" 
			onCLick="box_toggle('.boxes_main_toggle', '#infobox_main_standart', '.menu_tabs_main_toggle', '#menu_tab_main_standart')"><?=$var["product"]["infoboxes"][5]["name"]?></a>
		<a id="menu_tab_main_norma" class="menu_tabs_main_toggle menu_tab col text-center" 
			onCLick="box_toggle('.boxes_main_toggle', '#infobox_main_norma', '.menu_tabs_main_toggle', '#menu_tab_main_norma')"><?=$var["product"]["infoboxes"][9]["name"]?></a>
		<a class="menu_tabs_main_toggle menu_tab col text-center menu_tab_last glide" href="#infobox_media"><?=Lang::str('Фото')?>/<?=Lang::str('видео')?></a>
	</div>

	<!--product_toggle-->
	<div id="product_toggle">

		<!--Описание-->
		<div id="infobox_main_desc" class="boxes_main_toggle" style="display:block">
			<div class="container-fluid">
				<div class="wrapper_product">
					<?=$var["product"]["infoboxes"]["0"]["content"]?>
				</div>
			</div>
		</div>

		<!--KOMPAKT-->
		<div id="infobox_main_kompakt" class="boxes_main_toggle" style="display:none">

			<div class="container-fluid">
				<div class="wrapper_product" style="border-bottom:none;">
					<?=$var["product"]["infoboxes"][1]["content"]?>
					<div class="float_right"><?php include 'app/view/block_feedback_btn.php';?></div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="row menu_tabs justify-content-around no-gutters">
				<a
					id="menu_tab_<?=$var["product"]["infoboxes"][2]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle menu_tab menu_tab_active col text-center"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][2]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][2]["id"]?>')">
				<?=$var["product"]["infoboxes"][2]["name"]?></a>
				<a 
					id="menu_tab_<?=$var["product"]["infoboxes"][3]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle menu_tab col text-center"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][3]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][3]["id"]?>')">
				<?=$var["product"]["infoboxes"][3]["name"]?></a>
				<a 
					id="menu_tab_<?=$var["product"]["infoboxes"][4]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle menu_tab col text-center menu_tab_last"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][4]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][4]["id"]?>')">
				<?=$var["product"]["infoboxes"][4]["name"]?></a>
			</div>

			<div class="container-fluid">
				<div class="wrapper_product">
					<div class="row">

						<div class="boxes_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][2]["id"]?>">
							<?=$var["product"]["infoboxes"][2]["content"]?>
						</div>

						<div class="boxes_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][3]["id"]?>" style="display:none">
							<?=$var["product"]["infoboxes"][3]["content"]?>
						</div>

						<div class="boxes_<?=$var["product"]["infoboxes"][1]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][4]["id"]?>" style="display:none">
							<?=$var["product"]["infoboxes"][4]["content"]?>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!--STANDART-->
		<div id="infobox_main_standart" class="boxes_main_toggle" style="display:none">

			<div class="container-fluid">
				<div class="wrapper_product" style="border-bottom:none;">
					<?=$var["product"]["infoboxes"][5]["content"]?>
					<div class="float_right"><?php include 'app/view/block_feedback_btn.php';?></div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="row menu_tabs justify-content-around no-gutters">
				<a
					id="menu_tab_<?=$var["product"]["infoboxes"][6]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle menu_tab menu_tab_active col text-center"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][6]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][6]["id"]?>')">
				<?=$var["product"]["infoboxes"][6]["name"]?></a>
				<a 
					id="menu_tab_<?=$var["product"]["infoboxes"][7]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle menu_tab col text-center"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][7]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][7]["id"]?>')">
				<?=$var["product"]["infoboxes"][7]["name"]?></a>
				<a 
					id="menu_tab_<?=$var["product"]["infoboxes"][8]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle menu_tab col text-center menu_tab_last"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][8]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][3]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][8]["id"]?>')">
				<?=$var["product"]["infoboxes"][8]["name"]?></a>
			</div>

			<div class="container-fluid">
				<div class="wrapper_product">
					<div class="row">

						<div class="boxes_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][6]["id"]?>">
							<?=$var["product"]["infoboxes"][6]["content"]?>
						</div>
						<div class="boxes_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][7]["id"]?>" style="display:none">
							<?=$var["product"]["infoboxes"][7]["content"]?>
						</div>
						<div class="boxes_<?=$var["product"]["infoboxes"][5]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][8]["id"]?>" style="display:none">
							<?=$var["product"]["infoboxes"][8]["content"]?>
						</div>

					</div>
				</div>
			</div>
		</div>

		<!--NORMA-->
		<div id="infobox_main_norma" class="boxes_main_toggle" style="display:none">

			<div class="container-fluid">
				<div class="wrapper_product" style="border-bottom:none;">
					<?=$var["product"]["infoboxes"][9]["content"]?>
					<div class="float_right"><?php include 'app/view/block_feedback_btn.php';?></div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>

			<div class="row menu_tabs justify-content-around no-gutters">
				<a
					id="menu_tab_<?=$var["product"]["infoboxes"][10]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle menu_tab menu_tab_active col text-center"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][10]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][10]["id"]?>')">
				<?=$var["product"]["infoboxes"][10]["name"]?></a>
				<a 
					id="menu_tab_<?=$var["product"]["infoboxes"][11]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle menu_tab col text-center"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][11]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][11]["id"]?>')">
				<?=$var["product"]["infoboxes"][11]["name"]?></a>
				<a 
					id="menu_tab_<?=$var["product"]["infoboxes"][12]["id"]?>"
					class="menu_tabs_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle menu_tab col text-center menu_tab_last"
					onCLick="box_toggle('.boxes_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle', '#infobox_<?=$var["product"]["infoboxes"][12]["id"]?>', '.menu_tabs_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle' ,'#menu_tab_<?=$var["product"]["infoboxes"][12]["id"]?>')">
				<?=$var["product"]["infoboxes"][12]["name"]?></a>
			</div>

			<div class="container-fluid">
				<div class="wrapper_product">
					<div class="row">

						<div class="boxes_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][10]["id"]?>">
							<?=$var["product"]["infoboxes"][10]["content"]?>
						</div>

						<div class="boxes_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][11]["id"]?>" style="display:none">
							<?=$var["product"]["infoboxes"][11]["content"]?>
						</div>

						<div class="boxes_<?=$var["product"]["infoboxes"][9]["id"]?>_toggle col-12" id="infobox_<?=$var["product"]["infoboxes"][12]["id"]?>" style="display:none">
							<?=$var["product"]["infoboxes"][12]["content"]?>
						</div>

					</div>
				</div>
			</div>
		</div>


		<div class="container-fluid">

			<div class="wrapper_product no_border_bottom">
				<?php if($var["product"]["files"]["total"]>0) require 'block_media.php';?>
			</div>

		</div>

	</div>
</article>

