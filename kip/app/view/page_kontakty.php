<div id="contacts_map"></div>

		<div id="contacts_bottom" itemscope itemtype="http://schema.org/LocalBusiness">
			<h3 class="expand_title"><?=Lang::str('Контакты')?></h3>
			<div class="navbar_top_contacts_box">
				<div class="navbar_top_contact">
					<h4 class="navbar_top_contact_title"><?=Lang::str('Адрес')?></h4>
					<div class="navbar_top_contact_text">
						<span  itemprop="name"><?=Lang::str('ООО "НОРД-ИНЖИНИРИНГ"')?></span>
						<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							<span itemprop="postalCode">173008</span>, г.<span itemprop="addressLocality"><?=Lang::str('Великий Новгород')?></span>,<br />
							<span itemprop="streetAddress"><?=Lang::str('Сырковское шоссе')?>, 30</span>
							<a href="https://2gis.ru/v_novgorod/query/%D0%9D%D0%BE%D1%80%D0%B4-%D0%98%D0%BD%D0%B6%D0%B8%D0%BD%D0%B8%D1%80%D0%B8%D0%BD%D0%B3/firm/70000001007430946?queryState=center%2F31.252896%2C58.558119%2Fzoom%2F16" target="_blank"><img src="/app/img/ico_2gis.png" width="39" height="18" alt="Nord-E 2GIS" class="navbar_top_contact_ico_2gis" /></a>
						</div>
					</div>
				</div>
				<div class="navbar_top_contact">
					<h4 class="navbar_top_contact_title"><?=Lang::str('Общие направления')?></h4>
					<div class="navbar_top_contact_text">
						<?=Lang::str('Тел.')?>: <span itemprop="telephone">+7 (8162) 90-15-15</span><br />
						<?=Lang::str('Моб.')?>: <span itemprop="telephone">+7 (921) 730-15-15</span><br />
						E-mail: <a href="mailto:info@nord-e.ru?subject=<?=Lang::str('Сообщение-с-сайта')?>"><span itemprop="email">info@nord-e.ru</span></a>
					</div>
				</div>
				<div class="navbar_top_contact navbar_top_contact_last">
					<h4 class="navbar_top_contact_title"><?=Lang::str('Порошковая окраска')?></h4>
					<div class="navbar_top_contact_text">
						<?=Lang::str('Моб.')?>: +7 (921) 707-00-95<br />
						WhatsApp: +7 (921) 707-00-95<br />
						E-mail: <a href="mailto:kaa@nord-e.ru?subject=<?=Lang::str('Сообщение-с-сайта')?>"><span itemprop="email">kaa@nord-e.ru</span></a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

<script type="text/javascript">
	var infoWindow
	var map;
	function initMap() {
		map = new google.maps.Map(document.getElementById('contacts_map'), {
			center: {lat: 58.55811015, lng: 31.25320673},
			zoom: 16,
			disableDefaultUI: true,
			zoomControl: true,
			mapTypeControl: true,
			//mapTypeId: "roadmap"
		});
		infoWindow = new google.maps.InfoWindow();
(new google.maps.Marker({ position: {lat: 58.55805698, lng: 31.25317454}, map: map, icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAABu0lEQVRoge2Zy42DMBCGpw7byiF1pInUQAcojHNNA9sAdwqgDgpIAzbKlQZ2D8RStAE/AL8QnzTH2P7zj4exDXBwkBev++kskBSf8bqfzrHXZYW8satE1vRIhUT6OxU9UtFXtBVIitjr/eItoJtb/HywLhlBktMfdwH/g9RRRfQVbdeLeKdcRdvsRUQTs006ze6bJoyIG7s6/ctIha6KTUWQAmBXnVgnOS2/fstpaVndnn5FWLlhrkA2qenVFYms2aqMmsR43fj6XGed63iGNBt8aHhPrHFjYk8Yx+O01I9JLpuLEEgK3aRLxw2+TzwKeR5ClrCb1DJNms1mB9hV+d3JB1FyctELsRNj1T37SiuF3TlkbdPonqbO2LnykSKptvEAfk6HQd1QmL4payL4rcqy6x9zGgYVAeDHFcHZI7gQgG1dieKGQnD2yN4NhWtpnQl/7Ygt27gS+cpUsdKVIZnnhnWuJOIGwPioI5EOS4Qk44ZCIqmdS26sG3gdi1zx3aovxcWVJN1QOLX4qbqhsD14xV6nERtXknkANaF3JQM3FLp3lGzcUExVsKQrlY7x8EVqiaTOzomDg5E/u6baDVKP/IAAAAAASUVORK5CYII=", title: decodeURIComponent("%D0%9D%D0%9E%D0%A0%D0%94-%D0%98%D0%9D%D0%96%D0%98%D0%9D%D0%98%D0%A0%D0%98%D0%9D%D0%93") })) .addListener('click', function (e) { infoWindow.setContent(decodeURIComponent("%3Cstrong%3E%D0%9E%D0%9E%D0%9E%20%22%D0%9D%D0%9E%D0%A0%D0%94-%D0%98%D0%9D%D0%96%D0%98%D0%9D%D0%98%D0%A0%D0%98%D0%9D%D0%93%22%3C%2Fstrong%3E%3Cbr%3E%20173008%2C%20%D0%B3.%D0%92%D0%B5%D0%BB%D0%B8%D0%BA%D0%B8%D0%B9%20%D0%9D%D0%BE%D0%B2%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%2C%3Cbr%3E%20%D0%A1%D1%8B%D1%80%D0%BA%D0%BE%D0%B2%D1%81%D0%BA%D0%BE%D0%B5%20%D1%88%D0%BE%D1%81%D1%81%D0%B5%2C%2030%3Cbr%3E%20%D0%A2%D0%B5%D0%BB.%3A%20%2B7%20(8162)%2090-15-15")); infoWindow.open(map, this); });
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMdQ5FrzinGqEZDsRjLWpuWroYIz7UIzM&callback=initMap"
	async defer></script>