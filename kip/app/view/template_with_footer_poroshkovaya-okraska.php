<?php include 'app/view/block_header.php'; ?>

<?php require 'block_sidebar_left.php';?>

<div id="wrapper">

	<div id="main" class="scroll">
		<?php require 'block_navbar_top.php';?>
		<?php require 'app/view/'.$content_file;?>

	<footer id="footer" class="row">
		<span>Моб.:&nbsp;+7&nbsp;(921)&nbsp;707-00-95</span><span>&emsp;|&emsp;</span><span>WhatsApp:&nbsp;+7&nbsp;(921)&nbsp;707-00-95</span><span>&emsp;|&emsp;</span><span>E-mail:&nbsp;kaa@nord-e.ru</span>
	</footer>

	</div>

</div>

<?php include 'app/view/block_footer.php'; ?>