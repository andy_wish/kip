<style>
	html{height:100%}
</style>
<?php
//echo '<pre>';var_dump($var["banner"]);exit;
if(isset($var["banner"]) AND $var["banner"]["total"]>0){
?>
<div class="slider_wrapper">
	<ul class="slider">

<?php
	foreach($var["banner"]["list"] as $row){
?>
	<li class="slider_item" style="

background:url('/files/image/<?=$row["img"]?>');
  background-position: center;
  background-size: cover;
  width: 100%;
  height: 100vh;

">
	</li>
<?php
	}
?>

	</ul>
</div>

<div id="slider_dots">
<?php
	$i=0;
	foreach($var["banner"]["list"] as $row){
?>
		<a href="" data-slide-index="<?=$i?>">&emsp;</a>
<?php
		$i++;
	}
?>
</div>
<div id="slider_contents">
<?php
	$i=0;
	foreach($var["banner"]["list"] as $row){
?>
	<div id="slider_content_wrapper_<?=$i?>" class="slider_content_wrapper align-items-center justify-content-center">
		<div class="slider_content">
			<?=$row["name"]!=''? '<div class="slider_name" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">'.Lang::str($row["name"]).'</div>' : ''?>
			<?=$row["content"]!=''? '<div class="slider_text" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">'.Lang::str($row["content"]).'</div>' : ''?>
			<?=$row["target"]!=''? '<a class="slider_link" href="'.$row["target"].'" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">'.Lang::str('подробнее').'</a>' : ''?>
		</div>
	</div>
<?php
		$i++;
	}
?>
</div>
<?php
}
?>

<script>
$(document).ready(function(){

	$('.slider').bxSlider({
		auto: true,
		pause: 4000,
		responsive: true,

		pager: true,
		pagerCustom: '#slider_dots',
		controls: false,

		onSliderLoad: function (id) {
			slider_show_content(id)
		},

		onSlideBefore: function (el, old_id, id) {
			slider_hide_content(old_id)
			slider_show_content(id)
		},

		onSlideAfter: function (el, old_id, new_id) {
		}

	});

});
function slider_show_content(id){
	var content=$('#slider_content_wrapper_' + id)
	//content.fadeIn(600)
	content.fadeIn(500)
}
function slider_hide_content(id){
	var content=$('#slider_content_wrapper_' + id)
	content.slideUp(10)
}
</script>