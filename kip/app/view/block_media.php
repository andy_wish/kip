<?php //echo '<pre>';var_dump($var["product"]["files"]);exit;?>
<div id="infobox_media">
	<div class="row">
		<div class="col-md-6">
			<h2 class="infobox_title"><?=Lang::str('Фото')?><?=isset($var["product"]["files"]["youtube"])? '/'.Lang::str('видео') : ''?></h2>
		</div>
<?php
if(isset($var["product"]["files"]["video"])){
?>
		<div class="col-md-6 text-center">
			<div class="infobox_media_switches row justify-content-around no-gutters">
				<div class="infobox_media_switch col" onClick="infobox_media_switch('photo')"><?=Lang::str('Фото')?></div>
				<div class="infobox_media_switch infobox_media_switch_last col" onClick="infobox_media_switch('video')"><?=Lang::str('Видео')?></div>
			</div>
		</div>
<?php
}
?>
	</div>
</div>

<div style="min-height:90vh;">
	<!--MEDIA PHOTO-->
	<div class="row" id="infobox_media_photo">
		<div class="col-12 slider_wrapper_infobox">
			<div class="fotorama"
				data-nav="thumbs"
				data-thumbheight="100"
				data-allowfullscreen="true"
				data-transition="slide"
				data-clicktransition="crossfade"

				data-loop="true"
				data-autoplay="3000"
				data-keyboard="true"
				data-arrows="true"
				data-click="true"
				data-swipe="false"
				data-nav="thumbs"
				data-stopautoplayontouch="true"
				data-shadows="true"
			>
<?php
foreach($var["product"]["files"]["image"] as $row){
?>
				<img src="/files/image/<?=$row["img"]?>">
<?php
}
?>
			</div>


		</div>
	</div>

	<!--MEDIA VIDEO-->
	<div class="row" id="infobox_media_video">
		<div class="col-12 slider_wrapper_infobox">
			<div class="fotorama"
				data-nav="thumbs"
				data-thumbheight="100"
				data-allowfullscreen="true"
				data-width="1100"
				data-transition="slide"
				data-clicktransition="crossfade"

				data-loop="true"
				data-autoplay="3000"
				data-keyboard="true"
				data-arrows="true"
				data-swipe="false"
				data-nav="thumbs"
				data-stopautoplayontouch="true"
				data-shadows="true"
			>
<?php
foreach($var["product"]["files"]["video"] as $row){
?>
				<a href="https://youtube.com/watch?v=<?=$row["img"]?>"><?=$row["img"]?></a>
<?php
}
?>
			</div>
		</div>
	</div>

</div>



<script>
function infobox_media_switch(target){

	if(target=='video'){
		$('#infobox_media_photo').css({'display':'none'})
		$('#infobox_media_video').css({'display':'block'})
	}else if(target=='photo'){
		$('#infobox_media_video').css({'display':'none'})
		$('#infobox_media_photo').css({'display':'block'})
	}

	setTimeout(function(){
		$('body,html').animate({ scrollTop: $('#infobox_media').offset().top }, 400);
	}, 200);

}
</script>