<div id="menu_trigger_box">
	<div id="menu_trigger_btn" onClick="menu_trigger_click()"></div>
</div>

<div id="menu_box">
<?php
if($var["page"]["controller"]!='index') {
?>
		<a href="/">
			<img id="top_logo_img" src="/app/img/logo.png" alt="<?=Lang::str(Core::config('site_name'))?>" height="104" width="158" />
			<!--<h5 id="top_title"><?=Lang::str(Core::config('site_name'))?></h5>-->
		</a>
<?php
}else{
?>
		<img id="top_logo_img" src="/app/img/logo.png" alt="<?=Lang::str(Core::config('site_name'))?>" height="104" width="158" />
		<!--<h1 id="top_title"><?=Lang::str(Core::config('site_name'))?></h1>-->
<?php
}
?>

		<ul class="nav flex-column" id="menu_nav">
			<li class="nav-item">
				<a class="nav-link
<?php
if($var["page"]["controller"]=='index') echo' actived';
?>
				" href="/"><?=Lang::str('Главная')?></a>
			</li>
<?php
foreach($var["category"]["all"]["list"] as $row){

	if($row["name_translit"]==$var["page"]["current"]) $active_mark=' actived';
	else $active_mark='';
?>
			<li class="nav-item">
				<a class="nav-link<?=$active_mark?>" href="/<?=$row["name_translit"]?>"><?=$row["name"]?></a>
			</li>
	
<?php
}
?>
			<li class="nav-item">
				<a class="nav-link
<?php
if($var["page"]["controller"]=='page' AND $var["page"]["action"]=='kontakty') echo' actived';
?>
				" href="/page/kontakty"><?=Lang::str('Контакты')?></a>
			</li>
		</ul>

		<div id="menu_footer">
			<div id="menu_social">
				<a href="https://www.youtube.com/channel/UCQ7w4mAOE4PvmM9W5jh8kkA" target="_blank"><span class="social youtube"></span></a>
				<a href="https://vk.com/svaekrut_nord" target="_blank"><span class="social vk"></span></a>
				<a href="https://www.instagram.com/nord_engineering" target="_blank"><span class="social instagram"></span></a>
				<a href="#" target="_blank"><span class="social facebook"></span></a>
				<div class="clear"></div>
			</div>
			
			<div class="da_design"><a href="http://dmitry-a.ru" target="_blank"><span>Design by DA design</span></a></div>
			<div class="copyright"><span>© Copyright 2010 - 2019<br />
				<?=Lang::str('Все права защищены')?></span></div>

		</div>
</div>