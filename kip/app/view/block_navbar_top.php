<div id="navbar_top">

		<div id="navbar_top_buttons">
			<a href="#" class="navbar_top_button" onClick="navbar_top_show(event, 'contacts')">
				<span id="navbar_top_button_contacts"></span>
			</a>
			<a href="#" class="navbar_top_button" onClick="navbar_top_show(event, 'feedback')">
				<span id="navbar_top_button_feedback"></span>
			</a>
			<a href="#" class="navbar_top_button" onClick="navbar_top_show(event, 'search')">
				<span id="navbar_top_button_search"></span>
			</a>
				
<?php include 'block_lang_change.php';?>
			<div class="clear"></div>
		</div>

	<div class="" id="navbar_top_expand">

		<div id="navbar_top_contacts">
			<h3 class="expand_title"><?=Lang::str('Контакты')?></h3>
			<div class="navbar_top_contacts_box">
				<div class="navbar_top_contact">
					<h4 class="navbar_top_contact_title"><?=Lang::str('Адрес')?></h4>
					<div class="navbar_top_contact_text">
						<?=Lang::str('ООО "НОРД-ИНЖИНИРИНГ"')?><br />
						173008, <?=Lang::str('г.Великий Новгород')?>,<br />
						<?=Lang::str('Сырковское шоссе')?>, 30
						<a href="https://2gis.ru/v_novgorod/query/%D0%9D%D0%BE%D1%80%D0%B4-%D0%98%D0%BD%D0%B6%D0%B8%D0%BD%D0%B8%D1%80%D0%B8%D0%BD%D0%B3/firm/70000001007430946?queryState=center%2F31.252896%2C58.558119%2Fzoom%2F16" target="_blank"><img src="/app/img/ico_2gis.png" width="39" height="18" alt="Nord-E 2GIS" class="navbar_top_contact_ico_2gis" /></a>
					</div>
				</div>
				<div class="navbar_top_contact">
					<h4 class="navbar_top_contact_title"><?=Lang::str('Общие направления')?></h4>
					<div class="navbar_top_contact_text">
						<?=Lang::str('Тел.')?>: +7 (8162) 90-15-15<br />
						<?=Lang::str('Моб.')?>: +7 (921) 730-15-15<br />
						E-mail: info&copy;nord-e.ru
					</div>
				</div>
				<div class="navbar_top_contact navbar_top_contact_last">
					<h4 class="navbar_top_contact_title"><?=Lang::str('Порошковая окраска')?></h4>
					<div class="navbar_top_contact_text">
						<?=Lang::str('Моб.')?>: +7 (921) 707-00-95<br />
						WhatsApp: +7 (921) 707-00-95<br />
						E-mail: kaa&copy;nord-e.ru
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div id="navbar_top_feedback">
			<h3 class="expand_title"><?=Lang::str('Обратная связь')?></h3>
			<form id="feedback_menu_top" method="post" action="#">
				<div class="row no-gutters">
					<div class="col-md-4 col-xl-3">
						<input type="text" class="form-control navbar_top_feedback_input feedback_data" id="feedback_name" name="Имя" placeholder="<?=Lang::str('Ваше имя')?>" required><br />
						<input type="email" class="form-control navbar_top_feedback_input feedback_data" id="feedback_email" name="email" placeholder="<?=Lang::str('Ваш адрес электронной почты')?>"><br />
						<input type="text" class="form-control navbar_top_feedback_input feedback_data" id="feedback_phone" name="телефон" placeholder="<?=Lang::str('Ваш номер телефона')?>"><br />
						<a href="#" class="feedback_btn text-center" onClick="feedback_submit(event, '#feedback_menu_top', 'Обратная связь')">
							<span class="navbar_top_lang_letters"><?=Lang::str('ОТПРАВИТЬ')?></span>
						</a>
					</div>

					<div class="col-md-8 col-xl-8 pl-md-4 ml-md-2 mb-3">
						<textarea class="form-control navbar_top_feedback_textarea feedback_data" id="feedback_text" name="комментарий" rows="6" placeholder="<?=Lang::str('Ваш вопрос или комментарий')?>" required></textarea>
						<label class="checkbox_container">
							<?=Lang::str('Согласен на обработку персональных данных')?>
							<input type="checkbox" name="agree" class="feedback_data">
							<span class="checkbox_checkmark"></span>
						</label>
						<div id="feedback_checkbox_desc">
							<?=Lang::str('Нажимая кнопку "Отправить", я даю согласие на обработку всех моих персональных данных, указанных в форме обращения, а также получения информации в соответствии с Федеральным законом "О персональных данных" от 27.07.2006 N 152-ФЗ.')?>
						</div>
					</div>

				</div>
			</form>
		</div>

		<div id="navbar_top_search">
			<h3 class="expand_title"><?=Lang::str('Поиск')?></h3>
			<div class="input-group">
				<input type="text" class="form-control expand_search_input" id="search_box" placeholder="<?=Lang::str('Поиск по сайту')?>" autocomplete="off" value="" maxlength="20" OnKeyUp="search_top_main(event.keyCode)" OnClick="search_top_main('click')" />
				<div class="input-group-append">
					<span class="input-group-text badge badge-secondary" id="search_count_box"></span>
				</div>
			</div>
			<table class="table table-sm table-striped" id="search_list"></table>
		</div>

	</div>


</div>