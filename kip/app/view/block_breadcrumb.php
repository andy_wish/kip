<ol class="breadcrumb breadcrumb_product" itemscope itemtype="http://schema.org/BreadcrumbList">
	<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a href="//<?=$_SERVER['SERVER_NAME']?>" itemprop="item">
			<span itemprop="name"><?=Lang::str('Главная')?></span>
		</a>
		<meta itemprop="position" content="1" />
	</li>
<?php
$i=2;
foreach($var["breadcrumb"] as $b){
?>
	<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a href="<?=$b["url"]?>" itemprop="item">
			<span itemprop="name"><?=Lang::str($b["name"])?></span>
		</a>
		<meta itemprop="position" content="<?=$i?>" />
	</li>
<?php
	$i++;
}
?>
	<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<span itemprop="name"><?=Lang::str($var["page"]["h1"])?></span>
		<meta itemprop="position" content="<?=$i?>" />
	</li>
</ol>