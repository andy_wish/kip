<?php //echo '<pre>';var_dump($var);exit;?>
<article itemscope itemtype="http://schema.org/Product">

	<header class="product_header">
		<link itemprop="url" content="http://<?=$_SERVER['SERVER_NAME']?>/<?=$var["page"]["url"]?>" href="http://<?=$_SERVER['SERVER_NAME']?>/<?=$var["page"]["url"]?>" property="alternate" />
		<meta itemprop="alternativeHeadline" content="<?=Lang::str($var["page"]["title"])?>"/>
		<meta itemprop="image" content="<?=$var["page"]["img"]?>" />
<?php
	if($var["page"]["keywords"]!=''){
?>
		<meta itemprop="keywords" content="<?=Lang::str($var["page"]["keywords"])?>"/>
<?php
	}
	if($var["page"]["description"]!=''){
?>
		<meta itemprop="description" content="<?=Lang::str($var["page"]["description"])?>"/>
<?php
	}
?>
		<h1 class="page_title" itemprop="headline name"><?=Lang::str($var["page"]["h1"])?></h1>
	</header>

<?php include 'app/view/block_breadcrumb_share.php';?>

	<div class="row menu_tabs justify-content-around no-gutters">
<?php
foreach($var["product"]["infoboxes"] as $infobox){
?>
		<a class="menu_tab col text-center glide" href="#infobox_<?=$infobox["id"]?>"><?=$infobox["name"]?></a>
<?php
}
if($var["product"]["files"]["total"]>0){
?>
		<a class="menu_tab col text-center menu_tab_last glide" href="#infobox_media"><?=Lang::str('Фото')?>/<?=Lang::str('видео')?></a>
<?php
}
?>
	</div>
	<div class="container-fluid">
<?php
$infobox_last=count($var["product"]["infoboxes"]);
$i=0;
foreach($var["product"]["infoboxes"] as $infobox){
	$i++;
?>
		<div class="wrapper_product" id="infobox_<?=$infobox["id"]?>">
<?php
			echo $infobox["content"];
?>
			<div class="clear"></div>
<?php
	if($i==$infobox_last) {
?>
			<div class="float_right">
				<?php include 'app/view/block_feedback_btn.php';?>
			</div>
			<div class="clear"></div>
<?php
	}
?>

		</div>
<?php
}
?>
	</div>

	<div class="container-fluid">

		<div class="wrapper_product no_border_bottom">
			<?php if($var["product"]["files"]["total"]>0) require 'block_media.php';?>
		</div>

	</div>


</article>