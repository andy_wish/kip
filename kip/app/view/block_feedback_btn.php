<a href="#" class="feedback_btn text-center" onClick="feedback_draw(event)">
	<span class="navbar_top_lang_letters"><?=Lang::str('Заказать обратный звонок')?></span>
</a>

<script type="text/javascript">
function feedback_draw(e){
	e.preventDefault()

	modal_clear('top');

	$('#top_modal_body').append(
		$('<form id="feedback_product_<?=$var["product"]["id"]?>" method="post" action="#">').append(
			$('<div class="row no-gutters">').append(
				$('<div class="col-md-5 col-xl-4">').append(
					$('<input type="text" class="form-control navbar_top_feedback_input feedback_data" id="feedback_name_product" placeholder="<?=Lang::str('Ваше имя')?>" name="Имя"><br />'),
					$('<input type="email" class="form-control navbar_top_feedback_input feedback_data" id="feedback_email_product" placeholder="<?=Lang::str('Ваш адрес электронной почты')?>" name="email"><br />'),
					$('<input type="text" class="form-control navbar_top_feedback_input feedback_data" id="feedback_phone_product" placeholder="<?=Lang::str('Ваш номер телефона')?>" name="телефон"><br />'),
					$('<a href="#" class="feedback_btn text-center" onClick="feedback_submit(event, \'#feedback_product_<?=$var["product"]["id"]?>\', \'<?=$var["product"]["name"]?>\')"><span class="navbar_top_lang_letters"><?=Lang::str('ОТПРАВИТЬ')?></span></a>'),
				),
				$('<div class="col-md-7 col-xl-7 pl-md-4 ml-md-2 mb-3">').append(
					$('<textarea class="form-control navbar_top_feedback_textarea feedback_data" id="feedback_text" name="комментарий" rows="6" placeholder="<?=Lang::str('Ваш вопрос или комментарий')?>" required></textarea>'),
					$('<label class="checkbox_container">').append(
						$('<?=Lang::str('Согласен на обработку персональных данных')?>'),
						$('<input type="checkbox" name="agree" class="feedback_data">'),
						$('<span class="checkbox_checkmark"></span>'),
					),
					$('<div id="feedback_checkbox_desc">').append(
						$('<span><?=Lang::str('Нажимая кнопку "Отправить", я даю согласие на обработку всех моих персональных данных, указанных в форме обращения, а также получения информации в соответствии с Федеральным законом "О персональных данных" от 27.07.2006 N 152-ФЗ.')?></span>'),
					),
				),
			),
		),
	);
	modal_show('top', '50rem');
	document.getElementById("feedback_name_product").focus();
	wait_finish();
}
</script>