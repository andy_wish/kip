<?php if (!defined("INBOX")) die('separate call');

class Category_controller extends Controller {

	function __construct($var) {
		$this->model=new Category_model();
		$this->view=new View();

		if(isset($var[0])){
			$action='product_read';
		}else{
			$action='read';
		}

		if(!Access::permit($var["page"]["controller"], $action)) Core::error(401);
		else $this->$action($var);

	}


	function read($var) {
		$var=$this->model->read($var);
		$this->view->generate('template_with_footer.php', 'content_category.php', $var);
	}

	function product_read($var) {
		$var=$this->model->product_read($var);

		switch($var[0]){//костыли: ошибки в проектировании
			//ru
			case 'poroshkovaya-okraska':
				$this->view->generate('template_with_footer_poroshkovaya-okraska.php', 'content_product_poroshkovaya-okraska.php', $var);
			break;
			case 'oborudovanie-dlya-montazha-vintovyh-svaj':
				$this->view->generate('template_with_footer.php', 'content_product_oborudovanie-dlya-montazha-vintovyh-svaj.php', $var);
			break;

			//en
			case 'powder-coating':
				$this->view->generate('template_with_footer_poroshkovaya-okraska.php', 'content_product_poroshkovaya-okraska.php', $var);
			break;
			case 'equipment-for-the-installation-of-screw-piles':
				$this->view->generate('template_with_footer.php', 'content_product_oborudovanie-dlya-montazha-vintovyh-svaj.php', $var);
			break;

			//de
			case 'pulverbeschichtung':
				$this->view->generate('template_with_footer_poroshkovaya-okraska.php', 'content_product_poroshkovaya-okraska.php', $var);
			break;
			case 'ausrüstung-für-die-montage-der-schraubpfähle':
				$this->view->generate('template_with_footer.php', 'content_product_oborudovanie-dlya-montazha-vintovyh-svaj.php', $var);
			break;
			
			
			default:
				$this->view->generate('template_with_footer.php', 'content_product.php', $var);
		}

	}

}
