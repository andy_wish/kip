<?php if (!defined("INBOX")) die('separate call');

class Index_model extends Model {

	public function index($var) {

		$var["banner"]=Basis::read([
			'type'=>'banner_index',
			'response'=>[
				'order'=>'order_num',
				'direction'=>'asc'
			]
		]);

		//PAGE
		$var["page"]["title"]=Core::config('site_name');
		$var["page"]["description"]='';
		$var["page"]["keywords"]='';
		$var["page"]["h1"]=Core::config('site_name');

		return $var;
	}

}
