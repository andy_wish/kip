<?php if (!defined("INBOX")) die('separate call');

class Page_controller extends Controller {

	function __construct($var) {
		//$this->model=new Page_model();
		$this->view=new View();

		if(isset($var[0])){
			$var["page"]["action"]=$var[0];
			if(!file_exists('app/view/page_'.$var["page"]["action"].'.php')) Core::error(404);
		}else Core::error(404);

		if(!Access::permit($var["page"]["controller"], $var["page"]["action"])) Core::error(401);

		Cache::start($var["page"]["controller"], $var["page"]["action"]);

		$this->view->generate('template_main_scroll.php', 'page_'.$var["page"]["action"].'.php', $var);

		Cache::finish();

	}

}
