<?php if (!defined("INBOX")) die('separate call');

class Category_model extends Model {

	public function read($var) {

		$var["category"]["one"]=Basis::read([
			"type"=>'category',
			"name_translit"=>$var["cat_name"],
			"response"=>[
				"limit"=>1
			]
		]);
		if(!$var["category"]["one"]) Core::error(404);
		$var["page"]["current"]=$var["category"]["one"]["list"][0]["name_translit"];

		$var["products"]=Basis::read([
			"type"=>'product',
			"id_parent"=>$var["category"]["one"]["list"][0]["id"],
			"response"=>[
				"order"=>'order_num'
			]
		]);

		//PAGE
		$var["page"]["h1"]=$var["category"]["one"]["list"][0]["name"];

		$var["category"]["one"]["list"][0]["title"]!=''? $var["page"]["title"]=$var["category"]["one"]["list"][0]["title"] : $var["page"]["title"]=$var["category"]["one"]["list"][0]["name"];

		$var["category"]["one"]["list"][0]["description"]!=''? $var["page"]["description"]=$var["category"]["one"]["list"][0]["description"] : $var["page"]["description"]=$var["category"]["one"]["list"][0]["name"];

		$var["category"]["one"]["list"][0]["keywords"]!=''? $var["page"]["keywords"]=$var["category"]["one"]["list"][0]["keywords"] : $var["page"]["keywords"]=$var["category"]["one"]["list"][0]["name"];

		$var["page"]["img"]='';


		//BREADCRUMB
		$var["breadcrumb"]=[];

		return $var;
	}

	public function product_read($var) {

		$var["category"]["one"]=Basis::read([
			"type"=>'category',
			"name_translit"=>$var["cat_name"],
			"response"=>[
				"limit"=>1
			]
		]);
		if(!$var["category"]["one"]) Core::error(404);

		$var["product"]=Basis::read([
			"type"=>'product',
			"name_translit"=>$var[0],
			"response"=>[
				"limit"=>1
			]
		]);
		if(isset($var["product"]["error"]) OR !$var["product"]["total_db"]) Core::error(404);
		$var["product"]=$var["product"]["list"][0];
		//echo "<pre>";var_dump($var);die();

		$var["product"]["infoboxes"]=Basis::read([
			"type"=>'infobox',
			"id_parent"=>$var["product"]["id"],
			"response"=>[
				"order"=>'order_num',
				"direction"=>'asc',
				"limit"=>200
			]
		]);
		if(isset($var["product"]["infoboxes"]["error"]) OR !$var["product"]["infoboxes"]["total_db"]) Core::error(404);
		$var["product"]["infoboxes"]=$var["product"]["infoboxes"]["list"];

		$var["product"]["files"]["total"]=0;
		$files=Basis::read([
			"type"=>'file_product',
			"id_parent"=>$var["product"]["id"],
			"response"=>[
				"order"=>'order_num',
				"limit"=>100
			]
		]);
		if(!isset($files["error"])){
			foreach($files["list"] as $file){
						$var["product"]["files"]["total"]++;
						$mime_type=explode('/', $file["mime_type"]);
						$mime_type=$mime_type[0];
						if(!isset($var["product"]["files"]["$mime_type"])) $var["product"]["files"]["$mime_type"]=[];
						$var["product"]["files"]["$mime_type"][]=[
							"id"=>$file["id"],
							"img"=>$file["img"],
							"name"=>$file["name"],
							"content"=>$file["content"],
							"order_num"=>$file["order_num"]
						];
					}
		}

		//echo "<pre>";var_dump($var["product"]);die();

		//PAGE
		//$var["page"]["h1"]=$var["product"]["name"];

		$var["product"]["title"]!=''? $var["page"]["title"]=$var["product"]["title"] : $var["page"]["title"]=$var["product"]["name"];

		$var["page"]["h1"]=$var["product"]["name"];
		$var["product"]["description"]!=''? $var["page"]["description"]=$var["product"]["description"] : $var["page"]["description"]=$var["product"]["name"];

		$var["product"]["keywords"]!=''? $var["page"]["keywords"]=$var["product"]["keywords"] : $var["page"]["keywords"]=$var["product"]["name"];

		if($var["product"]["img"]!='') $var["page"]["img"]='/files/image/'.$var["product"]["img"];
		else $var["page"]["img"]='/images/no-img.png';

		//BREADCRUMB
		$var["breadcrumb"]=[["url"=>'/'.$var["category"]["one"]["list"][0]["name_translit"],"name"=>$var["category"]["one"]["list"][0]["name"]]];

		//echo "<pre>";var_dump($var);die();
		return $var;
	}
}
