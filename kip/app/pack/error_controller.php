<?php if (!defined("INBOX")) die('separate call');

class Error_controller extends Controller {

	function __construct($var) {
		$this->view = new View();
		isset($var[0])? $var["error_num"]=$var[0] : $var["error_num"]=0;

		//BREADCRUMB
		$var["breadcrumb"]=[];

		switch($var["error_num"]){

			case 401:
				header('HTTP/1.1 401 Unauthorized');
				header("Status: 401 Unauthorized");
				$var["page"]["title"]='Требуется аутентификация';
				$var["page"]["description"]=$var["page"]["title"];
				$var["page"]["keywords"]=$var["page"]["title"];
				$var["page"]["h1"]=$var["page"]["title"];
				break;

			case 403:
				$var["page"]["title"]='Сюда нельзя.';
				$var["page"]["description"]=$var["page"]["title"];
				$var["page"]["keywords"]=$var["page"]["title"];
				$var["page"]["h1"]='Ошибка доступа';
				break;

			case 404:
				header('HTTP/1.1 404 Not Found');
				header("Status: 404 Not Found");
				$var["page"]["title"]='Страница не найдена';
				$var["page"]["description"]=$var["page"]["title"];
				$var["page"]["keywords"]=$var["page"]["title"];
				$var["page"]["h1"]='Добро пожаловать на страницу 404!';
				break;

			case 500://Internal Server Error
				$var["page"]["title"]='500 Internal Server Error';
				$var["page"]["description"]=$var["page"]["title"];
				$var["page"]["keywords"]=$var["page"]["title"];
				$var["page"]["h1"]=$var["page"]["title"];
				break;

			case 502:
				$var["page"]["title"]='502 Bad Gateway';
				$var["page"]["description"]=$var["page"]["title"];
				$var["page"]["keywords"]=$var["page"]["title"];
				$var["page"]["h1"]=$var["page"]["title"];
				break;

			default:
				$var["page"]["title"]='Ошибка :(';
				$var["page"]["description"]=$var["page"]["title"];
				$var["page"]["keywords"]=$var["page"]["title"];
				$var["page"]["h1"]=$var["page"]["title"];
		}

		$this->view->generate('template_main.php', 'content_error.php', $var);
	}

}
