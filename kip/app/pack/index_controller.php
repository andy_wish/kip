<?php if (!defined("INBOX")) die('separate call');

class Index_controller extends Controller {

	function __construct($var) {
		$this->model = new Index_model();
		$this->view = new View();
		$this->index($var);
	}

	function index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template_main.php', 'content_index.php', $var);
	}

}
