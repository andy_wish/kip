function jsonrpc_request(method, params, id=1, async=false, callback=false){

	if(token=getCookie('data')) params["token"]=token;

	var data={"jsonrpc":"2.0", "method":method, "params":params, "id":id};
	var req=getXmlHttp();

	req.open('post', 'http://nord/jsonrpc/', async);
	//req.open('post', 'https://umdi.ru/jsonrpc/', async);

	req.send(JSON.stringify(data));
	if(!async){
		if(req.status==200) return JSON.parse(req.responseText);
	}else if(callback){
		req.onreadystatechange = function () {
			if(req.readyState==4 && req.status==200) return callback(JSON.parse(req.responseText));
		}
	}
	wait_finish(0, 'jsonrpc error');
}
function getXmlHttp(){var xmlhttp;try {xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(e){try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(E){xmlhttp=false;}}if(!xmlhttp&&typeof XMLHttpRequest!='undefined'){xmlhttp=new XMLHttpRequest();}return xmlhttp;}