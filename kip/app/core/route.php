<?php if (!defined("INBOX")) die('separate call');

class Route {

	static function start($var) {

		$routes=explode('?', $_SERVER['REQUEST_URI']);
		$var["page"]["url"]=$routes[0];
		$routes=explode('/', $routes[0]);

		if (!empty($routes[1])) {
			$controller=$routes[1];
			if (isset($routes[2]) AND $routes[2]!='') {
				$var[0]=$routes[2];
				if (isset($routes[3]) AND $routes[3]!='') {
					$var[1]=$routes[3];
					if (isset($routes[4]) AND $routes[4]!='') {
						$var[2]=$routes[4];
						if (isset($routes[5]) AND $routes[5]!='') {
							$var[3]=$routes[5];
							if (isset($routes[6]) AND $routes[6]!='') {
								$var[4]=$routes[6];
							}
						}
					}
				}
			}
		}else $controller='index';

		$var["page"]["controller"]=$controller;

		$controller_path = 'app/pack/'.strtolower($controller).'_controller.php';
		if(file_exists($controller_path)) include $controller_path;
		else{
			$var["cat_name"]=$controller;
			$controller='category';
			$var["page"]["controller"]=$controller;
			include 'app/pack/category_controller.php';
		}

		$model_path = 'app/pack/'.strtolower($controller).'_model.php';
		if(file_exists($model_path)) include $model_path;

		$controller_name=$controller.'_controller';
		$controller=new $controller_name($var);
	}

}