<?php if (!defined("INBOX")) die('separate call');

Cache::init();

class Cache{

	protected static $_instance;
	protected static $_config=[
		"status"=>false,
		"compress"=>false
	];

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	public static function init(){
		if (self::$_instance === null) {
			self::$_instance = new self;
			if(Core::config('cache_web')){
				self::$_config["status"]=true;
				if(Core::config('cache_web_compress')) self::$_config["compress"]=true;
				if (!is_dir(ROOT_DIR.'/app/cache/')) mkdir(ROOT_DIR.'/app/cache/', 0777);
			}
		}
	}

	public static function start($controller, $tag){

		if(!self::$_config["status"]) return true;

		if (!is_dir(ROOT_DIR.'/app/cache/'.$controller.'/')) mkdir(ROOT_DIR.'/app/cache/'.$controller.'/', 0777);

		self::$_config["name"]=ROOT_DIR.'/app/cache/'.$controller.'/'.$tag.'.cache';
		if (file_exists(self::$_config["name"])) {
			$html=file_get_contents(self::$_config["name"]);
			if (self::$_config["compress"]) {
				header('content-encoding: gzip');
				header('vary: accept-encoding');
				header('content-length: '.strlen($html));
			}
			echo $html;
			exit();
		}
		ob_start();
	}

	public static function finish(){
		if(!self::$_config["status"]) return true;

		include (ROOT_DIR.'/plugins/optimize/optimize.php');
		$html=Optimize::html(ob_get_contents());
		if (self::$_config["compress"]) $html=gzencode($html);
		ob_end_flush();
		$f=fopen(self::$_config["name"], 'w');
		fwrite($f, $html);
		fclose($f);

	}

}