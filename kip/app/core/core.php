<?php if (!defined("INBOX")) die('separate call');

class Core{

	protected static $_instance;
	protected static $config=[];
	///public static $var=[];?

	private function __clone(){}
	private function __wakeup(){}

	private function __construct(){
		$a=DB::getAll('SELECT `name`, `value` FROM `options` WHERE `autoload`=1');
		foreach($a as $b){
			self::$config[$b["name"]]=$b["value"];
		}
	}

	public static function init($var) {
		if (self::$_instance === null) {
			self::$_instance = new self();

			$var=self::environments($var);

			$var=Lang::init($var);

			$var=self::get_page_data($var);

			//$var["widgets"]=self::get_widgets();

			return $var;
		}
	}

	private static function environments($var){
		if(!empty($_POST)){
			foreach($_POST as $name => $value){
				$var["post"]["$name"]=$value;
			}
		}
		if(!empty($_GET)){
			foreach($_GET as $name => $value){
				$var["get"]["$name"]=$value;
			}
		}
		if(!empty($_COOKIE)){
			foreach($_COOKIE as $name => $value){
				$var["cookie"]["$name"]=$value;
			}
		}
		return $var;
	}

	public static function config($name){
		if(isset(self::$config[$name])) return self::$config[$name];
		else return self::config_find_value($name);
	}


	public static function get_page_data($var){
		$var["page"]["current"]='';
		$var["page"]["title"]='';
		$var["page"]["description"]='';
		$var["page"]["keywords"]='';
		$var["page"]["action"]='index';

		$var["category"]["all"]=Basis::read([
			"type"=>'category',
			"id_parent"=>0,
			"lang"=>Lang::current_lang()
		]);
		return $var;
	}

	private static function config_find_value($name){
		if($val=DB::getOne('SELECT `value` FROM `options` WHERE `name`=?s LIMIT 1', $name)){
			self::config_add($name, $val);
			return $val;
		}else return false;
	}

	private static function config_add($name, $val){
		self::$config[$name]=$val;
	}

	public static function error($error) {
		header('Location: /error/'.$error);
		exit;
	}

	public static function translit($s) {
		//$s=(string) $s; // в строковое значение
		//$s=strip_tags($s); // HTML-теги
		$s=str_replace(array("\n", "\r"), " ", $s); // перевод каретки
		$s=str_replace("+", " ", $s);
		$s=str_replace("/", " ", $s);
		$s=str_replace(".", " ", $s);
		$s=trim($s); // пробелы в начале и конце строки
		$s=strtr($s, array(
			'а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>'',
			'А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Д'=>'d','Е'=>'e','Ё'=>'e','Ж'=>'j','З'=>'z','И'=>'i','Й'=>'y','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'c','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch','Ы'=>'y','Э'=>'e','Ю'=>'yu','Я'=>'ya','Ъ'=>'','Ь'=>''
		));
		$s=preg_replace("/[^0-9a-z-_ ]/i", "", $s); // от недопустимых символов
		$s=preg_replace("/\s+/", ' ', $s); // повторяющие пробелы
		$s=str_replace(" ", "-", $s); // пробелы знаком минус
		return $s;
	}
}