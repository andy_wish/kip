<?php if (!defined("INBOX")) die('separate call');

DB::init($var["db"]);
unset($var["db"]);

require ROOT_DIR.'/app/core/view.php';
require ROOT_DIR.'/app/core/controller.php';
require ROOT_DIR.'/app/core/model.php';

require ROOT_DIR.'/app/core/core.php';
$var=Core::init($var);

require ROOT_DIR.'/app/core/cache.php';

isset($_COOKIE["data"])? User::auth($_COOKIE["data"]) : User::auth(false);

require ROOT_DIR.'/app/access.php';
Access::init($conf["access"]);

require ROOT_DIR.'/app/core/route.php';
Route::start($var);