<?php if (!defined("INBOX")) die('separate call');

class Category_model extends Model {

	public function index($var) {

		$var["basis"]=Basis::read([
			"type"=>'category',
			"id_parent"=>0,
			"closed"=>1,
			"response"=>[
				"order"=>'order_num'
			]
		]);
		$var["basis"]["id_parent"]=0;

		//PAGE
		$var["page"]["title"]='Категории';
		$var["page"]["h1"]='Категории';

		//BREADCRUMB
		$var["breadcrumb"]=[];
		return $var;
	}

	public function listing($var) {

		/////PARAMS/////////////////////////////////////////////////////////
		$params=[
			"id_parent"=>(int)$var[0],
			"closed"=>1,
		];

		$params["response"]=Core::environment_response((int)Core::config('default_items_per_page'));
//var_dump($params);exit;
		$var["basis"]=Basis::read($params);

		$var["basis"]["id_parent"]=$var[0];


		//PAGE
		if($var["basis"]["total"]>0){
			$var["page"]["title"]=$var["basis"]["list"][0]["name_parent"];
			$var["page"]["h1"]=$var["basis"]["list"][0]["name_parent"];
		}else{
			$parent_info=Basis::read([
				"id"=>$var[0],
				"response"=>[
					"limit"=>1
				]
			]);
			$var["page"]["title"]=$parent_info["list"][0]["name"];
			$var["page"]["h1"]=$parent_info["list"][0]["name"];
		}

		//BREADCRUMB
		$var["breadcrumb"]=[
			["name"=>'Категории', "url"=>'/admin/category']
		];
		$parent_info=Basis::parents(["id"=>$var[0]]);
		foreach($parent_info as $row){
			$var["breadcrumb"][]=[
				"name"=>$row["name"],
				"url"=>'/admin/category/'.$row["id"]
			];
		}
		//var_dump($var["breadcrumb"]);exit;
		

		////PAGINATION
		if($var["basis"]["total_db"]>$params["response"]["limit"]){
			$var["pagination"]["finish"]=ceil($var["basis"]["total_db"]/$params["response"]["limit"]);
			if($var["pagination"]["finish"]<=$var["pagination"]["current"]){
				$var["pagination"]["finish"]=false;
				$var["pagination"]["next"]=false;
			}else{
				$var["pagination"]["next"]=$var["pagination"]["current"]+1;
			}

			if($var["pagination"]["current"]>1) {
				$var["pagination"]["start"]=1;
				$var["pagination"]["prev"]=$var["pagination"]["current"]-1;
			}else {
				$var["pagination"]["start"]=false;
				$var["pagination"]["prev"]=false;
			}

			if($var["pagination"]["prev"] AND ($var["pagination"]["prev"]-1)>0) $var["pagination"]["prev2"]=$var["pagination"]["prev"]-1;
			else $var["pagination"]["prev2"]=false;

			if($var["pagination"]["next"] AND ($var["pagination"]["next"]+1)<$var["pagination"]["finish"]) $var["pagination"]["next2"]=$var["pagination"]["next"]+1;
			else $var["pagination"]["next2"]=false;
			//echo '<pre>';var_dump($var["pagination"]);
		}else $var["pagination"]["current"]=false;


		return $var;
	}

}
