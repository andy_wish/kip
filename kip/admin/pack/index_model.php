<?php if (!defined("INBOX")) die('separate call');

class Index_model extends Model {

	public function index($var) {

		//PAGE
		$var["page"]["title"]=Core::config('site_name');
		$var["page"]["description"]='';
		$var["page"]["keywords"]='';
		//$var["page"]["h1"]=Core::config('site_name');
		$var["page"]["current"]='index';
		$var["page"]["h1"]='Последние комментарии';


		$var["comment"]=Basis::read([
			"type"=>'comment',
			//"closed"=>1,
			"lang"=>'all',
			"response"=>[
				"order"=>'created',
				"direction"=>'desc',
				"limit"=>Core::config('comments_per_page')
			]
		]);
		return $var;
	}


}
