<?php if (!defined("INBOX")) die('separate call');

class Product_model extends Model {


	public function create($var) {//

		if(!isset($var[0])) Core::error_404();

		$var["id"]=false;
		$var["cat_id"]=(int)$var[0];

		$params["response"]["type"]='one';
		$params["id"]=$var["cat_id"];
		$result=Category::read($params);
		
		$var["cat_name"]=$result["name"];
		
		$var["form_action"]='/admin/category/create/action/'.$var["cat_id"];
		$var["name"]='';
		$var["date"]='';
		$var["text"]='';
		$var["title"]='Создать';
		$var["show_files"]=false;

		return $var;
	}

	public function create_action($var) {//
		if(!isset($var["post"])) Core::error_404();
		if(isset($var[1])) $params["cat_id"]=(int)$var["1"];
		else {
			$var["error"]='cat_id required';
			return $var;
		}
		
		if(isset($var["post"]["name"])) $params["name"]=$var["post"]["name"];
		else {
			$var["error"]='name required';
			return $var;
		}
		
		if(isset($var["post"]["text"])) $params["text"]=$var["post"]["text"];

		$result=category::create($params);

		if(isset($result["error"])) {
			$var["error"]=$result["error"];
			return $var;
		}else $var["success"]='Статья #'.$result["id"].' создана. Можете добавить файлы.';

		$params=array(
			"id"=>$result["id"],
			"response"=>array(
				"limit"=>1,
				"type"=>'read'
			)
		);
		$var["category"]=category::read($params);

		$var["id"]=$var["category"]["list"][0]["id"];
		$var["cat_id"]=$var["category"]["list"][0]["cat_id"];
		$var["cat_name"]=$var["category"]["list"][0]["cat_name"];
		$var["form_action"]='/admin/category/update/'.$var["cat_id"].'/'.$var["id"];
		$var["name"]=$var["category"]["list"][0]["name"];
		$var["date"]=$var["category"]["list"][0]["create_nice"];
		$var["text"]=$var["category"]["list"][0]["text"];

		$var["title"]=$var["name"];
		$var["show_files"]=true;
		$var["files"]=$var["category"]["list"][0]["files"];
//echo '<pre>';var_dump($var);
		return $var;
	}

	public function read($var) {

		$var["category"]["one"]=Basis::read([
			"id"=>$var[1],
			"response"=>[
				"limit"=>1
			]
		]);
		if(!$var["category"]["one"]) Core::error(404);
		$var["category"]["one"]=$var["category"]["one"]["list"][0];

		$var["product"]=Basis::read([
			"id"=>$var[2],
			"lang"=>'all',
		]);
		if(!$var["product"]) Core::error(404);
		$var["product"]=$var["product"]["list"][0];

		$var["product"]["infoboxes"]=Basis::read([
			"type"=>'infobox',
			"id_parent"=>$var["product"]["id"],
			"lang"=>'all',
			"response"=>[
				"order"=>'order_num',
				"limit"=>200
			]
		]);
		if(isset($var["product"]["infoboxes"]["error"]) OR !$var["product"]["infoboxes"]["total_db"]) Core::error(404);
		$var["product"]["infoboxes"]=$var["product"]["infoboxes"]["list"];

		$var["product"]["files"]["total"]=0;
		$files=Basis::read([
			"type"=>'file_product',
			"id_parent"=>$var["product"]["id"],
			"response"=>[
				"order"=>'order_num',
				"limit"=>200
			]
		]);
		if(!isset($files["error"])){
			foreach($files["list"] as $file){
						$var["product"]["files"]["total"]++;
						$mime_type=explode('/', $file["mime_type"]);
						$mime_type=$mime_type[0];
						if(!isset($var["product"]["files"]["$mime_type"])) $var["product"]["files"]["$mime_type"]=[];
						$var["product"]["files"]["$mime_type"][]=[
							"id"=>$file["id"],
							"img"=>$file["img"],
							"name"=>$file["name"],
							"content"=>$file["content"],
							"order_num"=>$file["order_num"]
						];
					}
		}

		//PAGE
		$var["page"]["h1"]=$var["product"]["name"];
		$var["page"]["title"]=$var["product"]["name"];
		$var["page"]["img"]=$var["product"]["img"];

		//BREADCRUMB
		$var["breadcrumb"]=[["url"=>'/admin/category/read/'.$var["category"]["one"]["id"],"name"=>$var["category"]["one"]["name"]]];

		return $var;
	}

	public function update($var) {//

		if(isset($var[1])) $params["id"]=(int)$var[1];
		else {
			$var["error"]='id required';
			return $var;
		}

		if(isset($var["post"]["name"])) $params["name"]=$var["post"]["name"];
		else {
			$var["error"]='name required';
			return $var;
		}

		if(isset($var["post"]["text"])) $params["text"]=$var["post"]["text"];

		$result=category::update($params);
		if(isset($result["error"])) {
			$var["error"]=$result["error"];
			return $var;
		}

		$params=array(
			"id"=>$result["id"],
			"response"=>array(
				"limit"=>1,
				"type"=>'read',
				"status"=>'all'
			)
		);

		$var["category"]=Category::read($params);

		$var["id"]=$var["category"]["list"][0]["id"];
		$var["cat_id"]=$var["category"]["list"][0]["cat_id"];
		$var["cat_name"]=$var["category"]["list"][0]["cat_name"];
		$var["form_action"]='/admin/category/update/'.$var["cat_id"].'/'.$var["id"];
		$var["name"]=$var["category"]["list"][0]["name"];
		$var["date"]=$var["category"]["list"][0]["create_nice"];
		$var["text"]=$var["category"]["list"][0]["text"];

		$var["title"]=$var["name"];
		$var["show_files"]=true;
		if(isset($var["category"]["list"][0]["files"]) AND $var["category"]["list"][0]["files"]!='') {
			$var["files"]=$var["category"]["list"][0]["files"];
		}else $var["files"]=array();

		return $var;
	}

}
