<?php if (!defined("INBOX")) die('separate call');

class Page_controller extends Controller {

	function __construct() {
		$this->model = new Page_model();
		$this->view = new View();
	}

	function action_kontakty($var) {
		$var=$this->model->kontakty($var);
		$this->view->generate('template.php', 'content_page_kontakty.php', $var);
	}

}
