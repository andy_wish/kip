<?php if (!defined("INBOX")) die('separate call');

class Product_controller extends Controller {

	function __construct($var) {

		$this->model=new Product_model();
		$this->view=new View();
		$var["page"]["current"]='product';

		if(isset($var[0])){
			switch($var[0]){
				case 'create':
					$action='create';
					break;
				case 'read':
					$action='read';
					break;
				case 'update':
					$action='update';
					break;
				default:
					Core::error(404);
			}
		}else{
			Core::error(404);
		}

		if(Access::permit('product', $action)) $this->$action($var);
		else Core::error(401);
	}

	function create($var) {//

		if(isset($var[0]) AND $var[0]=='action') $var=$this->model->create($var);
		else $var=$this->model->create($var);
		$this->view->generate('template.php', 'content_product_create.php', $var);
	}

	function read($var) {

		$var=$this->model->read($var);
		$this->view->generate('template.php', 'content_product.php', $var);
	}

	function update($var) {//

		$var=$this->model->update($var);
		$this->view->generate('template.php', 'content_product.php', $var);
	}

}
