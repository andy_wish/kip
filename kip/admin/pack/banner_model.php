<?php if (!defined("INBOX")) die('separate call');

class Banner_model extends Model {

	public function index($var) {
		$var["page"]["title"]='Баннеры';
		$var["banner"]=Basis::read([
			"type"=>'banner_index',
			"closed"=>1
		]);
		return $var;
	}

}
