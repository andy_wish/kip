<?php if (!defined("INBOX")) die('separate call');

class Files_controller extends Controller {

	function __construct($var) {
		$this->model=new Files_model();
		//$this->view=new View();
		if(isset($var[0])) {
			$action=$var[0];
			if(Access::permit('category', $action)) $this->$action($var);
			else Core::error(401);
		}else Core::error(404);
	}

	function create_ajax($var) {
		$this->model->create_ajax($var);
	}

	function update_ajax($var) {
		$this->model->update_ajax($var);
	}

	function delete_ajax($var) {
		$this->model->delete_ajax($var);
	}

}
