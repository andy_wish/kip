<?php if (!defined("INBOX")) die('separate call');

class User_controller extends Controller {

	function __construct(){
		//$this->model = new User_model();
		$this->view = new View();
	}

	function action_index($var) {//
		Core::error_404();
		//$this->view->generate('template.php', 'content_user.php', User::info());
	}

	function action_login($var) {
		if(!isset($var["post"]["user_email"]) OR !isset($var["post"]["user_password"])){
			$var["error"]='login data error';
			return $var;
		}

		$var["login_result"]=User::login($var["post"]["user_email"], $var["post"]["user_password"]);

		if(isset($var["login_result"]["error"])) {
			$location='/admin/?auth_error='.$var["login_result"]["error"];
			header('Location: '.$location);
			exit();
		}

		header('Location: /admin/');
		exit();
	}

	function action_logout($var) {
		SetCookie('data', '', time(), '/');
		if(User::logout()) header('Location: /admin/');
		else header('Location: /admin/?auth_error=logout_error');
		exit();
	}

	function action_registration($var) {
		$var["page"]["current"]='user';
		$var["page"]["title"]='Регистрация';
		$var["page"]["keywords"]='Регистрация';
		$this->view->generate('template.php', 'content_user_registration.php', $var);
	}

	function action_registration_action($var) {
		$var=User::registration_start($var);

		if(!isset($var["error"])) {
			$var["action"]='check_email';
			$var["page"]["title"]='Проверка почты';
		}else {
			$var["action"]='registration_error';
			$var["page"]["title"]='Ошибка при регистрации';
		}

		$this->view->generate('template.php', 'content_user_registration_action.php', $var);
	}

	function action_activate($var) {
		$var=User::email_activate($var);

		if(!isset($var["error"])) {
			$var["page"]["title"]='Успешно проверили почту';
			$var["action"]='activation_success';
		}else {
			$var["page"]["title"]='Ошибка';
			$var["action"]='activation_error';
		}

		$this->view->generate('template.php', 'content_user_registration_action.php', $var);
	}

}
