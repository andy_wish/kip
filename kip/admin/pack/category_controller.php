<?php if (!defined("INBOX")) die('separate call');

class Category_controller extends Controller {

	function __construct($var) {

		$this->model=new Category_model();
		$this->view=new View();
		$var["page"]["current"]='category';

		if(isset($var[0])){
			$action='listing';
		}else{
			$action='index';
		}

		if(Access::permit('category', $action)) $this->$action($var);
		else Core::error(401);
	}

	function index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_basis_list.php', $var);
	}

	function listing($var) {
		$var=$this->model->listing($var);
		$this->view->generate('template.php', 'content_basis_list.php', $var);
	}

}
