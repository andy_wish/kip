<?php if (!defined("INBOX")) die('separate call');

class File_model extends Model {

	public function create_ajax($var) {

		//var_dump($_POST);
		//var_dump($_FILES);

		if(!isset($_POST['product_id']) AND !isset($_POST['banner_mode']) AND !isset($_POST['banner_image_change_mode'])) die(json_encode(array("total"=>0, "error"=> 'product_id or banner_mode or banner_image_change_mode required')));

		$result=[
			"total"=>0,
			"list"=>[]
		];
		$i=-1;
		
		if(isset($_POST["product_id"])) {
			$product_id=(int)$_POST["product_id"];
			foreach($_FILES as $file){
				$i++;
				$result["list"]["$i"]=Files::upload($file, 'product');
				if(!isset($result["list"]["$i"]["error"])) {
					$result["list"]["$i"]["product_id"]=$product_id;
					$result["list"]["$i"]=array_merge($result["list"]["$i"], Files::create_product($result["list"]["$i"]));
				}
			}
		}elseif(isset($_POST["banner_mode"])){
			foreach($_FILES as $file){
				$i++;
				$result=Files::upload($file, 'banner');
				if(!isset($result["error"])) {
					$result=Banner::create(array("image"=> $result["src"]));
				}
			}

		}elseif(isset($_POST["banner_image_change_mode"])){
			$banner_id=(int)$_POST["banner_id"];
			$image_old=$_POST["image_old"];

			foreach($_FILES as $file){
				$i++;
				$result=Files::upload($file, 'banner');
				if(!isset($result["error"])) {
					$result=Banner::update(array("id"=> $banner_id, "image"=> $result["src"]));
					if(!isset($result["error"])) unlink (ROOT_DIR.'/files/image/'.$image_old);
				}
			}

		}

		echo json_encode($result);
		exit;

	}

	public function update_ajax($var) {
		//var_dump($_POST);
		if(!isset($_POST['id'])) die(json_encode(array('error'=> 'id required')));

		$_POST["id"]=(int)$_POST["id"];

		$answer=Files::update($_POST);
		echo json_encode($answer);
		exit;
	}
	
	public function delete_ajax($var) {
		//var_dump($_POST);
		if(!isset($_POST['id'])) die(json_encode(array('error'=> 'id required')));
		if(!isset($_POST['type'])) die(json_encode(array('error'=> 'type required')));
		if(!isset($_POST['src'])) die(json_encode(array('error'=> 'src required')));

		$answer=Files::delete(array("id"=> (int)$_POST["id"]));
		if(!isset($answer["error"])){
			if (file_exists(ROOT_DIR.'/files/'.$_POST['type'].'/'.$_POST['src'])) {
				unlink (ROOT_DIR.'/files/'.$_POST['type'].'/'.$_POST['src']);
			}else die(json_encode(array('error'=> 'file not exist')));
			
		}

		echo json_encode($answer);
		exit;
	}
}
