<?php if (!defined("INBOX")) die('separate call');

class Banner_controller extends Controller {

	function __construct($var) {
		$this->model=new Banner_model();
		$this->view=new View();
		$var["page"]["current"]='banner';

		$action='index';

		if(Access::permit('banner', $action)) $this->$action($var);
		else Core::error(401);
	}

	function index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_banner_list.php', $var);
	}

}
