<?php if (!defined("INBOX")) die('separate call');

DB::init($var["db"]);
unset($var["db"]);

require ROOT_DIR.'/admin/core/view.php';
require ROOT_DIR.'/admin/core/controller.php';
require ROOT_DIR.'/admin/core/model.php';

require ROOT_DIR.'/admin/core/core.php';
$var=Core::init($var);

isset($_COOKIE["data"])? User::auth($_COOKIE["data"]) : User::auth(false);

require ROOT_DIR.'/admin/access.php';
Access::init($conf["access"]);

require ROOT_DIR.'/admin/core/route.php';
Route::start($var);