var pattern={

	category:{
		draw:{

			create:[
			{
				name: 'type',
				type: 'hidden',
				value: 'category'
			},
			{
				name: 'id_parent',
				type: 'hidden'
			},
			{
				name: 'name',
				title: 'Имя',
				type: 'text',
				required: true
			},
			{
				name: 'content',
				title: 'Описание',
				type: 'textarea',
				rows: 4
			}
			],

			update:[
			{
				name: 'id',
				type: 'hidden'
			},
			{
				name: 'name',
				title: 'Имя',
				type: 'text'
			},
			{
				name: 'content',
				title: 'Описание',
				type: 'textarea'
			}
			]
		}
	},

	product:{
		
	}
}

function basis_draw(type, action, params){

	modal_clear('top');

	/*$('#top_modal_header').append(
		$('<h5></h5>'),
	);*/
	
	$('#top_modal_body').append(
		$('<form id="basis_'+action+'_form">')
	)

	$.each(pattern[type]["draw"][action], function(item, value){

		if(typeof(params[this["name"]])!=='undefined') var value=params[this["name"]]
		else if(typeof(this["value"])!=='undefined') var value=this["value"]
		else var value=''

		switch(this["type"]){
			case "hidden":
				$('#basis_'+action+'_form').append(
					$('<input type="hidden" name="'+this["name"]+'" value="'+value+'" class="submit_data" id="basis_'+action+'_input_'+this["name"]+'" />')
				)
			break

			case "text":
				$('#basis_'+action+'_form').append(
					$('<div class="form-group">').append(
						$('<label for="exampleInputPassword1">'+this["title"]+'</label>'),
						$('<input type="text" name="'+this["name"]+'" value="'+value+'" class="submit_data form-control" id="basis_'+action+'_input_'+this["name"]+'" />')
					)
				)
			break

			case "textarea":
				$('#basis_'+action+'_form').append(
					$('<div class="form-group">').append(
						$('<label for="exampleInputPassword1">'+this["title"]+'</label>'),
						$('<textarea name="'+this["name"]+'" class="submit_data form-control" id="basis_'+action+'_input_'+this["name"]+'" rows="'+this["rows"]+'">'+value+'</textarea>'),
					)
				)
			break

		}
	})

	$('#top_modal_footer').append(
		$('<div class="input-group col-7">').append(
			$('<span class="input-group-text text-danger" id="basis_'+action+'_msg"></span>'),
			$('<div class="input-group-append">').append(
				$('<button class="btn btn-success btn-block" OnClick="basis_submit(\''+type+'\', \''+action+'\')">Сохранить</button>'),
				$('<button class="btn btn-danger" OnClick="modal_close(\'top\');">Отмена</button>'),
			)
		)
	);

	modal_show('top', '50rem');

	/*setTimeout(function() {
		document.getElementById('').focus();
	}, 500);*/

}
function basis_submit(type, action){

	var params = {}
	var error={
		status: false,
		msg: ''
	}

	$.each(pattern[type]["draw"][action], function(item, value){

		if(
			typeof(this.required)!=='undefined' &&
			this.required===true &&
			$('#basis_'+action+'_input_'+this.name).val()==''
		){
			error.status=true
			error.msg=this.title+'&nbsp;<strong>required</strong>'
		}

		params[this.name]=$('#basis_'+action+'_input_'+this.name).val()

	})

	console.log(params)
	//console.log(error)

	if(error.status){
		//$('#basis_'+action+'_msg').addClass('text-danger')
		$('#basis_'+action+'_msg').html(error.msg)
		return
	}

	switch(action){

		case "create":
			var answer = basis_create(params);
			break

		case "update":
			var answer = basis_update(params);
			break

	}

	if(typeof(answer["error"])!="undefined"){
		$('#basis_'+action+'_msg').html(answer["result"])
		return
	}
	
	modal_close('top')
}

//CREATE
function basis_create(params){
	wait_start()
	var answer = jsonrpc_request("basis.create", params)
	wait_finish()
	return answer
}
//READ
function basis_read(params){
	wait_start()
	var answer = jsonrpc_request("basis.read", params)
	wait_finish()
	return answer
}
//UPDATE
function basis_update(params){
	wait_start()
	var answer = jsonrpc_request("basis.update", params)
	wait_finish()
	return answer
}
function basis_update_one(id, item, val){

	wait_start()

	var params={"id": parseInt(id)};

	switch(item){

		case 'order_num':
			params[item]=parseInt(val)
			break
		case 'status':
			if($('#status_btn_'+id).hasClass('btn-success')){
				$('#status_btn_'+id).removeClass('btn-success').addClass('btn-danger')
				params["closed"]=1
			}else{
				$('#status_btn_'+id).removeClass('btn-danger').addClass('btn-success')
				params["closed"]=0
			}
			break
		default:
			params[item]=val

	}

	jsonrpc_request("basis.update", params, 1, true, wait_finish)
}
//DELETE
function basis_delete(basis_id, basis_wipe){
	wait_start()
	var answer = jsonrpc_request("basis.delete", {id: basis_id, wipe: basis_wipe})
	wait_finish()
	return answer
}