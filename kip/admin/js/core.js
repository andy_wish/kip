$(document).ready(function(){

});
/*
/////NAV
function nav_draw(){
	document.getElementById('nav_box').innerHTML='';
	$.each(pattern, function(item, value) {
		if(current["type"]==item) var nav_active_mark=' active';
		else var nav_active_mark='';
		$('#nav_box').append(
			$('<li class="nav-item'+nav_active_mark+'">').append(
				$('<a class="nav-link" href="#" onClick="nav_click(\''+item+'\')">'+value["name"]+'</a>')
			)
		)
	})
	
}
function nav_click(type){
	current["type"]=type;
	
	
	basis_draw()
	breadcrumb_draw()
}
////////Breadcrumb
function breadcrumb_draw(){
	document.getElementById('breadcrumb_box').innerHTML='';
	$.each(current["breadcrumb"], function(item, value) {
		if(current["type"]==item) var nav_active_mark=' active';
		else var nav_active_mark='';
		$('#breadcrumb_box').append(
			$('<li class="breadcrumb-item">').append(
				$('<button class="btn btn-secondary" onClick="breadcrumb_click('+this["id_parent"]+', \''+this["name"]+'\')">'+this["name"]+'</button>')
			)
		)
	})
	//<li class="breadcrumb-item active">
}


function getTmpl(name) {
	if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
		$.ajax({
			url : '/admin/template/'+name+'.html',
			success : function(data) {
				if (Handlebars.templates === undefined) {
					Handlebars.templates = {};
				}
				Handlebars.templates[name] = Handlebars.compile(data);
			},
			async : false
		});
	}
	return Handlebars.templates[name];
}
*/

function modal_clear(position){$('#'+position+'_modal_header').empty();$('#'+position+'_modal_body').empty();$('#'+position+'_modal_footer').empty();}
function modal_show(position, width='60rem'){$('.modal_box').css('max-width', width);$('#'+position+'_modal').modal();}
function modal_close(position){$('#'+position+'_modal').modal('hide');}
function wait_start(){$('#wait_notify').fadeIn();}
function wait_finish(result=1, desc=''){$('#wait_notify').fadeOut();}
function setCookie(n,v,h) {var d = new Date();d.setTime(d.getTime() + (h*60*60*1000));var expires="expires="+ d.toUTCString();document.cookie=n+"="+v+";"+expires+";path=/";}
function getCookie(n) {var n=n+"=";var decodedCookie=decodeURIComponent(document.cookie);var ca=decodedCookie.split(';');for(var i=0;i<ca.length;i++) {var c=ca[i];while (c.charAt(0)==' '){c=c.substring(1);}if (c.indexOf(n)==0) {return c.substring(n.length,c.length);}}return "";}