<div class="row my-3 justify-content-center">
	<div class="сol-12" id="pagination">
		<div class="btn-toolbar" role="toolbar" aria-label="Pagination">
<?php

		if($var["pagination"]["start"]) {
?>
			<div class="btn-group mr-2" role="group" aria-label="В начало страниц">
				<a href="<?=$var["page"]["url"]?>" class="btn btn-secondary">В начало</a>
			</div>
<?php
		}
?>
			<div class="btn-group mr-2" role="group" aria-label="">
<?php
		if($var["pagination"]["prev"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev"]?>" class="btn btn-secondary"><i class="fas fa-arrow-left"></i></a>
<?php
		}
		if($var["pagination"]["prev2"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev2"]?>" class="btn btn-secondary"><?=$var["pagination"]["prev2"]?></a>
<?php
		}
		if($var["pagination"]["prev"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev"]?>" class="btn btn-secondary"><?=$var["pagination"]["prev"]?></a>
<?php
		}
?>

				<span class="btn btn-warning pagination_current"><?=$var["pagination"]["current"]?></span>

<?php
		if($var["pagination"]["next"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next"]?>" class="btn btn-secondary"><?=$var["pagination"]["next"]?></a>
<?php
		}
		if($var["pagination"]["next2"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next2"]?>" class="btn btn-secondary"><?=$var["pagination"]["next2"]?></a>
<?php
		}
		if($var["pagination"]["next"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next"]?>" class="btn btn-secondary"><i class="fas fa-arrow-right"></i></a>
<?php
		}
?>
			</div>
<?php
		if($var["pagination"]["finish"]) {
?>
			<div class="btn-group mr-2" role="group" aria-label="В конец страниц">
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["finish"]?>" class="btn btn-secondary">В конец</a>
			</div>
<?php
		}
?>
		</div>
	</div>
</div>