<?php if (!defined("INBOX")) die('separate call');?>
<?php
if(Auth::id()){
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12 text-center">
			<h4 class="m-3">Вход только для администраторов</h4>
		</div>
	</div>
</div>
	
<?php
	exit;
}

if(isset($_GET["auth_error"])){
	switch ($_GET["auth_error"]) {
		case 'login_error':
			$auth_error='Неправильные логин или пароль.';
			break;
		case 'password invalid':
			$auth_error='Пароль не подходит';
			break;
		case 'user not found':
			$auth_error='Пользователь не найден';
			break;
		case 1:
			$auth_error='Ошибка ввода данных.';
			break;
		case 'logout_error':
			$auth_error='Ошибка выхода.';
			break;
		case 3:
			$auth_error='Ошибка. Подмена запроса.';
		default:
			$auth_error='Ошибка.';
	}
	echo '<div class="text-center m-3"><span class="badge badge-danger">'.$auth_error.'</span></div>';
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-12 text-center">
			<h4 class="m-3">Необходимо войти в систему</h4>
		</div>
	</div>
</div>

<div class="container p-4" style="max-width:20rem">

	<form class="form-signin" action="/admin/user/login/" method="post">
		<div class="form-group">
			<label class="sr-only" for="user_email">E-mail</label>
			<div class="input-group mb-2 mr-sm-2 mb-sm-0">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-at"></i></div>
				</div>
				<input type="email" name="user_email" class="form-control" id="user_email" placeholder="e-mail" required />
			</div>
		</div>

		<div class="form-group">
			<label class="sr-only" for="user_password">Пароль</label>
			<div class="input-group mr-sm-2 mb-sm-0">
				<div class="input-group-prepend">
					<div class="input-group-text"><i class="fas fa-key"></i></div>
				</div>
				<input type="password" name="user_password" class="form-control" id="user_password" placeholder="пароль" required />
			</div>
		</div>
		
		<div class="form-group">
			<div class="input-group mr-sm-2 mb-sm-0">
				<button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> вход</button>
			</div>
		</div>

	</form>

</div>