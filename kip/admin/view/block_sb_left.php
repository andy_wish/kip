<nav class="navbar navbar-expand-md navbar-light">
	<button class="navbar-toggler navbar-toggler-left mb-1" type="button" data-toggle="collapse" data-target="#navbar_sb_left" aria-controls="navbar_sb_left" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="row pl-1 collapse navbar-collapse" id="navbar_sb_left">
		<div class="col-md-12 col-sm-6" id="content_box_sb_left_fixed">
		
		
		</div>
		<div class="col-md-12 col-sm-6" id="content_box_sb_left"></div>

	</div>

</nav>