<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/admin/">Главная</a></li>
				<li class="breadcrumb-item active">Опросы</li>
			</ol>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="form-group">
				<!--<label for="poll_create_file" style="cursor:pointer"><h5><i class="far fa-plus-square"></i> добавить</h5></label>
				<input type="file" onChange="poll_create(this.files)" accept="<?=Files::upload_accept_type('poll')?>" class="form-control-file" id="poll_create_file" />-->
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">

<?php
if($var["poll"]["total"]>0){
?>
<div class="row justify-content-center">
	<div class="col-xl-11 col-lg-12">
		<table class="table table-sm table-striped table-bordered" id="polls">
			<thead>
				<tr>
					<th>№</th>
					<th>Название</th>
					<th>Описание</th>
					<th class="text-right">начало</th>
					<th class="text-right">окончание</th>
					<th class="text-right">статус</th>
				</tr>
			</thead>
<?php


	foreach($var["poll"]["list"] as $row){

		if($row["finished"]==1) {
			$active_mark=' text-muted';
			$status_btn='<button id="status_btn_'.$row["id"].'" class="btn btn-sm btn-danger py-0 px-1" title="включить" onClick="poll_update('.$row["id"].', \'status\', 1)"><i class="fas fa-toggle-off"></i></button>';
		}else{
			$active_mark='';
			$status_btn='<button id="status_btn_'.$row["id"].'" class="btn btn-sm btn-success py-0 px-1" title="отключить" onClick="poll_update('.$row["id"].', \'status\', 0)"><i class="fas fa-toggle-on"></i></button>';
		}
?>
			<tr class="<?=$active_mark?>">
				<td style="width:2rem">
					<a class="btn btn btn-secondary" title="открыть" href="/admin/poll/read_filled/<?=$row["id"]?>"><i class="fas fa-list-ol"></i>&nbsp<?=$row["id"]?></a>
				</td>
				<td style="width:20rem">
					<input class="form-control" type="text" id="poll_<?=$row["id"]?>_name" onKeyUp="poll_update(<?=$row["id"]?>, 'name', this.value)" style="width:100%;" value="<?=isset($row["name"])? $row["name"]: ''?>" />
				</td>
				<td style="width:20rem">
					<input class="form-control" type="text" id="poll_<?=$row["id"]?>_description" onKeyUp="poll_update(<?=$row["id"]?>, 'description', this.value)" style="width:100%;" value="<?=isset($row["description"])? $row["description"]: ''?>" />
				</td>
				<td style="width:7rem" class="text-right">
					<!--<input class="form-control" type="text" id="poll_<?=$row["id"]?>_start" onKeyUp="poll_update(<?=$row["id"]?>, 'start', this.value)" style="width:100%;" value="<?=$row["start_nice"]?>" />-->
					<?=$row["start_nice"]?>
				</td>
				<td style="width:7rem" class="text-right">
					<!--<input class="form-control" type="text" id="poll_<?=$row["id"]?>_finish" onKeyUp="poll_update(<?=$row["id"]?>, 'finish', this.value)" style="width:100%;" value="<?=$row["finish_nice"]?>" />-->
					<?=$row["finish_nice"]?>
				</td>
				<td style="width:2rem" class="text-right">
					<?=$status_btn?>
				</td>
			</tr>
<?php
	}
?>
		</table>
	</div>
</div>

<?php
}else{
?>
<div class="row justify-content-center">
	<div class="сol-12 my-2"><p>не найдено</p></div>
</div>
<?php
}
?>

</div>

<script type="text/javascript">

function poll_update(id, target, val){
	//var val=$('#poll_'+id+'_'+target).val();
	var params={"id": parseInt(id)};

	switch(target){
		case 'name':
			params["name"]=val;
			break;
		case 'description':
			params["description"]=val;
			break;
		case 'status':
			if($('#status_btn_'+id).hasClass('btn-success')){
				$('#status_btn_'+id).removeClass('btn-success').addClass('btn-danger');
				params["status"]=0;
			}else{
				$('#status_btn_'+id).removeClass('btn-danger').addClass('btn-success');
				params["status"]=1 ;
			}
			break;
		default:
			return;
	}
	jsonrpc_request("poll.update", params, 1, true);

}
</script>