<?php if(User::id()) {echo '<h6>вы уже зарегистрированы</h6></div></div>';exit;}?>
	<div class="pl-4 col-lg-8 col-md-9">
<?php //echo '<pre>';var_dump($view_data);echo '</pre>';?>
<?php if(!isset($view_data["action"])) {?>
	<h3 class="text-center">Регистрация</h3>
	<form class="form-horizontal" role="form" method="POST" action="/user/registration_action/" id="reg_form">
		<input type="hidden" id="val_user_r" name="val_user_r" value="val_user_r_disabled" />
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<h4><i class="fas fa-home"></i> Адрес</h4>
				<hr>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="form-group"><!--has-danger-->
					<label class="sr-only" for="user_name">Фамилия и Имя</label>
					<div class="input-group mb-2 mr-sm-2 mb-sm-0">
						<div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-user"></i></div>
						<input type="text" name="user_name" class="form-control" id="user_name" placeholder="Фамилия Имя" required autofocus value="" />
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-control-feedback">
					<span class="text-danger align-middle" style="display:none"><i class="fas fa-close"></i>Забыли представиться</span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="sr-only" for="user_telephone">Телефон</label>
						<div class="input-group mb-2 mr-sm-2 mb-sm-0">
							<div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-phone"></i></div>
							<input type="tel" name="user_telephone" class="form-control" id="user_telephone" placeholder="Телефон" required />
						</div>
					</div>
				</div>
			<div class="col-md-3">
				<div class="form-control-feedback">
					<span class="text-danger align-middle" style="display:none">Укажите номер телефона</span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="form-group">
					<label class="sr-only" for="user_email">E-mail</label>
						<div class="input-group mb-2 mr-sm-2 mb-sm-0">
							<div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-at"></i></div>
							<input type="email" name="user_email" class="form-control" id="user_email" placeholder="e-mail" required />
						</div>
					</div>
				</div>
			<div class="col-md-3">
				<div class="form-control-feedback">
					<span class="text-danger align-middle" style="display:none">Забыли e-mail</span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
					<div class="form-group">
						<label class="sr-only" for="user_address">Город, адрес</label>
						<div class="input-group mb-2 mr-sm-2 mb-sm-0">
							<div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-home"></i></div>
							<input type="text" name="user_address" class="form-control" id="user_address" placeholder="Город, адрес" required>
						</div>
					</div>
			</div>
			<div class="col-md-3">
					<div class="form-control-feedback">
						<span class="text-danger align-middle" style="display:none">Введите адрес пожалуйста</span>
					</div>
				</div>
			</div>

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6" style="padding-top: .35rem">
				<div class="form-check mb-2 mr-sm-2 mb-sm-0">
					<label class="form-check-label">
						<input class="form-check-input" id="checkBoxId" type="checkbox" />
						<span style="padding-bottom: .15rem">я не робот</span>
					</label>
				</div>
			</div>
		</div>

			<div class="row" style="padding-top: 1rem">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<button type="submit" id="form_submit" class="btn btn-success" OnClick="btn_submit()">Погнали! <i class="fas fa-arrow-right"></i></button>
				</div>
			</div>

	</form>

<script type="text/javascript">
$(function () {
	$("#user_name").val(getCookie('user_name'));
	$("#user_telephone").val(getCookie('user_telephone'));
	$("#user_email").val(getCookie('user_email'));
	$("#user_address").val(getCookie('user_address'));

	$('#checkBoxId').removeAttr("checked");
	$("#form_submit").attr('disabled','disabled');
	$('#val_user_r').val('val_user_r_disabled');

	$('#checkBoxId').change(function() {
		if($(this).is(':checked')){
			$("#form_submit").removeAttr('disabled');
			$('#val_user_r').val('val_user_r_true');
		}else {
			$("#form_submit").attr('disabled','disabled');
			$('#val_user_r').val('val_user_r_disabled');
		}
	});

});
function btn_submit(){
	setCookie('user_name', $("#user_name").val(), 1);
	setCookie('user_telephone', $("#user_telephone").val(), 1);
	setCookie('user_email', $("#user_email").val(), 1);
	setCookie('user_address', $("#user_address").val(), 1);

}
</script>
<?php }elseif($view_data["action"]=='check_email'){ ?>
	<div class="alert alert-success" role="alert">
		<h3 class="text-center"><?=$view_data["user_name"]?>, мы почти у цели.</h3>
	</div>
	<p>На email <strong><?=$view_data["user_email"]?></strong> сейчас придёт письмо для активации аккаунта. Если долго не приходит, проверьте папку "спам"</p>
<?php }elseif($view_data["action"]=='registration_error'){ ?>
	<div class="alert alert-danger" role="alert">
		<h5 class="text-center">Что-то пошло не так.</h5>
	</div>
<?php
	$errors=explode(';', $view_data["error"]);
	foreach($errors as $error){
		switch ($error){
			case 'name_length':
				echo '<span class="badge badge-danger">Ошибка в длине имени</span><br />';
				break;
			case 'email_length':
				echo '<span class="badge badge-danger">Ошибка в длине e-mail</span><br />';
				break;
			case 'tel_length':
				echo '<span class="badge badge-danger">ошибка в длине телефона</span><br />';
				break;
			case 'address_length':
				echo '<span class="badge badge-danger">ошибка в длине адреса</span><br />';
				break;
			case 'email_exist':
				echo '<span class="badge badge-danger">Этот почтовый ящик уже зарегистрирован.</span><br />';
				break;
			case 'email_wrong':
				echo '<span class="badge badge-danger">Некорректное имя почтового ящика.</span><br />';
				break;
			default: '<span class="badge badge-warning">Техническая ошибка.</span><br />';
		}
	}
?>
<div class="text-center">
	<a class="btn btn-success" href="/user/registration/" style="margin:2rem 0 1rem">Давайте попробуем ещё раз</a>
</div>
<?php }elseif($view_data["action"]=='activation_success'){ ?>
	<div class="alert alert-success" role="alert">
		<h4 class="text-center">Приехали!<span class="small"> можете входить</span></h4>
	</div>
<?php }elseif($view_data["action"]=='activation_error'){ ?>
	<div class="alert alert-danger" role="alert">
		<h6 class="text-center">Ошибка активации.</h6>
	</div>
	<p>Активировали ранее?</p>
<?php }?>

</div>