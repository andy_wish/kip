<?php if (!defined("INBOX")) die('separate call');

if(isset($_GET["auth_error"])){
	switch ($_GET["auth_error"]) {
		case 'login_error':
			$auth_error='Неправильные логин или пароль.';
			break;
		case 1:
			$auth_error='Ошибка ввода данных.';
			break;
		case 'logout_error':
			$auth_error='Ошибка выхода.';
			break;
		case 3:
			$auth_error='Ошибка. Подмена запроса.';
		default:
			$auth_error='Ошибка.';
	}
	echo '<span class="badge badge-danger">'.$auth_error.'</span>';
}

if(User::id()){
?>
<form action="/user/logout/" method="post" class="form-inline my-2 my-lg-0">
	<div class="input-group">
		<div class="input-group-prepend">
			<div class="input-group-text"><strong><?=User::name()?></strong>&nbsp;:&nbsp;<span class="text-muted"><?=User::group_name()?></span></div>
		</div>
		<button type="submit" class="btn btn-info btn-sm">выйти&nbsp;<i class="fas fa-sign-out-alt"></i></button>
	</div>
</form>

<?php 
}else{
?>
<div class="btn-group-sm" role="group">
	<button type="button" class="btn btn-secondary" id="login-trigger">Войти <span>&#x25BC;</span></button>
	<!--<a href="/admin/user/registration/" class="btn btn-secondary">Регистрация</a>-->
</div>
<div style="display:none;" id="login-content">
	<form action="/admin/user/login/" method="post">
		<input type="hidden" name="page_current" value="<?=$_SERVER['REQUEST_URI']?>" />
		<div class="form-group">
			<input name="user_email" id="user_email_login" type="email" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="email" required />
		</div>
		<div class="form-group">
			<input name="user_password" type="password" class="form-control form-control-sm" placeholder="Password" required />
		</div>
		<button type="submit" class="btn btn-primary btn-sm btn-block">вперёд <i class="fas fa-sign-in-alt"></i></button>
	</form>
</div>                     

<?php } ?>
<script type="text/javascript">
$(document).ready(function(){
	$('#login-trigger').click(function(){
		$('#login-content').slideToggle();
		$('#login-trigger').toggleClass('active');
		$('#user_email_login').focus();

		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
			else $(this).find('span').html('&#x25BC;')
		})
});
</script>