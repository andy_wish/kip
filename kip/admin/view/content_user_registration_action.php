<div class="container registration_box p-4">
	<div class="row">
		<div class="col-12">


<?php
//echo '<pre>';var_dump($var);echo '</pre>';

switch($var["action"]){
	case 'check_email':
?>
			<div class="alert alert-success" role="alert">
				<h1 class="text-center"><?=$var["reg_user"]["name"]?>, мы почти у цели.</h1>
			</div>
			<p>На <strong><?=$var["reg_user"]["email"]?></strong> летит письмо для активации аккаунта. Если долго не приходит, проверьте папку "спам"</p>
<?php 
		break;

	case 'registration_error':

		switch ($var["error"]){
			case 'name_length':
				echo '<span class="badge badge-danger">Ошибка в длине имени</span><br />';
				break;
			case 'email_length':
				echo '<span class="badge badge-danger">Ошибка в длине e-mail</span><br />';
				break;
			case 'tel_length':
				echo '<span class="badge badge-danger">ошибка в длине телефона</span><br />';
				break;
			case 'address_length':
				echo '<span class="badge badge-danger">ошибка в длине адреса</span><br />';
				break;
			case 'email_exist':
				echo '<span class="badge badge-danger">Этот почтовый ящик уже зарегистрирован.</span><br />';
				break;
			case 'email_wrong':
				echo '<span class="badge badge-danger">Некорректное имя почтового ящика.</span><br />';
				break;
			default: '<span class="badge badge-warning">Техническая ошибка.</span><br />';
		}
?>
			<div class="my-4">
				<a href="/admin/user/registration/">ещё раз</a>
			</div>
<?php 
		break;

	case 'activation_success':
?>
			<div class="alert alert-success" role="alert">
				<h4 class="text-center">Отлично!<span class="small"> Можете входить</span></h4>
			</div>
<?php
		break;

	case 'activation_error':
?>
			<div class="alert alert-danger" role="alert">
				<h6 class="text-center">Ошибка активации.</h6>
			</div>
			<p>Активировали ранее?</p>
<?php
		break;
}
?>


		</div>
	</div>
</div>