<ol class="breadcrumb breadcrumb_product">
	<li class="breadcrumb-item">
		<a href="//<?=$_SERVER['SERVER_NAME']?>/admin/">
			<span><?=Lang::str('Главная')?></span>
		</a>
	</li>
<?php
foreach($var["breadcrumb"] as $b){
?>
	<li class="breadcrumb-item">
		<a href="<?=$b["url"]?>">
			<span><?=Lang::str($b["name"])?></span>
		</a>
	</li>
<?php
}
?>
	<li class="breadcrumb-item active">
		<span><?=Lang::str($var["page"]["h1"])?></span>
	</li>
</ol>