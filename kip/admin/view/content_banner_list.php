<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/admin/">Главная</a></li>
				<li class="breadcrumb-item active">Баннеры</li>
			</ol>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-12">
			<div class="form-group">
				<label for="banner_create_file" style="cursor:pointer"><h5><i class="far fa-plus-square"></i> добавить</h5></label>
				<input type="file" onChange="banner_create(this.files)" accept="<?=Core::config('banner_upload_allowed')?>" class="form-control-file" id="banner_create_file" />
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">

<?php
//echo '<pre>';var_dump($var["banner"]);//exit;
if($var["banner"]["total"]>0){
?>
<div class="row justify-content-center">
	<div class="col-xl-11 col-lg-12">
		<table class="table table-sm table-striped table-bordered" id="banners">
			<thead>
				<tr>
					<th></th>
					<th>№</th>
					<th>Название</th>
					<th>Описание</th>
					<th>ссылка</th>
					<th>статус</th>
				</tr>
			</thead>
<?php
	foreach($var["banner"]["list"] as $row){

		if($row["closed"]=='') {
			$active_mark='';
			$status_btn='<button id="status_btn_'.$row["id"].'" class="btn btn-sm btn-success py-0 px-1" title="отключить" onClick="banner_update('.$row["id"].', \'status\', 0)"><i class="fas fa-toggle-on"></i></button>';
		}else{
			$active_mark='text-muted';
			$status_btn='<button id="status_btn_'.$row["id"].'" class="btn btn-sm btn-danger py-0 px-1" title="включить" onClick="banner_update('.$row["id"].', \'status\', 1)"><i class="fas fa-toggle-off"></i></a>';
		}
?>
			<tr class="<?=$active_mark?>">
				<td style="width:12rem">
					<img src="/files/image/<?=$row["img"]?>" class="rounded event_img img-fluid" id="banner_<?=$row["id"]?>_image" />
					<div class="form-group">
						<input type="file" onChange="banner_update_image(<?=$row["id"]?>, this.files, '<?=$row["img"]?>')" accept="<?=Core::config('banner_upload_allowed')?>" class="form-control-file" />
					</div>
				</td>
				<td style="width:2rem">
					<input class="form-control" type="number" min="1" max="100" value="<?=$row["order_num"]?>" id="banner_<?=$row["id"]?>_order" style="width:4.5rem" onChange="banner_update(<?=$row["id"]?>, 'order_num', this.value)" />
				</td>
				<td style="width:15rem">
					<textarea class="form-control" id="banner_<?=$row["id"]?>_name" onKeyUp="banner_update(<?=$row["id"]?>, 'name', this.value)" style="width:100%;height:10rem"><?=isset($row["name"])? $row["name"]: ''?></textarea>
				</td>
				<td style="width:15rem">
					<textarea class="form-control" id="banner_<?=$row["id"]?>_content" onKeyUp="banner_update(<?=$row["id"]?>, 'content', this.value)" style="width:100%;height:10rem"><?=isset($row["content"])? $row["content"]: ''?></textarea>
				</td>
				<td>
					<input class="form-control" type="text" id="banner_<?=$row["id"]?>_link" onKeyUp="banner_update(<?=$row["id"]?>, 'link', this.value)" style="width:100%;" value="<?=isset($row["target"])? $row["target"]: ''?>" />
				</td>
				<td style="width:2rem">
					<?=$status_btn?>
				</td>
			</tr>
<?php
	}
?>
		</table>
	</div>
</div>

<?php
}else{
?>
<div class="row justify-content-center">
	<div class="сol-12 my-2"><p>не найдено</p></div>
</div>
<?php
}
?>

</div>

<script type="text/javascript">

function banner_update(id, target, val){
	//var val=$('#banner_'+id+'_'+target).val();
	//var params={"type": 'banner_index', "id": parseInt(id)};
	var params={"id": parseInt(id)};

	switch(target){
		case 'order_num':
			params["order_num"]=parseInt(val);
			break;
		case 'name':
			params["name"]=val;
			break;
		case 'content':
			params["content"]=val;
			break;
		case 'link':
			params["target"]=val;
			break;
		case 'status':
			if($('#status_btn_'+id).hasClass('btn-success')){
				$('#status_btn_'+id).removeClass('btn-success').addClass('btn-danger');
				params["closed"]=1;
			}else{
				$('#status_btn_'+id).removeClass('btn-danger').addClass('btn-success');
				params["closed"]=0;
			}
			break;
		default:
			return;
	}
	jsonrpc_request("basis.update", params, 1, true);

}

function banner_create(files){
	if(typeof files=='undefined') return;

	var data=new FormData();
	$.each(files, function(key, value){
		data.append(key, value);
	});
	data.append('banner_mode', 1);

	$.ajax({
		url			: '/admin/file/create_ajax',
		type		: 'POST',
		data		: data,
		cache		: false,
		dataType	: 'json',
		processData	: false,
		contentType	: false,
		success     : function(respond, status, jqXHR ){
			if(typeof respond.error === 'undefined' ){

				var params={"id": parseInt(respond.id)};
				var answer=jsonrpc_request("banner.read", params);
				var banner=answer["result"]["list"][0];

				$('#banners').append(
					$('<tr>').append(
						$('<td style="width:12rem">').append(
							$('<img src="/files/image/'+banner.image+'" class="rounded event_img img-fluid" id="banner_'+banner.id+'_image" />'),
							$('<div class="form-group">').append(
								$('<input type="file" onChange="banner_update_image('+banner.id+', this.files)" accept="<?=Core::config('banner_upload_allowed')?>" class="form-control-file" />')
							)
						),
						$('<td style="width:2rem">').append(
							$('<input class="form-control" type="number" min="1" max="100" value="'+banner.order+'" id="banner_'+banner.id+'_order" style="width:4.5rem" onChange="banner_update('+banner.id+', \'order\', this.value)" />')
						),
						$('<td style="width:15rem">').append(
							$('<textarea class="form-control" id="banner_'+banner.id+'_name" onKeyUp="banner_update('+banner.id+', \'name\', this.value)" style="width:100%;height:10rem">'+banner.name+'</textarea>')
						),
						$('<td style="width:15rem">').append(
							$('<textarea class="form-control" id="banner_'+banner.id+'_content" onKeyUp="banner_update('+banner.id+', \'content\', this.value)" style="width:100%;height:10rem">'+banner.content+'</textarea>')
						),
						$('<td>').append(
							$('<input class="form-control" type="text" id="banner_'+banner.id+'_link" onKeyUp="banner_update('+banner.id+', \'link\', this.value)" style="width:100%;" value="'+banner.target+'" />')
						),
						$('<td style="width:2rem">').append(
							$('<button id="status_btn_'+banner.id+'" class="btn btn-sm btn-success py-0 px-1" title="отключить" onClick="banner_update('+banner.id+', \'status\', 0)"><i class="fas fa-toggle-on"></i></button>')
						),
					)
				);

			}else console.log('AJAX ERROR: '+respond.error);
		},
		error: function(jqXHR, status, errorThrown){
			console.log('AJAX REQUEST ERROR: '+status, jqXHR);
		}
	});
}

function banner_update_image(id, files){
	wait_start();

	if(typeof files=='undefined') return;

	var data=new FormData();
	$.each(files, function(key, value){
		data.append(key, value);
	});
	
	var answer=jsonrpc_request("banner.read", {"id": parseInt(id)});
	var image_old=answer["result"]["list"][0]["image"];

	data.append('banner_image_change_mode', 1);
	data.append('banner_id', id);
	data.append('image_old', image_old);
	$.ajax({
		url			: '/admin/file/create_ajax',
		type		: 'POST',
		data		: data,
		cache		: false,
		dataType	: 'json',
		processData	: false,
		contentType	: false,
		success     : function(respond, status, jqXHR ){
			if(typeof respond.error === 'undefined' ){

				var params={"id": parseInt(respond.id)};
				var answer=jsonrpc_request("banner.read", params);
				var banner=answer["result"]["list"][0];

				$('#banner_'+respond.id+'_image').attr('src', '/files/image/'+banner.image);

			}else console.log('AJAX ERROR: '+respond.error);
		},
		error: function(jqXHR, status, errorThrown){
			console.log('AJAX REQUEST ERROR: '+status, jqXHR);
		}
	});

	wait_finish();

}
</script>