<div class="btn-group-sm ml-3" role="group">
	<div class="btn-group" role="group">
<?php
$lang=[
	"current"=>Lang::current_lang(),
	"list"=>Lang::listing()
];
foreach($lang["list"] as $l=>$vars){
	if($l==$lang["current"]){
?>
		<button class="btn btn-light btn-sm active">
			<span><?=$l?></span>
		</button>
<?php
	}else{
?>
		<button class="btn btn-light btn-sm" onClick="language_change('<?=$l?>')">
			<span><?=$l?></span>
		</button>
<?php
	}
}
?>
	</div>
</div>


<script type="text/javascript">
function language_change(l){
	setCookie('lang', l, 8760)
	//document.location.reload(true)
	window.location.href = '/admin/';
}
</script>