<div class="container px-md-5 px-sm-3 px-xs-0">
<?php include 'admin/view/block_breadcrumb.php';?>

<div class="row">


	<div class="col-lg-10 col-md-12 mb-3">
		<form class="col-12" action="/" method="post">
			<div class="form-group">
				<label><?=$var["product"]["created_nice"]?></label>
				<input type="text" class="form-control" placeholder="название, обязательно" name="name" value="<?=$var["product"]["name"]?>" />
			</div>
			<div class="form-group">
				<button class="btn btn-success" type="submit">Сохранить</button>
			</div>
		</form>
	</div>


	<div class="col-lg-10 col-md-12 mb-3">
		<table id="infoboxes" class="table table-sm table-striped table-bordered">
			<thead>
				<tr>
					<th></th>
					<th>Название</th>
					<th></th>
				</tr>
			</thead>
<?php
	if(isset($var["product"]["infoboxes"])){

		foreach($var["product"]["infoboxes"] as $row){
?>
				<tr id="infobox_<?=$row["id"]?>">
					<td>
						<input class="form-control" type="number" min="1" max="100" value="<?=$row["order_num"]?>" id="infobox_<?=$row["id"]?>_order_num" style="width:4.5rem" onChange="infobox_update(<?=$row["id"]?>, 'order_num')" />
					</td>
					<td title="редактировать" onClick="infobox_draw(<?=$row["id"]?>, '<?=$var["product"]["name"]?>')">
						<button class="btn btn-secondary btn-block text-left"><i class="far fa-edit"></i>&nbsp;<?=$row["lang"]?>&emsp;<?=$row["name"]?></button>
					</td>
					<td title="Удалить"><button class="btn btn-sm btn-danger py-0 px-1" onClick="infobox_delete(<?=$row["id"]?>)"><i class="fas fa-trash-alt"></i></button></td>
				</tr>
<?php
		}
	}
?>
		</table>
	</div>


	<div class="col-lg-10 col-md-12">
		<h3>Файлы</h3>
<?php
if($var["product"]["id"]){
?>
		<div class="form-group">
			<label for="files_input">Добавить файлы</label>
			<input type="file" onChange="file_create(this.files)" multiple="multiple" accept="<?=Core::config('product_upload_allowed')?>" class="form-control-file" id="files_input" />
		</div>
		<hr />

		<table id="files" class="table table-sm table-striped table-bordered">
			<thead>
				<tr>
					<th></th>
					<th>Превью</th>
					<th>на диске</th>
					<th>Название</th>
					<th>Описание</th>
					<th></th>
				</tr>
			</thead>
<?php
	if(isset($var["product"]["files"]["image"])){
		foreach($var["product"]["files"]["image"] as $row){
?>
				<tr id="file_<?=$row["id"]?>">
					<td>
						<input class="form-control" type="number" min="1" max="100" value="<?=$row["order_num"]?>" id="file_<?=$row["id"]?>_order_num" style="width:4.5rem" onChange="file_update(<?=$row["id"]?>, 'order_num')" />
					</td>
					<td><a href="/files/image/<?=$row["img"]?>"><img src="/files/image/<?=$row["img"]?>" width="100" class="rounded" /></a></td>
					<td><span><?=$row["img"]?></span><br /><span class="text-muted"></span></td>
					<td title="<?=$row["id"]?>">
						<input class="form-control" type="text" value="<?=isset($row["name"])? $row["name"]: ''?>" id="file_<?=$row["id"]?>_name" onKeyUp="file_update(<?=$row["id"]?>, 'name')" />
					</td>
					<td>
						<input class="form-control" type="text" value="<?=isset($row["desc"])? $row["desc"] : ''?>" id="file_<?=$row["id"]?>_desc" onKeyUp="file_update(<?=$row["id"]?>, 'desc')" />
					</td>
					<td title="Удалить"><button class="btn btn-sm btn-danger py-0 px-1" onClick="file_delete(<?=$row["id"]?>, '<?=$row["img"]?>', 'image')"><i class="fas fa-trash-alt"></i></button></td>
				</tr>
<?php
		}
	}
	if(isset($var["product"]["files"]["video"])){
?>
		<tr><td colspan="6" class="text-center"><h5>Видео:</h5></td></tr>
<?php
		foreach($var["product"]["files"]["video"] as $row){
?>
				<tr id="file_<?=$row["id"]?>">
					<td>
						<input class="form-control" type="number" min="1" max="100" value="<?=$row["order_num"]?>" id="file_<?=$row["id"]?>_order_num" style="width:4.5rem" onChange="file_update(<?=$row["id"]?>, 'order_num')" />
					</td>
					<td></td>
					<td><span><?=$row["img"]?></span></td>
					<td title="<?=$row["id"]?>">
						<input class="form-control" type="text" value="<?=isset($row["name"])? $row["name"]: ''?>" id="file_<?=$row["id"]?>_name" onKeyUp="file_update(<?=$row["id"]?>, 'name')" />
					</td>
					<td>
						<input class="form-control" type="text" value="<?=isset($row["desc"])? $row["desc"] : ''?>" id="file_<?=$row["id"]?>_desc" onKeyUp="file_update(<?=$row["id"]?>, 'desc')" />
					</td>
					<td title="Удалить"><button class="btn btn-sm btn-danger py-0 px-1" onClick="file_delete(<?=$row["id"]?>, '<?=$row["img"]?>', 'youtube')"><i class="fas fa-trash-alt"></i></button></td>
				</tr>
<?php
		}
	}
?>
		</table>

		<script type="text/javascript">
var product_id=<?=$var["product"]["id"]?>

function file_update(id, target){

	//console.log(id, target);
	
	var val=$('#file_'+id+'_'+target).val();
	//console.log(val);

	var data = new FormData();
	data.append('id', parseInt(id));
	data.append(target, val);

	$.ajax({
		url         : '/admin/file/update_ajax/',
		type        : 'POST',
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса это строковой запрос
		contentType : false, 
		success     : function( respond, status, jqXHR ){
		},
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});
}

function file_delete(id, img, type){
	if(confirm("Удалить?")){

		var data = new FormData();
		data.append('id', parseInt(id));
		data.append('type', type);
		data.append('img', img);
		$.ajax({
			url         : '/admin/file/delete_ajax/',
			type        : 'POST',
			data        : data,
			cache       : false,
			dataType    : 'json',
			processData : false,
			contentType : false, 
			success     : function(respond, status, jqXHR ){
				if(typeof respond.error === 'undefined' ){
					$('#file_'+respond.id).remove();
				}else console.log('AJAX ERROR: '+respond.error);
			},
			error: function(jqXHR, status, errorThrown){
				console.log('AJAX REQUEST ERROR: '+status, jqXHR);
			}

		});
	}
}

function file_create(files){

	if(typeof files=='undefined') return;

	var data=new FormData();
	$.each(files, function(key, value){
		data.append(key, value);
	});
	data.append('product_id', product_id);

	$.ajax({
		url			: '/admin/file/create_ajax',
		type		: 'POST',
		data		: data,
		cache		: false,
		dataType	: 'json',
		processData	: false,
		contentType	: false,
		success     : function(respond, status, jqXHR ){
			if(typeof respond.error === 'undefined' ){
				//$('#file_'+respond.id).remove();
				$.each(respond.list, function(key, value){
					var preview='';

					if(this["type"]=='image') preview='<a href="/files/'+this["type"]+'/'+this["img"]+'"><img src="/files/'+this["type"]+'/'+this["img"]+'" width="100" class="rounded" /></a>';
					else preview='<a class="btn btn-secondary btn-lg" href="/files/'+this["type"]+'/'+this["img"]+'"><i class="far fa-file-'+this["type"]+'"></i></a>';

					$('#files').append(
						$('<tr id="file_'+this["id"]+'">').append(
							$('<td>').append(
								$('<input class="form-control" type="number" min="1" max="100" value="5" id="file_'+this["id"]+'_order" style="width:4.5rem" onChange="file_update('+this["id"]+', \'order\')" />')
							),
							$('<td title="id: '+this["id"]+'">').append(
								$(preview)
							),
							$('<td title="'+this["id"]+'">').append(
								$('<span>'+this["img"]+'</span><br /><span class="text-muted">'+this["type"]+',</span>&nbsp;<span class="text-muted">'+this["size_nice"]+'</span>')
							),
							$('<td>').append(
								$('<input class="form-control" type="text" value="" id="file_'+this["id"]+'_name" onKeyUp="file_update('+this["id"]+', \'name\')" />')
							),
							$('<td title="'+this["id"]+'">').append(
								$('<input class="form-control" type="text" value="" id="file_'+this["id"]+'_desc" onKeyUp="file_update('+this["id"]+', \'desc\')" />')
							),
							$('<td title="Удалить">').append(
								$('<button class="btn btn-sm btn-danger py-0 px-1" onClick="file_delete('+this["id"]+', \''+this["img"]+'\', \''+this["type"]+'\')"><i class="fas fa-trash-alt"></i></button>')
							),
						)
					);
				});
			}else console.log('AJAX ERROR: '+respond.error);
		},
		error: function(jqXHR, status, errorThrown){
			console.log('AJAX REQUEST ERROR: '+status, jqXHR);
		}
	});

};



</script>
<?php
}else{
?>
	<p>Для добавления файлов, сохраните продукт</p>
<?php
}
?>
	</div>
</div>

<?php //echo '<pre>';var_dump($var["products"]);echo '</pre><hr />';?>

</div>
<script>
var editor;

function infobox_draw(id, product_name){
	var answer=basis_read({"id": id, "lang": 'all'})
	if(typeof(answer["error"])=="undefined" && answer["list"][0]["id"]==id) {
		var row=answer["list"][0];
	}else return false;

	modal_clear('top');
	$('#top_modal_header').append(
		$('<h5><small class="text-muted">Изменить</small> '+product_name+' / '+row["name"]+'</h5>'),
	);

	$('#top_modal_body').append(
		$('<form>').append(
			$('<div class="form-group m-1">').append(
				$('<small class="text-muted">Название*</small>'),
				$('<input type="text" class="form-control" id="name" value="'+row["name"]+'" required />'),
			),
			$('<div class="form-group m-1" id="text">').append(
				$('<textarea id="ckeditor">'+row["content"]+'</textarea>')
			),
		),
		$('<div id="modal_info_alert" class="alert" role="alert">'),
	);
	
	$('#top_modal_footer').append(
		$('<div class="btn-group" role="group" aria-label="row_info_buttons">').append(
			$('<button class="btn btn-success btn-block" OnClick="infobox_update_submit('+id+')">Сохранить</button>'),
			$('<button class="btn btn-danger" OnClick="modal_close(\'top\');">Отмена</button>'),
		),
	);
	modal_show('top')
	ClassicEditor
		.create(document.querySelector('#ckeditor'), {
			//toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
		} )
		.then(editor=>{
			window.editor=editor;
		})
		.catch(error =>{console.error( error );});

}
function infobox_update_submit(id) {
	var name=$("#name").val();
	var content=editor.getData();

	if(name=='') {
		$('#modal_info_alert').text('Название обязательно для заполнения');
		return false;
	}

	var params={
		"id": id,
		"name": name
	};

	if(content!='') params["content"]=content
	
	answer = basis_update(params);
	
	console.log(answer)
	if(answer["id"]==id){
		modal_close("top");
		location.reload();
	}
	return true;
}
</script>