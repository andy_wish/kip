<div class="container px-md-5 px-sm-3 px-xs-0">

<?php include 'admin/view/block_breadcrumb.php';?>

<div class="row justify-content-center">
	<div class="col-12">
		<h1>
			<button class="btn btn-secondary" onClick="basis_draw('category', 'create', {id_parent:<?=$var["basis"]["id_parent"]?>})">
				<i class="far fa-plus-square"></i>
			</button>
			<?=$var["page"]["h1"]?>
		</h1>
	</div>
</div>

<div class="row justify-content-center">
	<div class="col-xl-8 col-lg-9 col-md-12">
		<table class="table table-sm table-striped table-bordered">
			<thead>
				<tr>
					<th>№</th>
					<th></th>
					<th>Название</th>
					<th></th>
					<th>дата</th>
					<th>статус</th>
				</tr>
			</thead>
<?php
//echo '<pre>';var_dump($var["basis"]);echo '</pre><hr />';
if($var["basis"]["total"]>0){

	foreach($var["basis"]["list"] as $row){

		if($row["img"]!='')$img_src='/files/image/'.$row["img"];
		else $img_src='/admin/img/no-img.png';

		if($row["closed"]=='') {
			$active_mark='';
			$status_btn='<button id="status_btn_'.$row["id"].'" class="btn btn-sm btn-success py-0 px-1" title="вкл-выкл" onClick="basis_update('.$row["id"].', \'status\', 0)"><i class="fas fa-toggle-on"></i></button>';
		}else{
			$active_mark=' text-muted';
			$status_btn='<button id="status_btn_'.$row["id"].'" class="btn btn-sm btn-danger py-0 px-1" title="вкл-выкл" onClick="basis_update('.$row["id"].', \'status\', 1)"><i class="fas fa-toggle-off"></i></button>';
		}

?>
			<tr class="<?=$active_mark?>">
				<td style="width:2rem">
					<input class="form-control" type="number" min="1" max="100" value="<?=$row["order_num"]?>" id="basis_<?=$row["id"]?>_order" style="width:4.5rem" onChange="basis_update(<?=$row["id"]?>, 'order_num', this.value)" />
				</td>
				<td>
					<img src="<?=$img_src?>" alt="<?=$row["name"]?>" class="rounded event_img" style="width:5rem"/>
					
					<div class="form-group">
						<input type="file" onChange="basis_update_image11(<?=$row["id"]?>, this.files, '<?=$row["img"]?>')" accept="<?=Core::config('banner_upload_allowed')?>" class="form-control-file" />
					</div>
				</td>
				<td>
					<a href="/admin/category/<?=$row["id"]?>" class="btn btn-secondary btn-block text-left"><?=$row["name"]?>&nbsp;[<?=$row["id"]?>]</a>
					<span class="badge badge-secondary"><?=$row["child_num"]?></span>
				</td>
				<td><button class="btn btn-secondary" onClick="basis_draw('category', 'update', {id:<?=$row["id"]?>})"><i class="far fa-edit"></i></button></td>
				<td>
					<?=$row["created_nice"]?>
				</td>
				<td>
					<?=$status_btn?>
				</td>
			</tr>
<?php
	}
}else{
?>
<div class="row justify-content-center">
	<div class="сol-12 my-2"><p>не найдены</p></div>
</div>
<?php
}
?>
		</table>
	</div>
</div>

<?php if($var["pagination"]["current"]) include 'admin/view/block_pagination.php';?>

</div>