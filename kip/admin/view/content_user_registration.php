<?php if (!defined("INBOX")) die('separate call');?>

<?php
if(User::id()) {
?>
	<h1 class="m-4">вы уже зарегистрированы</h1>
<?php
	exit;
}
?>

<div class="container registration_box">
	<div class="row">
		<div class="col-12">
			<h1 class="m-4">Регистрация</h1>
			<form class="form-horizontal" role="form" method="POST" action="/admin/user/registration_action/" id="reg_form">
				<input type="hidden" id="val_user_r" name="val_user_r" value="val_user_r_disabled" />

				<div class="row">
					<div class="col-md-9">
						<label class="sr-only" for="user_name">Имя*</label>
						<div class="input-group mb-2">
							<div class="input-group-prepend">
								<div class="input-group-text"><i class="fas fa-user"></i></div>
							</div>
							<input type="text" name="user_name" class="form-control" id="user_name" placeholder="Имя*  отображается на сайте" required autofocus value="" />
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-control-feedback">
							<span class="text-danger align-middle" style="display:none"><i class="fas fa-close"></i>Забыли представиться</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-9">
						<div class="form-group">
							<label class="sr-only" for="user_email">E-mail*</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fas fa-at"></i></div>
								</div>
								<input type="email" name="user_email" class="form-control" id="user_email" placeholder="e-mail*  используется при входе в систему" required />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-control-feedback">
							<span class="text-danger align-middle" style="display:none">Забыли e-mail</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-9">
						<div class="form-group">
							<label class="sr-only" for="user_password">Пароль*</label>
							<div class="input-group mr-sm-2 mb-sm-0">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fas fa-key"></i></div>
								</div>
								<input type="password" name="user_password" class="form-control" id="user_password" placeholder="пароль*  от 8 до 16 символов" required />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-control-feedback">
							<span class="text-danger align-middle" style="display:none">Забыли пароль</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-9">
						<div class="form-group">
							<label class="sr-only" for="user_password2">Повторите пароль*</label>
							<div class="input-group mr-sm-2 mb-sm-0">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fas fa-key"></i></div>
								</div>
								<input type="password" name="user_password2" class="form-control" id="user_password2" placeholder="повторите пароль*" required />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-control-feedback">
							<span class="text-danger align-middle" style="display:none">Забыли пароль</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-9">
						<div class="form-group">
							<label class="sr-only" for="user_telephone">Телефон</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-prepend">
									<div class="input-group-text"><i class="fas fa-phone"></i></div>
								</div>
								<input type="tel" name="user_telephone" class="form-control" id="user_telephone" placeholder="Телефон, не обязательно" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-control-feedback">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-9 m-2">
						<div class="form-check mb-2 mr-sm-2 mb-sm-0">
							<label class="form-check-label">
								<input class="form-check-input" id="checkBoxId" type="checkbox" />
								<span style="padding-bottom: .15rem">Даю согласие на обработку моих <a href="https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0_%D0%BF%D0%B5%D1%80%D1%81%D0%BE%D0%BD%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D1%85_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85">персональных данных</a></span>
							</label>
						</div>
					</div>
				</div>

				<div class="row mt-2">
					<div class="col-md-9">
						<button type="submit" id="form_submit" class="btn btn-success" OnClick="btn_submit()"><i class="far fa-check-circle"></i> Хорошо</button>
					</div>
				</div>

			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function () {

	$("#user_name").val(getCookie('user_name'));
	$("#user_telephone").val(getCookie('user_telephone'));
	$("#user_email").val(getCookie('user_email'));

	$('#checkBoxId').removeAttr("checked");
	$("#form_submit").attr('disabled','disabled');
	$('#val_user_r').val('val_user_r_disabled');

	$('#checkBoxId').change(function() {
		if($(this).is(':checked')){
			$("#form_submit").removeAttr('disabled');
			$('#val_user_r').val('val_user_r_true');
		}else {
			$("#form_submit").attr('disabled','disabled');
			$('#val_user_r').val('val_user_r_disabled');
		}
	});

});
function btn_submit(){
	setCookie('user_name', $("#user_name").val(), 1);
	setCookie('user_telephone', $("#user_telephone").val(), 1);
	setCookie('user_email', $("#user_email").val(), 1);
}
</script>