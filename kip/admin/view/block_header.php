<?php if (!defined("INBOX")) die('separate call');?>
<!doctype html><html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Панель администрирования">
	<meta name="author" content="umdi.ru">
	<link rel="icon" href="/images/favicon/favicon.ico">

	<title><?=isset($var["page"]["title"])? 'LeadBox. '.$var["page"]["title"] : 'LeadBox'?></title>

	<link href="/admin/plugin/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="/admin/plugin/fontawesome/css/fontawesome-all.css" rel="stylesheet" type="text/css">
	<link href="/admin/css/style.css" rel="stylesheet" type="text/css">

	<script src="/admin/plugin/jquery-3.4.1.min.js" type="text/javascript"></script>
	<script src="/admin/plugin/handlebars.min-v4.1.2.js" type="text/javascript"></script>
	<script src="/admin/plugin/bootstrap/bootstrap.min.js" type="text/javascript"></script>
	<script src="/admin/plugin/ckeditor/ckeditor.js" type="text/javascript"></script>
	<script src="/admin/plugin/jsonrpc.js" type="text/javascript"></script>
	<script src="/admin/js/basis.js" type="text/javascript"></script>
	<script src="/admin/js/core.js" type="text/javascript"></script>
</head>

<body>
<header>
	<nav class="navbar navbar-dark navbar-expand-md px-md-5 px-sm-3 px-xs-1" style="background:#16638b;background:linear-gradient(to top, #16b, #38d);font-size:120%">
		<a class="navbar-brand" href="/">LeadBox</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto nav nav-pills">
<?php
$nav=[
	"index"=>[
		"name"=>'Главная',
		"url"=>'/admin/',
	],
	"category"=>[
		"name"=>'Категории',
		"url"=>'/admin/category/'
	],
	"banner"=>[
		"name"=>'Баннеры',
		"url"=>'/admin/banner/'
	],
];
foreach($nav as $current=>$val){
	if($var["page"]["current"]==$current) $active_mark=' active';
	else $active_mark='';
?>
				<li class="nav-item<?=$active_mark?>">
					<a class="nav-link" href="<?=$val["url"]?>"><?=$val["name"]?></a>
				</li>
<?php
}
?>
			<!--<li class="nav-item">
					<span class="badge badge-secondary" id="search_count_box" style="float:right"></span>
					<button OnClick="search_query_delete()" id="search_top_query_delete_btn" class="btn btn-sm btn-secondary p-0 px-1 mr-1 mt-1" style="float:right;margin-left:-0.4rem;display:none" title="убрать поисковую строку"><i class="fas fa-window-close"></i></button>
					<input id="search_box" class="form-control form-control-sm mr-sm-2" type="text" autocomplete="off" placeholder="поиск" value="" maxlength="20" style="width:23rem" OnKeyUp="search_top_main(event.keyCode)" OnClick="search_top_main('click')" />
					<table class="table table-sm table-striped" id="search_list"></table>
				</li>-->
			</ul>

			<?php require 'block_user_login.php';?>
			<?php require 'block_lang_change.php';?>

		</div>
	</nav>
<?php
if(isset($var["success"])){//success
?>
	<div class="alert alert-success alert-dismissible fade show text-center" role="alert">
		<?=$var["success"]?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="far fa-times-circle" aria-hidden="true"></i>
		</button>
	</div>
<?php
}
if(isset($var["error"])){//error
?>
	<div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
		<?=$var["error"]?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="far fa-times-circle" aria-hidden="true"></i>
		</button>
	</div>
<?php
}
if(isset($var["warning"])){//warning
?>
	<div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
		<?=$var["warning"]?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<i class="far fa-times-circle" aria-hidden="true"></i>
		</button>
	</div>
<?php
}
?>
</header>

<!--MODAL-->
<div class="modal fade" id="top_modal" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog modal_box" role="document"><div class="modal-content" id="top_modal_box"><button class="btn btn-sm btn-secondary m-1" style="position:absolute;right:0;" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button><div class="modal-header pr-5" id="top_modal_header"></div><div class="modal-body" id="top_modal_body"></div><div class="modal-footer" id="top_modal_footer"></div></div></div></div>

<!--wait_notify-->
<div id="wait_notify" style="display:none;position:fixed;top:90%;left:90%;z-index:9999;opacity:.5;"><i class="fas fa-cog fa-5x fa-spin"></i></div>