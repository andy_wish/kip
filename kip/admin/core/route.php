<?php if (!defined("INBOX")) die('separate call');

class Route {

	static function start($var) {

		$routes=explode('?', $_SERVER['REQUEST_URI']);
		$var["page"]["url"]=$routes[0];
		$routes=explode('/', $routes[0]);

		if (!empty($routes[2])) {
			$controller=$routes[2];
			if (isset($routes[3]) AND $routes[3]!='') {
				$var[0]=$routes[3];
				if (isset($routes[4]) AND $routes[4]!='') {
					$var[1]=$routes[4];
					if (isset($routes[5]) AND $routes[5]!='') {
						$var[2]=$routes[5];
						if (isset($routes[6]) AND $routes[6]!='') {
							$var[3]=$routes[6];
							if (isset($routes[7]) AND $routes[7]!='') {
								$var[4]=$routes[7];
							}
						}
					}
				}
			}
		}else $controller='index';

		$var["page"]["controller"]=$controller;

		$controller_path = 'admin/pack/'.strtolower($controller).'_controller.php';
		if(file_exists($controller_path)) include $controller_path;
		else Core::error('404');

		$model_path = 'admin/pack/'.strtolower($controller).'_model.php';
		if(file_exists($model_path)) include $model_path;

		$controller_name=$controller.'_controller';
		$controller=new $controller_name($var);
	}

}