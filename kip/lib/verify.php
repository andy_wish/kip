<?php if (!defined("INBOX")) die('separate call');

Verify::instance();

class Verify {

	const DEFAULT_INT_MIN=1;
	const DEFAULT_INT_MAX=18446744073709551615;
	const DEFAULT_STRING_MIN=1;
	const DEFAULT_STRING_MAX=16777215;
	const DEFAULT_PHONE_MIN=6;
	const DEFAULT_PHONE_MAX=16;
	const DEFAULT_LOGIN_MIN=3;
	const DEFAULT_LOGIN_MAX=32;
	const DEFAULT_PASSWORD_MIN=8;
	const DEFAULT_PASSWORD_MAX=64;
	const DEFAULT_EMAIL_MIN=3;
	const DEFAULT_EMAIL_MAX=64;

	static private $html_purifier;

	protected static $_instance;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){
		
		//HTMLpurifier
		require ROOT_DIR.'/app/plugin/htmlpurifier/library/HTMLPurifier.auto.php';
		$config = HTMLPurifier_Config::createDefault();
		self::$html_purifier = new HTMLPurifier($config);
	}

	public static function instance() {
		if (self::$_instance === null) {
			self::$_instance = new self;
		}
	}

	public static function valid_int($val, $min=DEFAULT_INT_MIN, $max=DEFAULT_INT_MAX){
		if(is_array($val)) return false;
		$val=intval($val);
		if($val<$min OR $val>$max) return false;
		return $val;
	}

	public static function valid_string($val, $min=self::DEFAULT_STRING_MIN, $max=self::DEFAULT_STRING_MAX){
		if(is_array($val)) return false;
		$val=trim(htmlspecialchars(strip_tags($val), ENT_QUOTES));
		if(mb_strlen($val, 'utf-8')<$min OR mb_strlen($val, 'utf-8')>$max) return false;
		return $val;
	}

	public static function valid_html($val, $min=self::DEFAULT_HTML_MIN, $max=self::DEFAULT_HTML_MAX){
		if(is_array($val)) return false;
		$val=trim(self::$html_purifier->purify($val));
		if(mb_strlen($val, 'utf-8')<$min OR mb_strlen($val, 'utf-8')>$max) return false;
		return $val;
	}

	public static function valid_email($val, $min=self::DEFAULT_EMAIL_MIN, $max=self::DEFAULT_EMAIL_MAX){
		if(is_array($val)) return false;
		$val=trim($val);
		if(mb_strlen($val, 'utf-8')<$min OR mb_strlen($val, 'utf-8')>$max) return false;
		$p=strpos($val, '@');
		if($p===false) return false;
		return $val;
	}

	public static function valid_phone($val, $min=self::DEFAULT_PHONE_MIN, $max=self::DEFAULT_PHONE_MAX){
		if(is_array($val)) return false;
		$val=trim(htmlspecialchars(strip_tags($val), ENT_QUOTES));
		if(mb_strlen($val, 'utf-8')<$min OR mb_strlen($val, 'utf-8')>$max) return false;
		return $val;
	}

	public static function valid_login($val, $min=self::DEFAULT_LOGIN_MIN, $max=self::DEFAULT_LOGIN_MAX){
		if(is_array($val)) return false;
		$val=trim(htmlspecialchars(strip_tags($val), ENT_QUOTES));
		if(mb_strlen($val, 'utf-8')<$min OR mb_strlen($val, 'utf-8')>$max) return false;
		if (!preg_match("/^[a-z0-9][a-z0-9-_]+[a-z0-9]$/is", $val)) return false;
		return $val;
	}

	public static function valid_password($val, $min=self::DEFAULT_PASSWORD_MIN, $max=self::DEFAULT_PASSWORD_MAX){
		if(is_array($val)) return false;
		$val=trim($val);
		if(mb_strlen($val, 'utf-8')<$min OR mb_strlen($val, 'utf-8')>$max) return false;
		return $val;
	}


	public static function filter($params, $data_desc){

		if(!is_array($params)) return ['error'=>'filter params must be array'];
		//if(!count($params)) return ['error'=>'filter params must have at least one value'];
		if(!is_array($data_desc)) return ['error'=>'filter data_desc must be array'];
		if(!count($data_desc)) return ['error'=>'filter data_desc must have at least one value'];

		foreach ($params as $input_name=>$input_value){
			if(!isset($data_desc["$input_name"])) return ['error'=>'unknown parameter ['.$input_name.']'];
			if(!isset($data_desc["$input_name"]["type"])) return ['error'=>'there is not a data type in params'];

			$input_type=$data_desc["$input_name"]["type"];

			switch($input_type){

				case 'int':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_INT_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_INT_MAX;
					$params["$input_name"]=self::valid_int($params["$input_name"], $min, $max);
					if($params["$input_name"]===false) return ['error'=>$input_type.': incorrect value '.$input_name.' '.$min.'-'.$max];
					break;

				case 'string':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_STRING_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_STRING_MAX;
					$params["$input_name"]=self::valid_string($params["$input_name"], $min, $max);
					if($params["$input_name"]===false) return ['error'=>$input_type.': incorrect value '.$input_name.' '.$min.'-'.$max];
					break;

				case 'html':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_STRING_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_STRING_MAX;
					$params["$input_name"]=self::valid_html($params["$input_name"], $min, $max);
					if($params["$input_name"]===false) return ['error'=>$input_type.': incorrect value '.$input_name.' '.$min.'-'.$max];
					break;

				case 'array_of_int':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_INT_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_INT_MAX;
					if(!is_array($params["$input_name"])) return ['error'=>$input_type.': '.$input_name.' must be array'];
					if(!count($params["$input_name"])) return ['error'=>$input_type.': '.$input_name.' must have at least one number'];

					foreach($params["$input_name"] as $key=>$val){
						$params["$input_name"]["$key"]=self::valid_int($params["$input_name"]["$key"], $min, $max);
						if($params["$input_name"]["$key"]===false) return ['error'=>$input_type.': incorrect value in array '.$input_name.' '.$min.'-'.$max];
					}
					break;

				case 'array_of_string':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_STRING_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_STRING_MAX;
					if(!is_array($params["$input_name"])) return ['error'=>$input_type.': '.$input_name.' must be array'];
					if(!count($params["$input_name"])) return ['error'=>$input_type.': '.$input_name.' must have at least one number'];

					foreach($params["$input_name"] as $key=>$val){
						$params["$input_name"]["$key"]=self::valid_string($params["$input_name"]["$key"], $min, $max);
						if($params["$input_name"]["$key"]===false) return ['error'=>$input_type.': incorrect value in array '.$input_name.' '.$min.'-'.$max];
					}
					break;
/*
				case 'array_assoc_of_string'://////////////
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_STRING_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_STRING_MAX;
					if(!is_array($params["$input_name"])) return ['error'=>$input_type.': '.$input_name.' must be array'];
					if(!count($params["$input_name"])) return ['error'=>$input_type.': '.$input_name.' must have at least one number'];

					foreach($params["$input_name"] as $key=>$val){
						$params["$input_name"]["$key"]=self::valid_string($params["$input_name"]["$key"], $min, $max);
						if($params["$input_name"]["$key"]===false) return ['error'=>$input_type.': incorrect value in array '.$input_name.' '.$min.'-'.$max];
						
						
						
					}
					break;
*/
				case 'email':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_EMAIL_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_EMAIL_MAX;
					$params["$input_name"]=self::valid_email($params["$input_name"], $min, $max);
					if($params["$input_name"]===false) return ['error'=>'Неправильно введён email '.$input_name.'. Длина '.$min.'-'.$max];
					break;

				case 'phone':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_PHONE_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_PHONE_MAX;
					$params["$input_name"]=self::valid_phone($params["$input_name"], $min, $max);
					if($params["$input_name"]===false) return ['error'=>'Неправильно введён номер телефона '.$input_name.'. Длина '.$min.'-'.$max];
					break;

				case 'login':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_LOGIN_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_LOGIN_MAX;
					$params["$input_name"]=self::valid_login($params["$input_name"], $min, $max);
					if($params["$input_name"]===false) return ['error'=>'Неправильно введён логин '.$input_name.'. Длина '.$min.'-'.$max];
					break;

				case 'password':
					$min=isset($data_desc["$input_name"]["min"])? $data_desc["$input_name"]["min"] : self::DEFAULT_PASSWORD_MIN;
					$max=isset($data_desc["$input_name"]["max"])? $data_desc["$input_name"]["max"] : self::DEFAULT_PASSWORD_MAX;
					$params["$input_name"]=self::valid_password($params["$input_name"], $min, $max);
					if($params["$input_name"]===false) return ['error'=>'Неправильно введён пароль. Длина '.$min.'-'.$max];
					break;

				case 'not_verify':
					break;

				case 'helper':
					//
					break;

				default:
					return ['error'=>'unknown params data type ['.$input_type.']'];

			}
		}

		foreach ($data_desc as $desc_name=>$desc_value){
			//echo $desc_name.' <br />';
			//echo $desc_name["require"].' <br />';
			//var_dump($desc_name);

			/** required **/
			if(isset($desc_value["require"]) AND $desc_value["require"]==true AND !isset($params["$desc_name"])) return ['error'=>$desc_name.' required'];
		}

		return $params;
	}

}