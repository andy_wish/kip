<?php if (!defined("INBOX")) die('separate call');

class Basis {

	public static function create($params){

		$data_desc=self::DATA_DESC;
		unset($data_desc["id"], $data_desc["meta_name"]);
		$data_desc["type"]["require"]=true;
		$params=Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["lang"])) $params["lang"]=Lang::current_lang();
		if(isset($params["name"]) AND $params["name"]!='' AND !isset($params["name_translit"])) $params["name_translit"]=Core::translit($params["name"]);

		//////SET
		$into='INTO `basis` (';
		$values='VALUES (';
		foreach($params as $name => $val) {
			switch($name){
				case 'closed':
					if($val==1){
						$into.='`basis`.`'.$name.'`, ';
						$values.='NOW(), ';
					}
					continue 2;

				default:
					$into.='`basis`.`'.$name.'`, ';
					$values.=DB::parse('?s, ', $val);
			}
		}
		if(strlen($into)>15 AND strlen($values)>9){
			$into.='`ip`, `user_id`)';
			$values.=DB::parse('?s, ?i)', User::ip(), User::id());
		}else return ['error'=>'nothing to insert'];

		$q='INSERT '.$into.' '.$values;
		DB::query($q);
		$id=DB::insertId();
		if($id){
			User::logging(10, $id);
			return ["id"=>$id];
		}else return ['error'=>'db error'];

		return $result;
	}

	public static function read($params){

		$data_desc=self::DATA_DESC;
		$data_desc["lang"]["max"]=3;
		$data_desc["search_str"]=[
			"type"=>'string',
			"min"=>2,
			"max"=>255
		];
		$data_desc["types"]=[////////////
			"type"=>'array_of_string',
			"min"=>1,
			"max"=>255
		];
		$params=Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["lang"])) $params["lang"]=Lang::current_lang();
		if(!isset($params["closed"])) $params["closed"]=0;
		if(!isset($params["meta_get_array"])) $params["meta_get_array"]=0;

		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by='`basis`.`id`';
					break;
				case 'id_parent':
					$order_by='`basis`.`id_parent`';
					break;
				case 'name':
					$order_by='`basis`.`name`';
					break;
				case 'name_translit':
					$order_by='`basis`.`name_translit`';
					break;
				case 'type':
					$order_by='`basis`.`type`';
					break;
				case 'content':
					$order_by='`basis`.`content`';
					break;
				case 'title':
					$order_by='`basis`.`title`';
					break;
				case 'description':
					$order_by='`basis`.`description`';
					break;
				case 'keywords':
					$order_by='`basis`.`keywords`';
					break;
				case 'lang':
					$order_by='`basis`.`lang`';
					break;
				case 'order_num':
					$order_by='`basis`.`order_num`';
					break;
				case 'img':
					$order_by='`basis`.`img`';
					break;
				case 'target':
					$order_by='`basis`.`target`';
					break;
				case 'popular':
					$order_by='`basis`.`popular`';
					break;
				case 'mime_type':
					$order_by='`basis`.`mime_type`';
					break;
				case 'created':
					$order_by='`basis`.`created`';
					break;
				case 'closed':
					$order_by='`basis`.`closed`';
					break;
				default:
					$order_by='`basis`.`order_num`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='desc') $order_dir='DESC';
			else $order_dir='ASC';
			$order_by='ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by='ORDER BY `basis`.`order_num`, `basis`.`id`';

		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit='LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit='LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit='LIMIT 20';

		/////WHERE
		$where='WHERE ';

		if($params["lang"]!='all'){
			$where.=DB::parse('`basis`.`lang`=?s AND ', $params["lang"]);
		}

		if(!$params["closed"]){
			$where.='`basis`.`closed` IS NULL AND ';
		}

		if(isset($params["type"])) $where.=DB::parse('`basis`.`type`=?s AND ', $params["type"]);
		if(isset($params["id"])) $where.=DB::parse('`basis`.`id`=?i AND ', $params["id"]);
		if(isset($params["id_parent"])) $where.=DB::parse('`basis`.`id_parent`=?i AND ', $params["id_parent"]);

		if(isset($params["name_translit"])) $where.=DB::parse(' `basis`.`name_translit`=?s AND ', $params["name_translit"]);
		if(isset($params["search_str"])) $where.=DB::parse("(`basis`.`name` LIKE(?s) OR `basis`.`content` LIKE(?s)) AND ", '%'.$params["search_str"].'%', '%'.$params["search_str"].'%');


		if(strlen($where)>5) $where=rtrim($where, 'AND ');
		else $where='';

		//$result=[];

		$q='SELECT COUNT(`id`) FROM `basis` '.$where;
		//die($q);
		$result["total_db"]=(int)DB::getOne($q);
		if($result["total_db"]==0){
			$result["total"]=0;
			$result["list"]=[];
			return $result;
		}

		/////SELECT
		$select='
			SELECT 
				`basis`.*, DATE_FORMAT(`basis`.`created`, "'.Core::config("mysql_date_comment_nice").'") AS `created_nice`,
				`parent`.`name` AS `name_parent`, `parent`.`name_translit` AS `name_translit_parent`, 
				(SELECT `user`.`name` FROM `user` WHERE `user`.`id`=`basis`.`user_id` LIMIT 1) AS `user_name`,
				(SELECT COUNT(`child`.`id`) FROM `basis` AS `child` WHERE `child`.`id_parent`=`basis`.`id` GROUP BY `child`.`id_parent`) AS `child_num`';
		if(isset($params["meta_get_value"])) $select.=DB::parse(', (SELECT `basis_meta`.`value` FROM `basis_meta` WHERE `basis_meta`.`basis_id`=`basis`.`id` AND `basis_meta`.`name`=?s LIMIT 1) AS `meta_get_value`', $params["meta_get_value"]);

		$q=$select.'
			FROM `basis`
			LEFT JOIN `basis` AS `parent` ON `parent`.`id`=`basis`.`id_parent`
			'.$where.'
			'.$order_by.'
			'.$limit;
//echo($q);echo '<br />';
		$list=DB::getAll($q);


		if(!$list){
			$result["total"]=0;
			return $result;
		}

		$id_current=false;
		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
//echo $name.' => '.$value.'<br />';
				if($name=='content'){
					$result["list"]["$total"]["$name"]=htmlspecialchars_decode($value);
					continue;
				}

				if($name=='meta_get_value'){
					$name=$params["meta_get_value"];
					$result["list"]["$total"]["meta"]["$name"]=unserialize($value);
					continue;
				}

				if($value=='') {
					$result["list"]["$total"]["$name"]='';
					continue;
				}
				$result["list"]["$total"]["$name"]=$value;
			}
//echo '<hr />';
			$total++;
		}
		$result["total"]=$total;
		
		if(isset($params["meta_get"]) AND $params["meta_get"]!=0){//медленный запрос, тестируем
			foreach($result as &$basis){
				$meta_params["basis_id"]=$basis["id"];
				if($params["meta_get"]=='autoload') $meta_params["autoload"]=1;
				$meta=self::read_meta($meta_params);
				if(isset($meta["error"])) return $meta;

				if(!isset($basis["meta"])) $basis["meta"]=$meta;
				else $basis["meta"]=array_merge($basis["meta"], $meta);
			}
			unset($basis);
		}

		//echo '<pre>';var_dump($result["list"][0]);exit;
		//echo '<pre>';var_dump($result);die();

		return $result;
	}

	public static function update($params){

		$data_desc=self::DATA_DESC;
		$data_desc["id"][]=["require"=>true];
		$params=Verify::filter($params, $data_desc);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `basis` WHERE `id`=?i LIMIT 1', $params["id"])) return ['error'=>'basis ['.$params["id"].'] not found'];

		//////SET
		$set='SET ';
		foreach($params as $name => $val) {
			switch($name){
				case 'id':
					continue 2;
				case 'closed':
					if($val==0) $set.=DB::parse('`basis`.`'.$name.'`=NULL, ');
					else $set.=DB::parse('`basis`.`'.$name.'`=NOW(), ');
					continue 2;
				default:
					$set.=DB::parse('`basis`.`'.$name.'`=?s, ', $val);
			}
		}
		if(strlen($set)>5) $set=rtrim($set, ', ');
		else return ['error'=>'nothing to update'];

		if(DB::query('UPDATE `basis` '.$set.', `user_id`=?i, `ip`=?s WHERE `basis`.`id`=?i LIMIT 1', User::id(), User::ip(), $params["id"])){
			User::logging(11, $params["id"]);
			return array("id"=>$params["id"]);
		}else return ['error'=>'db error'];
	}

	public static function delete($params){
		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"wipe"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			]
		]);
		if(isset($params["error"])) return $params;
	
		if(!isset($params["wipe"]) OR $params["wipe"]==0){
			DB::query('UPDATE `basis` SET `closed`=NOW() WHERE `id`=?i LIMIT 1', $params["id"]);
			$log_comment='';
		}else{
			DB::query('DELETE FROM `basis` WHERE `id`=?i LIMIT 1', $params["id"]);
			$log_comment='wipe';
		}
		if(DB::affectedRows()) {
			User::logging(12, $params["id"], $log_comment);
			return ["id"=>$params["id"]];
		}else return ['error'=>'db error / not found'];
	}

	public static function create_meta($params){
		$params=Verify::filter($params, [
			"basis_id"=>[
				"type"=>'int',
				"require"=>true
			],
			"name"=>[
				"type"=>'string',
				"min"=>1,
				"max"=>255,
				"require"=>true
			],
			"value"=>[
				"type"=>'not_verify',//?
				"require"=>true
			],
			"autoload"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			]
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["autoload"])) $params["autoload"]=0;

//echo 'meta_params:<br /> <pre>'.var_dump($params).'</pre>';

		$q='INSERT INTO `basis_meta` (`basis_id`, `name`, `value`, `autoload`) VALUES (?i, ?s, ?s, ?i)';
		DB::query($q, $params["basis_id"], $params["name"], serialize($params["value"]), $params["autoload"]);

		$id=DB::insertId();
		if($id){
			User::logging(13, $id, $params["name"]);
			return ["id"=>$id];
		}else return ['error'=>'db error'];

		return $result;
	}

	public static function read_meta($params){
		$params=Verify::filter($params, [
			"basis_id"=>[
				"type"=>'int',
				"require"=>true
			],
			"id"=>[
				"type"=>'int',
				"min"=>0
			],
			"name"=>[
				"type"=>'string',
				"min"=>1,
				"max"=>255
			],
			"autoload"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			]
		]);
		if(isset($params["error"])) return $params;

		/////WHERE
		$where='WHERE `basis_meta`.`basis_id`=?i';
		if(isset($params["id"])) $where.=DB::parse(' AND `basis_meta`.`basis_id`=?i', $params["id"]);
		if(isset($params["name"])) $where.=DB::parse(' AND `basis_meta`.`name`=?s', $params["name"]);
		if(isset($params["autoload"])) $where.=DB::parse(' AND `basis_meta`.`autoload`=?i', $params["autoload"]);

		/////SELECT
		$q='
			SELECT *
			FROM `basis_meta`
			'.$where;
		$list=DB::getAll($q);

		if(!$list){
			$result["total"]=0;
			return $result;
		}

		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
					$result["list"]["$name"]=unserialize($value);
			}
			$total++;
		}
		$result["total"]=$total;
		return $result;
	}

	public static function parents($params){
		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;

		if(count($arr=self::parents_recurcive($params["id"]))<2) return [];
		array_shift($arr);

		return array_reverse($arr);
	}

	private static function parents_recurcive($id, $arr=[]){
		$row=DB::getRow('SELECT `id`, `id_parent`, `name`, `name_translit` FROM `basis` WHERE `id`='.$id.' LIMIT 1');
		if($row){
			$arr[]=$row;
			$arr=self::parents_recurcive($row["id_parent"], $arr);
		}
		return $arr;
	}



	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}


	protected const DATA_DESC=[
			"meta_get_array"=>[//0, autoload
				"type"=>'string',
				"min"=>2,
				"max"=>4
			],
			"meta_get_value"=>[
				"type"=>'string',
				"min"=>1,
				"max"=>255
			],
			"meta_array"=>[
				"type"=>'array_assoc',//////?
				"min"=>1,
				"max"=>65535
			],
			"id"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>18446744073709551615
			],
			"id_parent"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>18446744073709551615
			],
			"name"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>65535
			],
			"name_translit"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>65535
			],
			"type"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"content"=>[
				"type"=>'html',
				"min"=>0,
				"max"=>16777215
			],
			"title"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"description"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"keywords"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"lang"=>[
				"type"=>'string',
				"min"=>2,
				"max"=>2
			],
			"order_num"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>65535
			],
			"img"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"target"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"popular"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			],
			"mime_type"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"created"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"user_id"=>[
				"type"=>'int',
				"min"=>0
			],
			"closed"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			],
			"response"=>[
				"type"=>'helper'
			],
			"search_str"=>[
				"type"=>'string',
				"min"=>2,
				"max"=>255
			]
		];

}