<?php if (!defined("INBOX")) die('separate call');

class User {

	const TOKEN_LIFETIME=10368000;//120 дней.

	protected static $_instance;

	private static $user;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	private static function user_data_default(){
		self::$user=array(
			"id"=>0,
			"ip"=>self::get_ip(),
			"name"=>'Гость',
			"phone"=>'',
			"card_number"=>'',
			"card_holder"=>'',
			"qiwi"=>'',
			"email"=>'',
			"group_id"=>1,
			"group_name"=>'Гости'
		);
	}

	private static function user_data_auth($u){
		self::$user=array(
			"id"=>$u["id"],
			"name"=>$u["name"],
			"phone"=>$u["phone"],
			"email"=>$u["email"],
			"card_number"=>$u["card_number"],
			"card_holder"=>$u["card_holder"],
			"qiwi"=>$u["qiwi"],
			"group_id"=>$u["group_id"],
			"group_name"=>$u["group_name"]
		);
		if(isset($u["ip"])) self::$user["ip"]=$u["ip"];
		else self::$user["ip"]=self::get_ip();
	}

	private static function token_check($token_input){

		$token_input=self::token_parse($token_input);
		if(isset($token_input["error"])) return $token_input;

		if(!$token_row=DB::getRow('SELECT `id`, `user_id`, `token`, (CASE WHEN (`expired`>NOW()) THEN 0 ELSE 1 END) AS `expired` FROM `token` WHERE `id`=?i LIMIT 1', $token_input["id"])) return ["error"=> 'access token invalid'];

		if($token_input["str"]==$token_row["token"]) {
			if($token_row["expired"]==1) return array("error"=> 'token expired');

			$q="SELECT * FROM `user` WHERE `id`=?i LIMIT 1";
			if(!$u=DB::getRow($q, $token_row["user_id"])) return array("error"=> 'user not found ['.$token_input["user_id"].']');

			$row=DB::getRow('SELECT `name` FROM `user_group` WHERE `id`=?i LIMIT 1', $u["group_id"]);
			$u["group_name"]=$row["name"];
			self::user_data_auth($u);
			return true;

		}else return array("error"=> 'access token invalid');

	}

	private static function token_parse($t){
		if(mb_strlen($t, 'utf-8')<34 OR !strpos($t, ':')) return array("error"=> 'token is invalid. template "id:string" and more than 34 simbols ['.$t.']', 1);
		$t=explode(':', $t);
		if(!isset($t[1])) return array("error"=> 'unable to parse token');
		if((int)$t[0]<1 OR mb_strlen($t[1], 'utf-8')<32 OR mb_strlen($t[1], 'utf-8')>32) return array("error"=> 'unable to parse token. id['.$t[0].'] must be positive int, str['.$t[1].'] must be 32 length', 1);
		return array('id'=>(int)$t[0], 'str'=>$t[1]);
	}

	private static function token_create(){

		$t["token_str"]=self::random_string(32);
		$t["token_expired"]=time()+self::TOKEN_LIFETIME;

		$q="INSERT INTO `token` (`user_id`, `token`, `created`, `expired`) VALUES (?i, ?s, NOW(), FROM_UNIXTIME(".$t["token_expired"]."))";
		if(!DB::query($q, self::id(), $t["token_str"])) return ["error"=> 'server token BD error'];
		$t["token_id"]=DB::insertId();

		return $t;
	}

	private static function get_ip(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) return $_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR'];
		elseif (!empty($_SERVER['REMOTE_ADDR'])) return $_SERVER['REMOTE_ADDR'];
		else return 'unknown';
	}

	public static function auth($token=false) {

		if (self::$_instance === null) {
			self::$_instance = new self;
			self::user_data_default();

			if($token) {
				self::token_check($token);
			}
		}
	}

	public static function auth_ulogin($var){

		$ulogin_data=file_get_contents('http://ulogin.ru/token.php?token='.$var["post"]["token"].'&host='.$_SERVER['HTTP_HOST']);
		$ulogin_data=json_decode($ulogin_data, true);

		if(empty($ulogin_data)) return ["error"=> 'ulogin_error: empty answer'];
		if(isset($ulogin_data["error"])) return ["error"=> 'ulogin_error: '.$ulogin_data["error"]];

		$user_id=false;
		//проверяем, существует ли такой пользователь в базе
		if($ulogin_db=DB::getRow('SELECT * FROM `ulogin` WHERE `ident`=?s LIMIT 1', $ulogin_data["identity"])){
			$user_id=DB::getOne('SELECT `id` FROM `user` WHERE `id`=?i LIMIT 1', $ulogin_db["user_id"]);
			if(!$user_id) DB::query('DELETE FROM `ulogin` WHERE `ident`=?s', $ulogin_data["identity"]);
			else $var["ulogin"]["password"]=substr($ulogin_data["identity"], 0, 8).$ulogin_db["salt"];
		}

		if(!$user_id){
			//добавляем нового пользователя
			$reg_user["email"]=$ulogin_data["email"];
			$reg_user["name"]=$ulogin_data["first_name"];
			if(isset($ulogin_data["last_name"])) $reg_user["name"].=' '.$ulogin_data["last_name"];
			
			if(isset($ulogin_data["phone"])) $reg_user["phone"]=$ulogin_data["phone"];

			$reg_user["salt"]=self::random_string(8);
			$reg_user["password"]=substr($ulogin_data["identity"], 0, 8).$reg_user["salt"];
			$reg_user["actkey"]=false;
			$reg_user["group_id"]=2;
			//добавляем пользователя в таблицу user сайта и получаем user_id новой записи
			$reg_user=self::create($reg_user);
			if(isset($reg_user["error"])){
				$var["error"]=$reg_user["error"];
				return $var;
			}
			self::logging(5, $reg_user["id"]);

			if(!DB::query('INSERT INTO `ulogin` (`user_id`, `ident`, `salt`) VALUES (?i, ?s, ?s)', $reg_user["id"], $ulogin_data["identity"], $reg_user["salt"])){
				DB::query('DELETE FROM `user` WHERE `id`=?i LIMIT 1', $reg_user["id"]);
				$var["error"]=$reg_user["db ulogin error"];
				return $var;
			}
			
			$var["ulogin"]["password"]=substr($ulogin_data["identity"], 0, 8).$reg_user["salt"];

		}

		$var["ulogin"]["login"]=$ulogin_data["email"];
		return $var;
	}

	public static function password_update($params){

		if(!self::$user["id"]) return ["error"=> 'Незарегистрированный пользователь'];

		$params=Verify::filter($params, [
			"password_old"=>[
				"type"=>'password',
				"require"=>true,
				"min"=>8,
				"max"=>16
			],
			"password_new"=>[
				"type"=>'password',
				"require"=>true,
				"min"=>8,
				"max"=>16
			],
			"password_new2"=>[
				"type"=>'password',
				"require"=>true,
				"min"=>8,
				"max"=>16
			],
		]);
		if(isset($params["error"])) return $params;

		if($params["password_old"] == $params["password_new"]) return ["error"=> 'Новый пароль совпадает со старым'];

		$q='SELECT `user`.`password` FROM `user` WHERE `id`=?i AND `actkey` IS NULL LIMIT 1';
		if(!$password_old=DB::getOne($q, self::$user["id"])) return ["error"=> 'Пользователь не найден, либо не активирован'];

		if(!password_verify($params["password_old"], $password_old)) return ["error"=> 'Пароль неправильный'];

		$params["password_new"] = password_hash($params["password_new"], PASSWORD_DEFAULT);

		if(!DB::query('UPDATE `user` SET `password`=?s WHERE `id`=?i LIMIT 1', $params["password_new"], self::$user["id"])) return ["error"=> 'Ошибка. Попробуйте позднее'];

		self::logging(8);

		return self::$user["id"];
	}

	public static function delete($params){

		if(!self::$user["id"]) return ["error"=> 'Не зарегистрированный пользователь'];

		/*$params=Verify::filter($params, [
			"password"=>[
				"type"=>'password',
				"require"=>true,
				"min"=>8,
				"max"=>16
			]
		]);
		if(isset($params["error"])) return $params;*/

		$q='SELECT `user`.`password` FROM `user` WHERE `id`=?i LIMIT 1';
		if(!$password=DB::getOne($q, self::$user["id"])) return ["error"=> 'Пользователь не найден'];

		//if(!password_verify($params["password"], $password)) return ["error"=> 'Пароль неправильный'];

		if(!DB::query('DELETE FROM `user` WHERE `id`=?i LIMIT 1', self::$user["id"])) return ["error"=> 'Ошибка. Попробуйте позднее'];
		DB::query('DELETE FROM `token` WHERE `user_id`=?i', self::$user["id"]);
		DB::query('DELETE FROM `ulogin` WHERE `user_id`=?i', self::$user["id"]);

		self::logging(9, self::$user["id"]);

		self::user_data_default();

		return self::$user["id"];
	}

	public static function access_recovery_request($params){

		$params=Verify::filter($params, [
			"email"=>[
				"type"=>'email',
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;

		$q='SELECT `user`.`id` FROM `user` WHERE `email`=?s LIMIT 1';
		if(!$id=DB::getOne($q, $params["email"])) return ["error"=> 'Пользователь не найден'];

		$secretkey=self::random_string(10);
		if(!DB::query('UPDATE `user` SET `secretkey`=?s WHERE `id`=?i LIMIT 1', $secretkey, $id)) return ["error"=> 'Ошибка. Попробуйте позднее'];

		if(!Notify::access_recovery_send(["email"=>$params["email"], "id"=>$id, "secretkey"=>$secretkey])) return ["error"=> 'Ошибка 2. Попробуйте позднее'];

		self::logging(30, self::$user["id"], $params["email"]);

		return $params["email"];
	}

	public static function access_recovery_action($params){

		$params=Verify::filter($params, [
			"user_id"=>[
				"type"=>'int',
				"require"=>true
			],
			"secretkey"=>[
				"type"=>'string',
				"min"=>10,
				"max"=>10,
				"require"=>true
			],
			"password"=>[
				"type"=>'password',
				"require"=>true,
				"min"=>8,
				"max"=>16
			],
			"password2"=>[
				"type"=>'password',
				"require"=>true,
				"min"=>8,
				"max"=>16
			],
			"token"=>[
				"type"=>'string',
				"require"=>true
			],
			"action"=>[
				"type"=>'string',
				"require"=>true
			],
		]);
		if(isset($params["error"])) return $params;

		if(!Recaptcha::check($params["token"], $params["action"])) return ["error"=>'data error'];

		if($params["password"] != $params["password2"]) return ["error"=> 'Пароли не совпадают'];

		$q='SELECT `user`.`id` FROM `user` WHERE `id`=?i AND `secretkey`=?s LIMIT 1';
		if(!$id=DB::getOne($q, $params["user_id"], $params["secretkey"])) return ["error"=> 'Пользователь не найден'];

		$params["password_new"]=password_hash($params["password"], PASSWORD_DEFAULT);

		if(!DB::query('UPDATE `user` SET `password`=?s, `secretkey`=NULL WHERE `id`=?i LIMIT 1', $params["password_new"], $params["user_id"])) return ["error"=> 'Ошибка. Попробуйте позднее'];

		self::logging(31, $params["user_id"]);

		return $params["user_id"];
	}

	public static function login($params){

		$params=Verify::filter($params, [
			"email"=>[
				"type"=>'email',
				"require"=>true
			],
			"password"=>[
				"type"=>'password',
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;

		$q="
			SELECT `user`.*, `user_group`.`name` AS `group_name`
			FROM `user`
			LEFT JOIN `user_group` ON `user_group`.`id`=`user`.`group_id`
			WHERE `email`=?s AND `actkey` IS NULL LIMIT 1";
		if(!$user=DB::getRow($q, $params["email"])) return ["error"=> 'Пользователь не найден, либо не активирован'];
		if(!password_verify($params["password"], $user["password"])) return ["error"=> 'Пароль неправильный'];

		self::user_data_auth(
			array(
				"id"=>$user["id"],
				"name"=>$user["name"],
				"phone"=>$user["phone"],
				"email"=>$user["email"],
				"card_number"=>$user["card_number"],
				"card_holder"=>$user["card_holder"],
				"qiwi"=>$user["qiwi"],
				"group_id"=>$user["group_id"],
				"group_name"=>$user["group_name"]
			)
		);
		self::logging(3, $user["id"]);

		return self::token_create();

	}

	public static function logout($t){

		DB::query('DELETE FROM `token` WHERE `id`=?i AND `token`=?s LIMIT 1', $t["token_id"], $t["token_str"]);
		self::logging(4, self::id());
		self::user_data_default();

	}

	public static function registration_start($params){

		$params=Verify::filter($params, [
			"name"=>[
				"type"=>'string',//type change to login?
				"require"=>true,
				"min"=>3,
				"max"=>16
			],
			"email"=>[
				"type"=>'email',
				"require"=>true
			],
			"password"=>[
				"type"=>'password',
				"require"=>true
			],
			"password2"=>[
				"type"=>'password',
				"require"=>true
			],
			"phone"=>[
				"type"=>'phone',
				"require"=>true
			]
		
		]);
		if(isset($params["error"])) return $params;

		$params["actkey"]=self::random_string(10);

		$params=self::create($params);

		if(isset($params["error"])) return $params;

		self::logging(1, $params["id"]);

		if(!Notify::user_registration($params)) $params["error"]='notify error';

		return $params;
	}

	private static function create($params){

		$params=Verify::filter($params, [
			"name"=>[
				"type"=>'string',//type change to login?
				"require"=>true,
				"min"=>3,
				"max"=>255
			],
			"name_translit"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>16
			],
			"email"=>[
				"type"=>'email',
				"require"=>true
			],
			"password"=>[
				"type"=>'password',
				"require"=>true
			],
			"password2"=>[
				"type"=>'password'
			],
			"phone"=>[
				"type"=>'phone'
			],
			"group_id"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>9
			],
			"salt"=>[
				"type"=>'helper',
			],
			"actkey"=>[
				"type"=>'helper',
			],
		]);
		if(isset($params["error"])) return $params;

		$params=self::find_double_users($params);
		if (isset($params["error"])) return $params;

		if(!isset($params["name_translit"])) $params["name_translit"]=Core::translit($params["name"]);
		if (!isset($params["phone"])) $params["phone"]='';
		if(!isset($params["group_id"])) $params["group_id"]=1;

		$params["hash"]=password_hash($params["password"], PASSWORD_DEFAULT);

		if($params["actkey"]) {
			$q='INSERT INTO `user` (`name`, `name_translit`, `group_id`, `password`, `phone`, `email`, `actkey`) VALUES (?s, ?s, ?i, ?s, ?s, ?s, ?s)';
			if(!DB::query($q, $params["name"], $params["name_translit"], $params["group_id"], $params["hash"], $params["phone"], $params["email"], $params["actkey"])){
				return ["error"=>'db error'];
			}
		}else{
			$q='INSERT INTO `user` (`name`, `name_translit`, `group_id`, `password`, `phone`, `email`, `actkey`) VALUES (?s, ?s, ?i, ?s, ?s, ?s, NULL)';
			if(!DB::query($q, $params["name"], $params["name_translit"], $params["group_id"], $params["hash"], $params["phone"], $params["email"])){
				return ["error"=>'db error'];
			}
		}

		$params["id"]=DB::insertId();
		return $params;
	}

	public static function find_double_users($params){
		/*
		if(DB::getOne('SELECT `id` FROM `user` WHERE `name_translit`=?s', $params["name_translit"])) {
			$params["error"]='Пользователь с таким именем уже существует';
			return $params;
		}
		*/
		if(DB::getOne('SELECT `id` FROM `user` WHERE `email`=?s', $params["email"])) {
			$params["error"]='Пользователь с таким email уже существует';
			return $params;
		}
		return $params;
	}

	public static function email_activate($params) {

		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"actkey"=>[
				"type"=>'string',
				"min"=>10,
				"max"=>10,
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;

		$q='UPDATE `user` SET `actkey`=NULL, `group_id`=2 WHERE `id`=?i AND `actkey`=?s LIMIT 1';
		if(DB::query($q, $params["id"], $params["actkey"]) AND DB::affectedRows()==1) {
			self::logging(2, $params["id"]);
			return $params;
		}else{
			$params["error"]='activation data db error';
			return $params;
		}
	}

	public static function logging($action_id, $target_id=0, $comment='') {
		if(DB::query(
			'INSERT INTO `user_log` (`user_id`, `action_id`, `target_id`, `comment`, `ip`) VALUES (?i, ?i, ?i, ?s, ?s)',
			self::id(), $action_id, $target_id, $comment, self::ip()
		)) return true;
		else return false;
	}

	public static function random_string($l=32){
		$s=str_shuffle('abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
		return substr($s, 1, $l);
	}

	public static function registration_condition() {
		return Core::config('registration_condition_text');
	}
	
	public static function id() {
		return self::$user["id"];
	}
	public static function ip() {
		return self::$user["ip"];
	}
	public static function group_id() {
		return self::$user["group_id"];
	}
	public static function group_name() {
		return self::$user["group_name"];
	}
	public static function name() {
		return self::$user["name"];
	}
	public static function email() {
		return self::$user["email"];
	}
	public static function phone() {
		return self::$user["phone"];
	}

	public static function info() {
		$params["id"]=self::$user["id"];
		return self::read($params);
	}
	public static function read($params) {
		
		$params=Verify::filter($params, [
					"id"=>[
						"type"=>'int',
						"min"=>1,
						"required"=> true
					]
				]);
		if(isset($params["error"])) return $params;
		
		if(!$info=DB::getRow('SELECT * FROM `user` WHERE `id`=?i LIMIT 1', $params["id"])){
			return ["error"=> 'not found'];
		}
		unset($info["password"], $info["actkey"]);
		return $info;
	}

	public static function info_update($params){

		if(!self::$user["id"]) return ["error"=> 'not found'];

		$params=Verify::filter($params, [
			"payment_method"=>[
				"type"=>'string',
				"min"=>2,
				"max"=>255
			],
			"card_number"=>[
				"type"=>'string',
				"min"=>19,
				"max"=>19
			],
			"card_holder"=>[
				"type"=>'string',
				"min"=>4,
				"max"=>255
			],
			"phone"=>[
				"type"=>'string',
				"min"=>9,
				"max"=>16
			],
			"qiwi"=>[
				"type"=>'string',
				"min"=>9,
				"max"=>16
			]
		]);
		if(isset($params["error"])) return $params;
		
		//////SET
		$set=array();
		if(isset($params["payment_method"])) $set[]=DB::parse('`'.DB_PREFIX.'user`.`payment_method`=?s', $params["payment_method"]);
		if(isset($params["phone"])) $set[]=DB::parse('`'.DB_PREFIX.'user`.`phone`=?s', $params["phone"]);
		if(isset($params["card_number"])) $set[]=DB::parse('`'.DB_PREFIX.'user`.`card_number`=?s', $params["card_number"]);
		if(isset($params["card_holder"])) $set[]=DB::parse('`'.DB_PREFIX.'user`.`card_holder`=?s', $params["card_holder"]);
		if(isset($params["qiwi"])) $set[]=DB::parse('`'.DB_PREFIX.'user`.`qiwi`=?s', $params["qiwi"]);

		if(count($set)) $set='SET '.implode(', ', $set);
		else return ['error'=>'nothing to update'];

		if(DB::query('UPDATE `'.DB_PREFIX.'user` ?p WHERE `'.DB_PREFIX.'user`.`id`=?i LIMIT 1', $set, self::$user["id"])){
			User::logging(7);
			return array("id"=>self::$user["id"]);
		}else return array('error'=>'db error');
	}
}