<?php if (!defined("INBOX")) die('separate call');

Notify::instance();

class Notify {

	protected static $_instance;
	protected static $config;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){
		self::$config["site_name"]=Core::config('site_name');
		self::$config["smtp"]["username"]=Core::config('smtp_login');
		self::$config["smtp"]["password"]=Core::config('smtp_password');
		self::$config["smtp"]["host"]=Core::config('smtp_host');
		self::$config["name_from"]=Core::config('notify_name_from');
		self::$config["smtp"]["port"]=Core::config('smtp_port');
		self::$config["feedback_target"]=Core::config('feedback_send_to_email');
		self::$config["charset"]=Core::config('notify_email_charset');
		self::$config["email_from"]=Core::config('notify_email_from');
		require ROOT_DIR.'/app/plugin/SendMailSmtpClass.php';
	}

	public static function instance() {
		if (self::$_instance === null) {
			self::$_instance = new self;
		}
	}

	public static function user_registration($params) {
		$subject='Добро пожаловать!';
		$mail_text='
			<p>'.$params["name"].', сохраните это сообщение. Ваша учётная запись:<br />
			Логин: '.$params["email"].'<br />
			Пароль: '.$params["password"].'<br />
			<a href="http://'.$_SERVER['SERVER_NAME'].'/user/activate/?u='.$params["id"].'&k='.$params["actkey"].'" style="
				background-color: #38d;
				border-radius: 5px;
				color: #eee;
				display: block;
				padding: 10px;
				margin-top:5px;
				text-align: center;
				text-decoration: none;
				width: 120px;" target="_blank" rel=" noopener noreferrer">Активировать</a>
			</p>
			<p style="padding:24px 0 5px;line-height:1.2;font-size:12px;color:#555;">Если кнопка не работает, скопируйте и вставьте эту ссылку в адресную строку браузера:<br />
			http://'.$_SERVER['SERVER_NAME'].'/user/activate/?u='.$params["id"].'&k='.$params["actkey"].'
			</p>
		';
		$result=self::email($params["email"], $mail_text, $subject);
		if(!isset($result["error"])) return true;
		else return $result;
	}

	public static function access_recovery_send($params){

		$params=Verify::filter($params, [
			"email"=>[
				"type"=>'email',
				"require"=>true
			],
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"secretkey"=>[
				"type"=>'string',
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;
		
		//["id"=>$id, "secretkey"=>$secretkey]
		$subject='Восстановление доступа';
		$mail_text='
			<p>Для восстановления пароля, нажмите кнопку<br />
			<a href="http://'.$_SERVER["SERVER_NAME"].'/user/access_recovery_form/?u='.$params["id"].'&k='.$params["secretkey"].'"
				style="
				background-color: #38d;
				border-radius: 5px;
				color: #eee;
				display: block;
				padding: 10px;
				margin-top:5px;
				text-align: center;
				text-decoration: none;
				width: 120px;" target="_blank" rel=" noopener noreferrer">Восстановить</a>
			</p>
			<p style="padding:14px 0 5px;">Если вы не запрашивали восстановление пароля, просто удалите это письмо.
			</p>
			<p style="padding:24px 0 5px;line-height:1.2;font-size:12px;color:#555;">Если кнопка не работает, скопируйте и вставьте эту ссылку в адресную строку браузера:<br />
			http://'.$_SERVER['SERVER_NAME'].'/user/access_recovery_form/?u='.$params["id"].'&k='.$params["secretkey"].'
			</p>
		';
		$result=self::email($params["email"], $mail_text, $subject);
		if(!isset($result["error"])) return true;
		else return $result;
	}
	public static function comment_created($params) {

		$mail_text='
			<p>'.$params["text"].'</p>
			<p><i>'.$params["user_name"].'['.$params["user_id"].'] '.$params["ip"].'</i></p>
			<p>
				<a href="http://'.$_SERVER['SERVER_NAME'].'/admin/comment/" style="
				background-color: #af0;
				border-radius: 5px;
				color: #fff;
				display: block;
				padding: 10px;
				margin-top:5px;
				text-align: center;
				text-decoration: none;
				width: 120px;" target="_blank" rel=" noopener noreferrer">админ-панель</a>

				<a href="http://'.$_SERVER['SERVER_NAME'].'/admin/comment/approve/'.$params["id"].'" style="
				background-color: #0f0;
				border-radius: 5px;
				color: #fff;
				display: block;
				padding: 10px;
				margin-top:5px;
				text-align: center;
				text-decoration: none;
				width: 120px;" target="_blank" rel=" noopener noreferrer">Утвердить</a>

				<a href="http://'.$_SERVER['SERVER_NAME'].'/admin/comment/delete/'.$params["id"].'" style="
				background-color: #f22;
				border-radius: 5px;
				color: #eee;
				display: block;
				padding: 10px;
				margin-top:10px;
				text-align: center;
				text-decoration: none;
				width: 120px;" target="_blank" rel=" noopener noreferrer">Удалить</a>
			</p>
		';
		$result=self::email(Core::config('email_admin'), $mail_text, 'Новый комментарий');
		if(!isset($result["error"])) return true;
		else return $result;
	}

	public static function feedback($params) {

		$subject='Обратная связь '.self::$config["site_name"];
		$mail_text='
			<p>'.$params["name"].'['.User::id().']['.User::ip().']<br />
			email: <a href="mailto:'.$params["email"].'?subject=Ответ на ваше обращение на '.self::$config["site_name"].'">'.$params["email"].'</a><br />
		';
		if(isset($params["phone"]) AND $params["phone"]!='') $mail_text.='телефон: '.$params["phone"].'<br />';
		$mail_text.='</p>';

		$mail_text.='<p style="padding:10px;">'.$params["text"].'</p>';

		$result=self::email(self::$config["feedback_target"], $mail_text, $subject);
		if(!isset($result["error"])) return true;
		else return $result;
	}

	public static function email($to, $text, $subject='оповещение') {
		$mailSMTP=new SendMailSmtpClass(
			self::$config["smtp"]["username"],
			self::$config["smtp"]["password"],
			self::$config["smtp"]["host"],
			self::$config["smtp"]["port"],
			self::$config["charset"]
		);

		$result = $mailSMTP->send($to, $subject, $text, [
			self::$config["name_from"],
			self::$config["email_from"]
		]);

		if($result !== true) return ["error"=>$result];
	}

}