<?php if (!defined("INBOX")) die('separate call');

Files::init();

class Files {

	protected static $_instance;
	protected static $config;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	public static function init() {
		if (self::$_instance === null) {
			self::$_instance = new self;
			self::$config=[
				"max_file_size"=> 52428800, //50Mb
				"types_allowed"=> [
					"banner"=>[
						"jpeg"=> 'image',
						"png"=> 'image',
						"gif"=> 'image'
					],
					"product"=>[
						"jpeg"=> 'image',
						"png"=> 'image',
						"gif"=> 'image',
						"pdf"=> 'pdf',
						"msword"=> 'word',
						"mspowerpoint"=> 'powerpoint',
						"vnd.ms-powerpoint"=> 'powerpoint',
						"x-zip-compressed"=> 'archive'
					],
				],
				"upload_accept"=>[
					"banner"=>'image/jpeg, image/png, image/gif',
					"product"=>'image/jpeg, image/png, image/gif, application/pdf, application/msword, application/zip, application/mspowerpoint, application/vnd.ms-powerpoint',
				]
			];
		}
	}

	public static function update($params) {
		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"name"=>[
				"type"=>'string',
				"min"=>0
			],
			"desc"=>[
				"type"=>'string',
				"min"=>0
			],
			"order_num"=>[
				"type"=>'int',
				"min"=>1
			]
		]);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `file` WHERE `id`=?i LIMIT 1', $params["id"])) return array('error'=> 'file not found');

		//////SET
		$set='SET ';
		if(isset($params["name"])) $set.='`file`.`name`='.DB::escapeString($params["name"]).', ';
		if(isset($params["order_num"])) $set.='`file`.`order_num`='.DB::escapeInt($params["order_num"]).', ';
		if(isset($params["desc"])) $set.='`file`.`desc`='.DB::escapeString($params["desc"]).', ';

		if(strlen($set)>7) $set=rtrim($set, ', ');
		else return array('error'=>'nothing to update');

		$q='UPDATE `file` '.$set.' WHERE `file`.`id`=?i LIMIT 1';
		if(DB::query($q, $params["id"])) return array("id"=>$params["id"]);
		else return array('error'=>'db error');
	}

	public static function create_product($params) {
		$params=Verify::filter($params, [
			"product_id"=>[
				"type"=>'int',
				"require"=>true
			],
			"type"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255,
				"require"=>true
			],
			"src"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255,
				"require"=>true
			],
			"name"=>[
				"type"=>'string',
				"min"=>0
			],
			"desc"=>[
				"type"=>'string',
				"min"=>0
			],
			"order_num"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>255
			],
			"size"=>[
				"type"=>'int'
			],
			"size_nice"=>[
				"type"=>'string'
			]
		]);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `product` WHERE `id`=?i LIMIT 1', $params["product_id"])) return array('error'=> 'product not found');

		if(DB::getOne('SELECT `id` FROM `file` WHERE `type`=?s AND `src`=?s LIMIT 1', $params["type"], $params["src"])) return array('error'=> 'file with same type and src already exist ['.$params["type"].']['.$params["src"].']');

		if(!isset($params["name"])) $params["name"]='';
		if(!isset($params["desc"])) $params["desc"]='';
		if(!isset($params["order_num"])) $params["order_num"]=5;
		$q="
			INSERT INTO `file` (`product_id`, `type`, `src`, `name`, `desc`, `order_num`)
			VALUES (?i, ?s, ?s, ?s, ?s, ?i)
		";
		DB::query($q, $params["product_id"], $params["type"], $params["src"], $params["name"], $params["desc"], $params["order_num"]);

		if($id=DB::insertId()) return array("id"=>$id);
		else return array("error"=>'db error');
	}

	public static function delete($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `file` WHERE `id`=?i LIMIT 1', $params["id"])) return array('error'=> 'id not found ['.$params["id"].']');

		DB::query('DELETE FROM `file` WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) return array("id"=>$params["id"]);
		else return array('error'=> 'not found');
	}

	public static function new_name($path, $name) {
		$name=strtr($name, ' ', '_');

		$p=pathinfo($name);
		$ext=$p['extension'];
		$basename=rtrim($p['basename'], '.'.$ext);
		$i=1;
		while(true){
			if(!file_exists($path.$name)) break;
			$i++;
			$name=$basename.'_'.$i.'.'.$ext;
		}

		return $name;
    }
	
	public static function size_convert($n){
		if($n<1024) return $n.' Байт';
		elseif($n<1048576) {
			$n=round($n/1024, 2);
			return $n.' Кб';
		}else{
			$n=round($n/1048576, 2);
			return $n.' Мб';
		}
		return $n;
	}

	public static function upload($file, $target){

			if($file['error']!=0) return array("error"=> 'transmit error ['.$file['error'].']');
			if(!isset($file['name'])) return array("error"=> 'name required');
			if(!isset($file['type'])) return array("error"=> 'file type required');
			if(!isset($file['tmp_name'])) return array("error"=> 'tmp_name required');
			if(!isset($file['size'])) return array("error"=> 'size required');

			if($file['size']>Core::config('file_upload_max_size')) return array("error"=> 'max file size is '.Core::config('file_upload_max_size').' ['.$file['size'].']');

			$type=substr(strrchr($file['type'], '/'), 1);

			switch($target){
				case 'banner':
					if(!isset($type)) return array("error"=> 'file type not supported ['.$type.']');
						
					/*Core::config('banner_upload_allowed') //image/jpeg, image/png, image/gif
					, self::$config["types_allowed"]["$target"]["$type"]))  return array("error"=> 'file type not supported ['.$type.']');
				!!!!*/
					break;
				case 'product':
					
					break;
				default:
				
			}
			

			$params["type"]='image';

			$path=ROOT_DIR.'/files/'.$params["type"].'/';
			if(!is_dir($path)) mkdir($path);

			$params["src"]=self::new_name($path, $file['name']);
			$dest=$path.$params["src"];

			if(move_uploaded_file($file['tmp_name'], $dest)){
				$result["src"]=$params["src"];
				$result["type"]=$params["type"];
				$result["size"]=$file['size'];
				$result["size_nice"]=self::size_convert($file['size']);
			}else{
				$result["error"]='cant move_uploaded_file ['.$dest.']';
			}
			
			return $result;
	}
	
	public static function upload_accept_type($mode){
		if(isset(self::$config["upload_accept"]["$mode"])) return self::$config["upload_accept"]["$mode"];
		else return '';
	}

}