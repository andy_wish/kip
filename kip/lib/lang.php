<?php if (!defined("INBOX")) die('separate call');



class Lang{

	private static $_instance;
	private static $APIKEY;
	private static $URL;

	protected static $current='ru';
	protected static $config;
	protected static $translations_base=[];


	private function __clone(){}
	private function __wakeup(){}
	private function __construct($var){

		self::$APIKEY=Core::config('translate_yandex_api_key');
		self::$URL=Core::config('translate_yandex_api_url');
		self::$config=unserialize(Core::config('lang'));

		if(isset($var["cookie"]["lang"]) AND in_array($var["cookie"]["lang"], self::$config["list"])) {
			self::$current = $_COOKIE["lang"];
		}else{
			self::$current=self::getBestMatch();
			if(!self::$current) self::$current = self::$config["default"];
		}

		$a=DB::getAll('SELECT `name`, `value` FROM `language` WHERE `lang`=?s', self::$current);
		foreach($a as $b){
			self::$translations_base[$b["name"]]=$b["value"];
		}

	}

	public static function init($var) {
		if (self::$_instance === null) {
			self::$_instance = new self($var);
			unset($var["lang"]["list"]);
			return $var;
		}
	}

	protected static function getBestMatch(){
		/**
		* Lang_detect Class
		* @author        La2ha
		* @version       1.0
		* @link          http://la2ha.ru/dev-seo-diy/web/lang_detect
		*/
		if ($list = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']) : null) {
			if (preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
				$accept_langs = array_combine($list[1], $list[2]);
				foreach ($accept_langs as $n => $v)
					$accept_langs[$n] = $v ? $v : 1;
				arsort($accept_langs, SORT_NUMERIC);
			}
		}else $accept_langs = array();
		$languages=array();
		foreach (self::$config["list"] as $lang => $alias) {
			if (is_array($alias)) {
				foreach ($alias as $alias_lang) {
					$languages[strtolower($alias_lang)] = strtolower($lang);
				}
			}else $languages[strtolower($alias)]=strtolower($lang);
		}
		foreach ($accept_langs as $l => $v) {
			$s = strtok($l, '-');
			if (isset($languages[$s]))
				return $languages[$s];
		}
		return false;
	}

	public static function current_lang() {
		return self::$current;
	}

	public static function str($str_key) {
		if(self::$current=='ru') return $str_key;

		if(array_key_exists($str_key, self::$translations_base)) return self::$translations_base["$str_key"];
		else {
			$str_translated=self::translate($str_key, 'ru-'.self::$current, 'html');
			if(isset($str_translated["error"])) return 'error:'.$str_translated["error"];

			self::$translations_base["str_key"]=$str_translated;
			self::in_base($str_key, $str_translated);
			return $str_translated;
		}
	}

	private static function in_base($str_key, $str_translated){
		DB::query('INSERT IGNORE INTO `language` (`lang`, `name`, `value`) VALUES (?s, ?s, ?s)', self::$current, $str_key, $str_translated);
	}

	public static function listing() {
		return self::$config["list"];
	}

	/*
	public static function translate_infobox($infobox_id){
		if(!$infobox=DB::getRow('SELECT `name`, `text` FROM `infobox_content` WHERE `lang`="ru" AND `infobox_id`=?i LIMIT 1', $infobox_id)) return ["error"=>'error select infobox(ru) for translate'];
		$infobox_new=[
			"name"=>self::translate($infobox["name"], $lang='ru-'.self::current_lang(), $format='html'),
			"text"=>self::translate($infobox["text"], $lang='ru-'.self::current_lang(), $format='html')
		];
		if(DB::query('INSERT INTO `infobox_content` (`infobox_id`, `lang`, `name`, `text`) VALUES (?i, ?s, ?s, ?s)', $infobox_id, self::current_lang(), $infobox_new["name"], $infobox_new["text"])){
			return $infobox_new;
		}else return ["error"=>'error insert infobox translate'];
	}
*/

	public static function translate($t, $lang='ru', $format='plain') {

		if(trim($t)=='') return '';

		$ch = curl_init(self::$URL.'?lang='.$lang.'&text='.urlencode($t).'&key='.self::$APIKEY);
		curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array('Content-Type: application/x-www-form-urlencoded'),
			CURLOPT_POSTFIELDS => [
				//"lang"=>$lang,
				//"key"=>self::$APIKEY,
				//"text"=>$text,
				//"format"=>$format
			]
		));

		$response = curl_exec($ch);
		if($response === FALSE){
			die(curl_error($ch));
		}

		//echo '<pre>';var_dump($response);exit;
		$response=json_decode($response, true);

		if($response["code"]==200) return $response["text"][0];
		else return ["error"=>$response["code"]];
	}

	public static function new_lang_create($lang){

		$parent_id_change=[0=>0];

		$list=Basis::read([
			"lang" =>'ru',
			"response"=>[
				"order"=>"id",
				"direction"=>'asc',
				"limit"=>1000000
			]
		]);
			//var_dump($list);

		foreach($list["list"] as $row){
			//echo '<pre>';var_dump($row);echo '</pre>';

			$id_translated_from=$row["id"];
			unset($row["id"], $row["user_id"], $row["user_name"], $row["ip"], $row["created"], $row["created_nice"]);
			
			$id_parent=$row["id_parent"];

			$row["name"]=self::translate($row["name"], 'ru-'.$lang, 'html');
			$row["name_translit"]=str_replace(' ', '-', trim(strtolower($row["name"])));
			$row["name_translit"]=str_replace('&quot;', '', $row["name_translit"]);

			$row["content"]=self::translate($row["content"], 'ru-'.$lang, 'html');
			$row["title"]=self::translate($row["title"], 'ru-'.$lang, 'html');
			$row["description"]=self::translate($row["description"], 'ru-'.$lang, 'html');
			$row["keywords"]=self::translate($row["keywords"], 'ru-'.$lang, 'html');
			
			$row["lang"]=$lang;
			$row["id_parent"]=$parent_id_change["$id_parent"];//замена на переведенный id для потомков
			if($row["closed"]=='') $row["closed"]=0;

			//echo '<h4>Будем переводить:</h4><pre>';var_dump($row);echo '</pre>';//exit;
			$result=Basis::create($row);
			if(isset($result["error"])){
				echo '<h5>ERROR</h5><pre>'.var_dump($result).'</pre><pre>'.var_dump($row).'</pre>';exit;
				//return $result;
			}

			$parent_id_change["$id_translated_from"]=$result["id"];

			$meta_params=[
				"basis_id"=> $result["id"],
				"name"=> 'translated_from',
				"value"=> (int)$id_translated_from,
				"autoload"=> 0
			];

			$result=Basis::create_meta($meta_params);
			if(isset($result["error"])){
				echo 'meta_params:<br /> <pre>'.var_dump($meta_params).'</pre>';
				echo 'ERROR<br /> <pre>'.var_dump($result).'</pre>';exit;
				//return $result;
			}

			//echo '<hr />';
			
		}
		//echo '<pre>';var_dump($parent_id_change);


		exit;
	}

}