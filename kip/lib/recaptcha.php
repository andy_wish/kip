<?php if (!defined("INBOX")) die('separate call');

Recaptcha::instance();

class Recaptcha {

	const URL = 'https://www.google.com/recaptcha/api/siteverify';
	const SCORE_MIN = 0;

	protected static $_instance;
	protected static $secret_key;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	public static function instance() {
		if (self::$_instance === null) {
			self::$secret_key = Core::config('grecaptcha_secret_key');
			self::$_instance = new self;
		}
	}

	public static function check($token, $action) {

		$params = [
			'secret' => self::$secret_key,
			'response' => $token,
			'remoteip' => $_SERVER['REMOTE_ADDR']
		];

		$ch = curl_init(self::URL);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = curl_exec($ch);
		if(!empty($response)) $decoded_response = json_decode($response);

		if (
			$decoded_response AND
			$decoded_response->success AND
			$decoded_response->action == $action AND
			$decoded_response->score > self::SCORE_MIN
		){
			return true;
		}else return false;
	}

}