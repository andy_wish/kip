<?php
require 'config.php';

if(DEBUG){
	ini_set('display_errors',1);
	error_reporting(E_ALL);
}

spl_autoload_register(function($class) {
	require_once(ROOT_DIR.'/lib/'.strtolower($class).'.php');
});

$routes=explode('/', $_SERVER['REQUEST_URI']);
switch($routes[1]){
	case 'admin':
		require ROOT_DIR.'/admin/boot.php';
		break;
	case 'jsonrpc':
		require ROOT_DIR.'/jsonrpc/boot.php';
		break;
	default:
		require ROOT_DIR.'/app/boot.php';
}