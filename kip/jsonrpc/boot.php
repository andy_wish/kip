<?php if (!defined("INBOX")) die('separate call');

//echo file_get_contents("php://input");echo '<hr/>';echo '<pre>';var_dump(json_decode(file_get_contents("php://input"), true));echo '</pre>';echo '<hr/>';

$request=json_decode(file_get_contents("php://input"), true);

DB::init($var["db"]);

require ROOT_DIR.'/jsonrpc/core/core.php';
Core::verify($request);

require ROOT_DIR.'/jsonrpc/core/model.php';
require ROOT_DIR.'/jsonrpc/core/controller.php';

if(isset($request["params"]["token"])) {
	User::auth($request["params"]["token"]);
	unset($request["params"]["token"]);
}else User::auth(false);

require ROOT_DIR.'/jsonrpc/access.php';
Access::init($conf["access"]);

require ROOT_DIR.'/jsonrpc/core/route.php';
Route::start($request);
