<?php if (!defined("INBOX")) die('separate call');

class Route {

	static function start($request) {

		$routes = explode('.', $request["method"]);

		$controller=$routes[0];
		$action=$routes[1];

		if(!Access::permit($controller, $action)){
			Core::error('unauthorized access');
		}

		$model=$controller.'_model';
		$controller=$controller.'_controller';

		$controller_path = ROOT_DIR.'/jsonrpc/pack/'.strtolower($controller).'.php';
		if(file_exists($controller_path)) {
			include $controller_path;

			$model_path = ROOT_DIR.'/jsonrpc/pack/'.strtolower($model).'.php';
			if(file_exists($model_path)) {
				include $model_path;
			}

		}else Core::error('controller`s file not found: '.$controller);

		$controller = new $controller;

		if(method_exists($controller, $action)) {
			$controller->$action($request["params"]);
		}else Core::error('action`s method not found: '.$action);
	}

}