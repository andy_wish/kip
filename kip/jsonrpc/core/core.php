<?php if (!defined("INBOX")) die('separate call');

class Core {

	const JSON_RPC_VERSION='2.0';

	protected static $_instance;
	protected static $config=[];

	private static $id=null;
	private static $result;

	private function __clone(){}
	private function __wakeup(){}
	public function __construct(){
		$a=DB::getAll('SELECT `name`, `value` FROM `options` WHERE `autoload`=1');
		foreach($a as $b){
			self::$config[$b["name"]]=$b["value"];
		}
	}

	public static function verify($request){
		if (self::$_instance === null) {
			self::$_instance = new self();
			
			if(isset($request["id"])) {
				if($request["id"]!=null) self::$id=$request["id"];
				else self::error('id is null');
			}

			if(!isset($request["jsonrpc"])) self::error('Invalid Request, jsonrpc not found', -32768);
			if($request["jsonrpc"]!==self::JSON_RPC_VERSION) self::error('Only jsonrpc 2.0 supported', -32600);
			if(!isset($request["method"])) self::error('Invalid Request, method not found', -32601);
			if(!is_string($request["method"])) self::error('Invalid Request, method must be a string', -32601);
			if(strlen($request["method"])<3 OR strlen($request["method"])>100) self::error('Invalid Request, method must be 3-100 characters', -32601);
			//if(!isset($request["params"]["access_token"]) AND $request["method"]!='auth.token_refresh' AND $request["method"]!='auth.login') self::error('access token required or methods auth.refresh_token/auth.login', 1);
		}
	}

	public static function error($m, $c=0, $d=null){
		$response["error"]["message"]=$m;
		$response["error"]["code"]=$c;
		if(isset($d)) $response["error"]["data"]=$d;
		self::send($response);
	}

	public static function result($result){
		$response["result"]=$result;
		self::send($response);
	}

	private static function send($response){
		$response["jsonrpc"]=self::JSON_RPC_VERSION;
		$response["id"]=self::$id;
		header("Access-Control-Allow-Origin: *");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400'); 
		echo json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		exit;
	}

	public static function config($name){
		if(isset(self::$config[$name])) return self::$config[$name];
		else return self::config_find_value($name);
	}

	private static function config_find_value($name){
		if($val=DB::getOne('SELECT `value` FROM `options` WHERE `name`=?s LIMIT 1', $name)){
			self::config_add($name, $val);
			return $val;
		}else return false;
	}

	private static function config_add($name, $val){
		self::$config[$name]=$val;
	}

	public static function translit($s) {
		//$s=(string) $s; // в строковое значение
		//$s=strip_tags($s); // HTML-теги
		$s=str_replace(array("\n", "\r"), " ", $s); // перевод каретки
		$s=str_replace("+", " ", $s);
		$s=str_replace("/", " ", $s);
		$s=str_replace(".", " ", $s);
		$s=trim($s); // пробелы в начале и конце строки
		$s=strtr($s, array(
			'а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>'',
			'А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Д'=>'d','Е'=>'e','Ё'=>'e','Ж'=>'j','З'=>'z','И'=>'i','Й'=>'y','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'c','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch','Ы'=>'y','Э'=>'e','Ю'=>'yu','Я'=>'ya','Ъ'=>'','Ь'=>''
		));
		$s=preg_replace("/[^0-9a-z-_ ]/i", "", $s); // от недопустимых символов
		$s=preg_replace("/\s+/", ' ', $s); // повторяющие пробелы
		$s=str_replace(" ", "-", $s); // пробелы знаком минус
		return $s;
	}

}
