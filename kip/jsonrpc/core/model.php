<?php if (!defined("INBOX")) die('separate call');

class Model {

	public function result_helper_verify($params) {

		foreach ($params as $name=>$value){

			switch ($name){
				case "response_order":
					if(!is_string($value) OR mb_strlen($value, 'utf-8')>255 OR mb_strlen($value, 'utf-8')<2) Response::error($name.' must be string and 2-255 characters. ['.$value.']');
					break;
				case "response_direction":
					if(!is_string($value) OR ($value!='asc' AND $value!='desc')) Response::error($name.' must be string: asc or desc(case sensitive) ['.$value.']');
					break;
				case "response_offset":
					if(!is_integer($value) OR $value<0) Response::error($name.' must be positive integer. ['.$value.']');
					break;
				case "response_limit":
					if(!is_integer($value) OR $value<1 OR $value>2000) Response::error($name.' must be integer from 1 to 2000. ['.$value.']');
					break;
			}
		}
	}

}