<?php if (!defined("INBOX")) die('not allowed');

class Comment_controller extends Controller {

	function __construct(){
		$this->model = new Comment_model();
	}

	function create($params) {
		Core::result($this->model->create($params));
	}

	function read($params) {
		Core::result(Basis::read($params));
	}

	function delete($params) {
		Core::result(Basis::delete($params));
	}

}
