<?php if (!defined("INBOX")) die('not allowed');

class User_controller extends Controller {

	function __construct(){
		$this->model = new User_model();
	}

	function registration_condition_read($params) {

		if(!Access::permit('user', 'registration_condition_read')) Core::error(401);

		$response=[
			"condition"=>[
				"text"=>User::registration_condition()
			]
		];
		Core::result($response);
	}

	function info_update($params) {

		if(!Access::permit('user', 'info_update')) Core::error(401);

		Core::result(User::info_update($params));
	}

	function password_update($params) {

		if(!Access::permit('user', 'password_update')) Core::error(401);

		Core::result(User::password_update($params));
	}

	function delete($params) {

		if(!Access::permit('user', 'delete')) Core::error(401);

		Core::result(User::delete($params));
	}

	function read($params) {

		if(!Access::permit('user', 'read')) Core::error(401);

		Core::result(User::read($params));
	}

	function access_recovery_request($params) {

		if(!Access::permit('user', 'access_recovery_request')) Core::error(401);

		Core::result(User::access_recovery_request($params));
	}
}
