<?php if (!defined("INBOX")) die('not allowed');

class Basis_controller extends Controller {

	function __construct(){
	}

	function create($params) {
		Core::result(Basis::create($params));
	}

	function read($params) {
		Core::result(Basis::read($params));
	}

	function update($params) {
		Core::result(Basis::update($params));
	}

	function delete($params) {
		Core::result(Basis::delete($params));
	}

}
