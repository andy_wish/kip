<?php if (!defined("INBOX")) die('separate call');

class Comment_model  extends Model {

	public function create($params) {

		if(
			isset($params["form_token"]) AND
			isset($params["form_action"]) AND
			$params["form_token"]!='false' AND
			$params["form_action"]=='user_comment_submit'
		){

			if(Recaptcha::check($params["form_token"], $params["form_action"])){

				unset($params["form_token"], $params["form_action"]);

				return Basis::create($params);

			}else return ["id"=>0];

		}else return ["id"=>0];

	}

}